
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#include "Hierarchical.hh"
namespace clustering {
template <typename T>
Hierarchical<T>::Hierarchical(const std::vector<T> objects,
                              float (*similarity)(const Cluster &,
                                                  const Cluster &),
                              float minSimilarity,
                              std::string (*printObject)(const T &))
    : _similarity(similarity), _minSimilarity(minSimilarity),
      _printObject(printObject) {
  assert(minSimilarity <= 1.f && minSimilarity >= 0.f);
  // generate level 0 where all clusters are of size 1 (one for each object)
  Level l0;
  for (const T &e : objects) {
    l0.push_back(Cluster({e}));
  }
  _levels.push_back(l0);

  _distMatrix = new float *[_levels[0].size()];
  for (size_t i = 0; i < l0.size(); i++) {
    _distMatrix[i] = new float[_levels[0].size()];
    // init
    for (size_t j = 0; j < _levels[0].size(); j++) {
      _distMatrix[i][j] = 0.f;
    }
  }

  // perform clustering
  execute();
}

template <typename T> Hierarchical<T>::~Hierarchical() {
  for (size_t i = 0; i < _levels[0].size(); i++) {
    delete[] _distMatrix[i];
  }
  delete _distMatrix;
}

template <typename T>
const typename Hierarchical<T>::Level &Hierarchical<T>::getTopLevel() {
  return _levels.back();
}
template <typename T>
const typename Hierarchical<T>::Level &Hierarchical<T>::getLevelWithAtleastClusterSize(size_t n){
    for (const auto &level : _levels) {
       for (const auto & cluster : level) {
           if(cluster.size()<n){
               goto nextLevel;
           }
       } 
       return level;

nextLevel:;
    }
    //if we don't find a suitable level
    return getTopLevel();
}

template <typename T> void Hierarchical<T>::execute() {
  std::unordered_set<size_t> deletedRows;
  std::unordered_set<size_t> deletedCols;

  // compute distance matrix
  size_t currentLevel = 0;
  auto &l0 = _levels[currentLevel];
  const size_t matrixSize = _levels[currentLevel].size();
  for (size_t i = 0; i < matrixSize; i++) {
    for (size_t j = 0; j < matrixSize; ++j) {
      if (i == j) {
        _distMatrix[i][j] = 0.f;
        continue;
      }
      _distMatrix[i][j] = _similarity(l0[i], l0[j]);
    }
  }
  /*
   * DEBUG - print dist matrix
  for (size_t i = 0; i < matrixSize; i++) {
    for (size_t j = 0; j < matrixSize; ++j) {
      if (deletedCols.count(j) || deletedRows.count(i)) {
        continue;
      }
      std::cout << std::setprecision(1) << _distMatrix[i][j] << "  ";
    }
    std::cout << "\n";
  }
  */

  while (deletedRows.size() < matrixSize - 1) {

    currentLevel = _levels.size() - 1;
    // find max
    size_t maxi = (1ULL << 63ULL);
    size_t maxj = (1ULL << 63ULL);
    float max = -1.f;
    for (size_t i = 0; i < matrixSize; i++) {
      for (size_t j = 0; j < matrixSize; j++) {
        if (i == j || deletedCols.count(j) || deletedRows.count(i)) {
          continue;
        }
        if (_distMatrix[i][j] > max) {
          max = _distMatrix[i][j];
          maxi = i;
          maxj = j;
        }
      }
    }

    if ((max <= _minSimilarity)) {
      break;
    }

    // copy the current level
    auto l = _levels[currentLevel];
    // merge clusters

    // DEBUG
     std::cout << "max:" << max << "maxi:" << maxi << "maxj:" << maxj << "\n";
    assert(l[maxj].size() > 0);
    //      std::cout << "Before: " << l[maxj].size() << "\n";
    for (const auto &i_obj : l[maxi]) {
      l[maxj].push_back(i_obj);
    }
    l[maxi].clear();
    _levels.push_back(l);

    // delete cols & rows
    deletedRows.insert({maxi});
    deletedCols.insert({maxi});
    // recompute scores for new cluster
    // row
    for (size_t j = 0; j < matrixSize; j++) {
      if (j == maxj || deletedCols.count(j)) {
        _distMatrix[maxj][j] = 0.f;
        continue;
      }
      _distMatrix[maxj][j] = _similarity(l[j], l[maxj]);
    }
    // col
    for (size_t i = 0; i < matrixSize; i++) {
      if (i == maxj || deletedRows.count(i)) {
        _distMatrix[i][maxj] = 0.f;
        continue;
      }
      _distMatrix[i][maxj] = _similarity(l[i], l[maxj]);
    }
    /*
     * DEBUG - print dist matrix
    for (size_t i = 0; i < matrixSize; i++) {
      for (size_t j = 0; j < matrixSize; ++j) {
        if (deletedCols.count(j) || deletedRows.count(i)) {
          continue;
        }
        std::cout << std::setprecision(5) << _distMatrix[i][j] << "  ";
      }
      std::cout << "\n";
    }
    */
  }
  // strip levels of empty clusters
  for (auto &l : _levels) {
    l.erase(std::remove_if(begin(l), end(l),
                           [](Cluster &c1) { return c1.empty(); }),
            end(l));
  }
}

template <typename F>
std::ostream &operator<<(std::ostream &o, Hierarchical<F> &h) {
  assert(h._printObject != nullptr);
  size_t levelID = 0;
  for (const auto &level : h._levels) {
    o << "Level " << levelID << "=======================\n";
    size_t clusterID = 0;
    for (const auto &cluster : level) {
      o << "Cluster " << clusterID << "-----------------------\n";
      for (const auto &obj : cluster) {
        o << h._printObject(obj) << " ";
      }
      clusterID++;
      o << "\n";
    }
    levelID++;
  }
  return o;
}

} // namespace clustering
