
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#pragma once
#include <algorithm>
#include <cassert>
#include <iomanip>
#include <iostream>
#include <ostream>
#include <unordered_set>
#include <vector>
namespace clustering {
template <typename T> class Hierarchical {
  using Cluster = std::vector<T>;
  using Level = std::vector<Cluster>;

public:
  Hierarchical(const std::vector<T> objects,
               float (*similarity)(const Cluster &, const Cluster &),
               float minSimilarity = 0.f,
               std::string (*printObject)(const T &) = nullptr);

  ~Hierarchical();
  const Level &getTopLevel();
  const Level &getLevelWithAtleastClusterSize(size_t n);

  template <typename F>
  friend std::ostream &operator<<(std::ostream &o, Hierarchical<F> &h);

private:
  void execute();

  float (*_similarity)(const Cluster &, const Cluster &);
  std::vector<Level> _levels;
  float **_distMatrix;
  float _minSimilarity;
  std::string (*_printObject)(const T &);
};

} // namespace clustering
#include "Hierarchical.i.hh"
