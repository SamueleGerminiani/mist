
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#include "visitors/OdenToZ3Visitor.hh"
#include "oden/odenUtils/propositionUtils.hh"

namespace oden {
static float float_precision = 0.01;
static float float_limit = std::pow(2, 20);
static double double_precision = 0.01;
static double double_limit = std::pow(2, 30);

#define EXTEND_LOGIC(O, OE1, OE2, E1, E2)                                      \
  uint8_t TO_EXTEND[2];                                                        \
  TO_EXTEND[0] = std::abs(OE1->getSize() - O.getSize());                       \
  TO_EXTEND[1] = std::abs(OE2->getSize() - O.getSize());                       \
  if (TO_EXTEND[0] > 0) {                                                      \
    /*If the operand is going from unsigned to signed, add a zero to           \
    avoid errors with z3::sext*/                                               \
    if (OE1->getType() == VarType::ULogic) {                                   \
      E1 = z3::zext(E1, 1);                                                    \
      TO_EXTEND[0] = TO_EXTEND[0] - 1;                                         \
    }                                                                          \
    E1 = z3::sext(E1, TO_EXTEND[0]);                                           \
  }                                                                            \
  if (TO_EXTEND[1] > 0) {                                                      \
    if (OE2->getType() == VarType::ULogic) {                                   \
      E2 = z3::zext(E2, 1);                                                    \
      TO_EXTEND[1] = TO_EXTEND[1] - 1;                                         \
    }                                                                          \
    E2 = z3::sext(E2, TO_EXTEND[1]);                                           \
  }

#define EXTEND_LOGIC_APPLY_CSTANDARD(O, OE1, OE2, E1, E2)                      \
  auto conversionResult = applyCStandardConversion(                            \
      std::make_pair(OE1->getType(), OE1->getSize()),                          \
      std::make_pair(OE2->getType(), OE2->getSize()));                         \
  uint8_t TO_EXTEND[2];                                                        \
  TO_EXTEND[0] = std::abs(OE1->getSize() - conversionResult.second);           \
  TO_EXTEND[1] = std::abs(OE2->getSize() - conversionResult.second);           \
  if (TO_EXTEND[0] > 0) {                                                      \
    /*If the operand is going from unsigned to signed, add a zero to           \
    avoid errors with z3::sext*/                                               \
    if (OE1->getType() == VarType::ULogic) {                                   \
      E1 = z3::zext(E1, 1);                                                    \
      TO_EXTEND[0] = TO_EXTEND[0] - 1;                                         \
    }                                                                          \
    E1 = z3::sext(E1, TO_EXTEND[0]);                                           \
  }                                                                            \
  if (TO_EXTEND[1] > 0) {                                                      \
    if (OE2->getType() == VarType::ULogic) {                                   \
      E2 = z3::zext(E2, 1);                                                    \
      TO_EXTEND[1] = TO_EXTEND[1] - 1;                                         \
    }                                                                          \
    E2 = z3::sext(E2, TO_EXTEND[1]);                                           \
  }

#define EXTEND_FLOAT(O, OE1, OE2, E1, E2)                                      \
  bool TO_EXTEND[2];                                                           \
  TO_EXTEND[0] = (O.getSize() - OE1->getSize()) != 0;                          \
  TO_EXTEND[1] = (O.getSize() - OE2->getSize()) != 0;                          \
  z3::expr rm(_c, _c.fpa_rounding_mode());                                   \
  z3::sort srt = _c.fpa_sort(11, 53);                                         \
  if (TO_EXTEND[0]) {                                                          \
    E1 = z3::expr(_c, Z3_mk_fpa_to_fp_float(_c, rm, E1, srt));               \
  }                                                                            \
  if (TO_EXTEND[1]) {                                                          \
    E2 = z3::expr(_c, Z3_mk_fpa_to_fp_float(_c, rm, E2, srt));               \
  }

#define EXTEND_FLOAT_APPLY_CSTANDARD(O, OE1, OE2, E1, E2)                      \
  auto conversionResult = applyCStandardConversion(                            \
      std::make_pair(OE1->getType(), OE1->getSize()),                          \
      std::make_pair(OE2->getType(), OE2->getSize()));                         \
                                                                               \
  bool TO_EXTEND[2];                                                           \
  TO_EXTEND[0] = (conversionResult.second - OE1->getSize()) != 0;              \
  TO_EXTEND[1] = (conversionResult.second - OE2->getSize()) != 0;              \
                                                                               \
  z3::expr rm(_c, _c.fpa_rounding_mode());                                   \
  z3::sort srt = _c.fpa_sort(11, 53);                                         \
  if (TO_EXTEND[0]) {                                                          \
    E1 = z3::expr(_c, Z3_mk_fpa_to_fp_float(_c, rm, E1, srt));               \
  }                                                                            \
  if (TO_EXTEND[1]) {                                                          \
    E2 = z3::expr(_c, Z3_mk_fpa_to_fp_float(_c, rm, E2, srt));               \
  }

OdenToZ3Visitor::~OdenToZ3Visitor() {  }

OdenToZ3Visitor::OdenToZ3Visitor()
    : OdenVisitor(), _z3Expressions(),  _overflowCond(_c),_varToTypeSize() {

        _overflowCond = _c.bool_val(true);
}

z3::expr OdenToZ3Visitor::get() {
  /*
if (_z3Expressions.size() != 1) {
  std::cout << "Size: "<<_z3Expressions.size()<<"\n\n\n";
while (!_z3Expressions.empty()) {
  std::cout << _z3Expressions.top()<<"\n\n\n";
  _z3Expressions.pop();
}
exit(0);
}
*/
  assert(_z3Expressions.size() == 1);
  //    std::cout<<_overflowCond<<"\n";
  return _z3Expressions.top() && _overflowCond;
}


size_t OdenToZ3Visitor::getNumberVariables() { return _varToTypeSize.size(); }
void OdenToZ3Visitor::clear() {
  for (size_t i = 0; i < _z3Expressions.size(); i++) {
    _z3Expressions.pop();
  }
  
  _overflowCond = _c.bool_val(true);
  _varToTypeSize.clear();
}

// proposition
void OdenToZ3Visitor::visit(oden::BooleanVariable &o) {
  assert(_varToTypeSize.count(o.getName()) == 0 ||
         (_varToTypeSize.at(o.getName()) ==
          std::make_pair(o.getType(), o.getSize())));
  _varToTypeSize[o.getName()] = std::make_pair(o.getType(), o.getSize());

  _z3Expressions.push(_c.bool_const(o.getName().c_str()));
}

void OdenToZ3Visitor::visit(oden::BooleanConstant &o) {
  if (o._getValue()) {
    z3::expr trueConstant = _c.bool_val(true);
    _z3Expressions.push(trueConstant);
  } else {
    z3::expr falseConstant = _c.bool_val(false);
    _z3Expressions.push(falseConstant);
  }
}

void OdenToZ3Visitor::visit(oden::PropositionAnd &o) {
  z3::expr_vector expVector(_c);

  assert(o.getItems().size() > 1);

  for (const auto &e : o.getItems()) {
    e->acceptVisitor(*this);
    expVector.push_back(_z3Expressions.top());
    _z3Expressions.pop();
  }
  _z3Expressions.push(z3::mk_and(expVector));
}

void OdenToZ3Visitor::visit(oden::PropositionOr &o) {
  z3::expr_vector expVector(_c);

  assert(o.getItems().size() > 1);

  for (const auto &e : o.getItems()) {
    e->acceptVisitor(*this);
    expVector.push_back(_z3Expressions.top());
    _z3Expressions.pop();
  }
  _z3Expressions.push(z3::mk_or(expVector));
}
void OdenToZ3Visitor::visit(oden::PropositionXor &o) {
  assert(o.getItems().size() == 2);
  o.getItems()[0]->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();
  o.getItems()[1]->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  _z3Expressions.push(e1 ^ e2);
}

void OdenToZ3Visitor::visit(oden::PropositionEq &o) {
  assert(o.getItems().size() == 2);
  o.getItems()[0]->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();
  o.getItems()[1]->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  _z3Expressions.push(e1 == e2);
}

void OdenToZ3Visitor::visit(oden::PropositionNeq &o) {
  assert(o.getItems().size() == 2);
  o.getItems()[0]->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();
  o.getItems()[1]->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  _z3Expressions.push(e1 != e2);
}

void OdenToZ3Visitor::visit(oden::PropositionNot &o) {
  assert(o.getItems().size() == 1);
  o.getItems()[0]->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();
  _z3Expressions.push(!e1);
}

void OdenToZ3Visitor::visit(oden::LogicNot &o) {
  assert(o.getItems().size() == 1);
  o.getItems()[0]->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();
  _z3Expressions.push(~e1);
}

// numeric
void OdenToZ3Visitor::visit(oden::NumericVariable &o) {
  assert(_varToTypeSize.count(o.getName()) == 0 ||
         (_varToTypeSize.at(o.getName()) ==
          std::make_pair(o.getType(), o.getSize())));

  _varToTypeSize[o.getName()] = std::make_pair(o.getType(), o.getSize());

  if (o.getSize() == 32) {
    z3::expr var = _c.fpa_const(o.getName().c_str(), 8, 24);
    _z3Expressions.push(var);
    _overflowCond = _overflowCond &&
                    var != _c.fpa_val(-std::numeric_limits<float>::infinity());
    _overflowCond = _overflowCond &&
                    var != _c.fpa_val(std::numeric_limits<float>::infinity());
    _overflowCond =
        _overflowCond && (z3::abs(var) > _c.fpa_val(float_precision) ||
                          var == _c.fpa_val(.0f));
    _overflowCond = _overflowCond && (z3::abs(var) < _c.fpa_val(float_limit));
  } else if (o.getSize() == 64) {
    z3::expr var = _c.fpa_const(o.getName().c_str(), 11, 53);
    _z3Expressions.push(var);
    _overflowCond = _overflowCond && var != _c.fpa_val(std::nan(""));
    _overflowCond = _overflowCond &&
                    var != _c.fpa_val(std::numeric_limits<double>::infinity());
    _overflowCond =
        _overflowCond &&
        var != _c.fpa_val(-std::numeric_limits<double>::infinity());
    _overflowCond =
        _overflowCond && (z3::abs(var) > _c.fpa_val(double_precision) ||
                          var == _c.fpa_val(.0));
    _overflowCond = _overflowCond && (z3::abs(var) < _c.fpa_val(double_limit));
  } else {
    messageError("Wrong floating-point size: expected 32 or 64!");
  }
}

void OdenToZ3Visitor::visit(oden::NumericConstant &o) {
  if (o.getSize() == 32) {
    _z3Expressions.push(_c.fpa_val((float)o._getValue()));
  } else if (o.getSize() == 64) {
    _z3Expressions.push(_c.fpa_val((double)o._getValue()));
  } else {
    messageError("Wrong floating-point size: expected 32 or 64!");
  }
}

void OdenToZ3Visitor::visit(oden::NumericSum &o) {
  assert(o.getItems().size() > 1);

  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  EXTEND_FLOAT(o, oe1, oe2, e1, e2);

  z3::expr finalExpr = e1 + e2;

  _z3Expressions.push(finalExpr);
}

void OdenToZ3Visitor::visit(oden::NumericSub &o) {
  assert(o.getItems().size() > 1);

  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  EXTEND_FLOAT(o, oe1, oe2, e1, e2);
  z3::expr finalExpr = e1 - e2;

  _z3Expressions.push(finalExpr);
}

void OdenToZ3Visitor::visit(oden::NumericMul &o) {
  assert(o.getItems().size() > 1);

  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  EXTEND_FLOAT(o, oe1, oe2, e1, e2);

  z3::expr finalExpr = e1 * e2;

  _z3Expressions.push(finalExpr);
}
void OdenToZ3Visitor::visit(oden::NumericDiv &o) {
  assert(o.getItems().size() > 1);

  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  if (oe2->getSize() == 32) {
    _overflowCond = _overflowCond && (e2 != _c.fpa_val((float)0));
  } else {
    _overflowCond = _overflowCond && (e2 != _c.fpa_val((double)0));
  }

  EXTEND_FLOAT(o, oe1, oe2, e1, e2);

  z3::expr finalExpr = e1 / e2;

  _z3Expressions.push(finalExpr);
}
void OdenToZ3Visitor::visit(oden::NumericEq &o) {
  assert(o.getItems().size() == 2);
  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  EXTEND_FLOAT_APPLY_CSTANDARD(o, oe1, oe2, e1, e2);

  _z3Expressions.push(e1 == e2);
}
void OdenToZ3Visitor::visit(oden::NumericNeq &o) {
  assert(o.getItems().size() == 2);
  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  EXTEND_FLOAT_APPLY_CSTANDARD(o, oe1, oe2, e1, e2);

  _z3Expressions.push(e1 != e2);
}

void OdenToZ3Visitor::visit(oden::NumericGreater &o) {
  assert(o.getItems().size() == 2);
  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  EXTEND_FLOAT_APPLY_CSTANDARD(o, oe1, oe2, e1, e2);

  _z3Expressions.push(e1 > e2);
}

void OdenToZ3Visitor::visit(oden::NumericGreaterEq &o) {
  assert(o.getItems().size() == 2);
  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  EXTEND_FLOAT_APPLY_CSTANDARD(o, oe1, oe2, e1, e2);

  _z3Expressions.push(e1 >= e2);
}

void OdenToZ3Visitor::visit(oden::NumericLess &o) {
  assert(o.getItems().size() == 2);
  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  EXTEND_FLOAT_APPLY_CSTANDARD(o, oe1, oe2, e1, e2);

  _z3Expressions.push(e1 < e2);
}

void OdenToZ3Visitor::visit(oden::NumericLessEq &o) {
  assert(o.getItems().size() == 2);
  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  EXTEND_FLOAT_APPLY_CSTANDARD(o, oe1, oe2, e1, e2);

  _z3Expressions.push(e1 <= e2);
}

void OdenToZ3Visitor::visit(oden::NumericToBool &o) {
  o.getItem().acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();
  _z3Expressions.push(e1 > 0);
}

// logic
void OdenToZ3Visitor::visit(oden::LogicVariable &o) {
  assert(_varToTypeSize.count(o.getName()) == 0 ||
         (_varToTypeSize.at(o.getName()) ==
          std::make_pair(o.getType(), o.getSize())));

  _varToTypeSize[o.getName()] = std::make_pair(o.getType(), o.getSize());

  _z3Expressions.push(_c.bv_const(o.getName().c_str(), o.getSize()));
}

void OdenToZ3Visitor::visit(EnumVariable &o) {
  _enums.insert({{o.getName(), o}});
  _varToTypeSize[o.getName()] = std::make_pair(o.getType(), o.getSize());
  z3::expr var = _c.bv_const(o.getName().c_str(), o.getSize());
  _z3Expressions.push(var);

  auto items = o.getItems();
  z3::expr enumCond = var == _c.bv_val(items.front()._value, o.getSize());
  for (size_t i = 1; i < items.size(); i++) {
    enumCond = enumCond || var == _c.bv_val(items[i]._value, o.getSize());
  }

  _overflowCond = _overflowCond && enumCond;
}
void OdenToZ3Visitor::visit(NamedLogicConstant &o) {
  _z3Expressions.push(_c.bv_val(o._getValue(), o.getSize()));
}
void OdenToZ3Visitor::visit(oden::LogicConstant &o) {
  _z3Expressions.push(_c.bv_val(o._getValue(), o.getSize()));
}
void OdenToZ3Visitor::visit(oden::LogicSum &o) {
  assert(o.getItems().size() > 1);

  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  /*Extend the size of the operands to make all operands of the same size (if
   * needed)*/
  EXTEND_LOGIC(o, oe1, oe2, e1, e2)

  z3::expr finalExpr = e1 + e2;

  // Add over/underflow conditions
  if (o.getType() == VarType::SLogic) {
    _overflowCond = _overflowCond && z3::bvadd_no_overflow(e1, e2, true);
    _overflowCond = _overflowCond && z3::bvadd_no_underflow(e1, e2);
  } else {
    _overflowCond = _overflowCond && z3::bvadd_no_overflow(e1, e2, false);
    _overflowCond = _overflowCond && z3::bvadd_no_underflow(e1, e2);
  }

  _z3Expressions.push(finalExpr);
}

void OdenToZ3Visitor::visit(oden::LogicSub &o) {
  assert(o.getItems().size() > 1);

  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  /*Extend the size of the operands to make all operands of the same size (if
   * needed)*/
  EXTEND_LOGIC(o, oe1, oe2, e1, e2)

  z3::expr finalExpr = e1 - e2;

  // Add over/underflow conditions
  if (o.getType() == VarType::SLogic) {
    _overflowCond = _overflowCond && z3::bvsub_no_overflow(e1, e2);
    _overflowCond = _overflowCond && z3::bvsub_no_underflow(e1, e2, true);
  } else {
    _overflowCond = _overflowCond && z3::bvsub_no_overflow(e1, e2);
    _overflowCond = _overflowCond && z3::bvsub_no_underflow(e1, e2, false);
  }

  _z3Expressions.push(finalExpr);
}

void OdenToZ3Visitor::visit(oden::LogicMul &o) {
  assert(o.getItems().size() > 1);

  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  /*Extend the size of the operands to make all operands of the same size (if
   * needed)*/
  EXTEND_LOGIC(o, oe1, oe2, e1, e2)

  z3::expr finalExpr = e1 * e2;

  if (o.getType() == VarType::SLogic) {
    // Add over/underflow conditions
    _overflowCond = _overflowCond && z3::bvmul_no_overflow(e1, e2, true);
    _overflowCond = _overflowCond && z3::bvmul_no_underflow(e1, e2);
  } else {
    // Add over/underflow conditions
    _overflowCond = _overflowCond && z3::bvmul_no_overflow(e1, e2, false);
    _overflowCond = _overflowCond && z3::bvmul_no_underflow(e1, e2);
  }

  _z3Expressions.push(finalExpr);
}

void OdenToZ3Visitor::visit(oden::LogicDiv &o) {
  assert(o.getItems().size() > 1);

  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  // add the condition before extending the size
  _overflowCond = _overflowCond && (e2 != _c.bv_val(0, oe2->getSize()));

  /*Extend the size of the operands to make all operands of the same size (if
   * needed)*/
  EXTEND_LOGIC(o, oe1, oe2, e1, e2)

  z3::expr finalExpr = e1 / e2;

  // Add over/underflow conditions
  if (o.getType() == VarType::SLogic) {
    _overflowCond = _overflowCond && z3::bvsdiv_no_overflow(e1, e2);
  }

  _z3Expressions.push(finalExpr);
}
void OdenToZ3Visitor::visit(oden::LogicEq &o) {
  assert(o.getItems().size() == 2);

  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  /*Extend the size of the operands to make all operands of the same size (if
   * needed)*/
  EXTEND_LOGIC_APPLY_CSTANDARD(o, oe1, oe2, e1, e2)

  z3::expr finalExpr = (e1 == e2);

  _z3Expressions.push(finalExpr);
}

void OdenToZ3Visitor::visit(oden::LogicNeq &o) {
  assert(o.getItems().size() == 2);

  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  /*Extend the size of the operands to make all operands of the same size (if
   * needed)*/
  EXTEND_LOGIC_APPLY_CSTANDARD(o, oe1, oe2, e1, e2)

  _z3Expressions.push(e1 != e2);
}

void OdenToZ3Visitor::visit(oden::LogicGreater &o) {
  assert(o.getItems().size() == 2);

  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  /*Extend the size of the operands to make all operands of the same size (if
   * needed)*/
  EXTEND_LOGIC_APPLY_CSTANDARD(o, oe1, oe2, e1, e2)

  if (conversionResult.first == VarType::SLogic) {
    _z3Expressions.push(e1 > e2);
  } else {
    _z3Expressions.push(z3::ugt(e1, e2));
  }
}

void OdenToZ3Visitor::visit(oden::LogicGreaterEq &o) {
  assert(o.getItems().size() == 2);

  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  EXTEND_LOGIC_APPLY_CSTANDARD(o, oe1, oe2, e1, e2)

  if (conversionResult.first == VarType::SLogic) {
    _z3Expressions.push(e1 >= e2);
  } else {
    _z3Expressions.push(z3::uge(e1, e2));
  }
}

void OdenToZ3Visitor::visit(oden::LogicLess &o) {
  assert(o.getItems().size() == 2);

  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  EXTEND_LOGIC_APPLY_CSTANDARD(o, oe1, oe2, e1, e2)

  if (conversionResult.first == VarType::SLogic) {
    _z3Expressions.push(e1 < e2);
  } else {
    _z3Expressions.push(z3::ult(e1, e2));
  }
}

void OdenToZ3Visitor::visit(oden::LogicLessEq &o) {
  assert(o.getItems().size() == 2);

  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  EXTEND_LOGIC_APPLY_CSTANDARD(o, oe1, oe2, e1, e2)

  if (conversionResult.first == VarType::SLogic) {
    _z3Expressions.push(e1 <= e2);
  } else {
    _z3Expressions.push(z3::ule(e1, e2));
  }
}

void OdenToZ3Visitor::visit(oden::LogicToBool &o) {
  o.getItem().acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();
  _z3Expressions.push(e1 > 0);
}

void OdenToZ3Visitor::visit(oden::LogicBAnd &o) {
  assert(o.getItems().size() > 1);

  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  /*Extend the size of the operands to make all operands of the same size (if
   * needed)*/
  EXTEND_LOGIC(o, oe1, oe2, e1, e2)

  z3::expr finalExpr = e1 & e2;

  _z3Expressions.push(finalExpr);
}
void OdenToZ3Visitor::visit(oden::LogicLShift &o) {
  assert(o.getItems().size() > 1);

  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  /*Extend the size of the operands to make all operands of the same size (if
   * needed)*/
  EXTEND_LOGIC(o, oe1, oe2, e1, e2)

  z3::expr finalExpr = z3::shl(e1, e2);

  _z3Expressions.push(finalExpr);
}
void OdenToZ3Visitor::visit(oden::LogicRShift &o) {
  assert(o.getItems().size() > 1);

  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  /*Extend the size of the operands to make all operands of the same size (if
   * needed)*/
  EXTEND_LOGIC(o, oe1, oe2, e1, e2)

  z3::expr finalExpr = z3::ashr(e1, e2);

  _z3Expressions.push(finalExpr);
}
void OdenToZ3Visitor::visit(oden::LogicBOr &o) {
  assert(o.getItems().size() > 1);

  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  /*Extend the size of the operands to make all operands of the same size (if
   * needed)*/
  EXTEND_LOGIC(o, oe1, oe2, e1, e2)

  z3::expr finalExpr = e1 | e2;

  _z3Expressions.push(finalExpr);
}

void OdenToZ3Visitor::visit(oden::LogicBXor &o) {
  assert(o.getItems().size() > 1);

  auto &oe1 = o.getItems()[0];
  auto &oe2 = o.getItems()[1];

  oe1->acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();

  oe2->acceptVisitor(*this);
  z3::expr e2 = _z3Expressions.top();
  _z3Expressions.pop();

  /*Extend the size of the operands to make all operands of the same size (if
   * needed)*/
  EXTEND_LOGIC(o, oe1, oe2, e1, e2)

  z3::expr finalExpr = e1 ^ e2;

  _z3Expressions.push(finalExpr);
}

void OdenToZ3Visitor::visit(oden::LogicToNumeric &o) {
  auto &oe = o.getItem();
  oe.acceptVisitor(*this);
  z3::expr e1 = _z3Expressions.top();
  _z3Expressions.pop();
  Z3_ast floatExpr;
  z3::expr rm(_c, _c.fpa_rounding_mode());
  z3::sort srt = _c.fpa_sort(8, 24);
  if (oe.getType() == VarType::SLogic) {
    floatExpr = Z3_mk_fpa_to_fp_signed(_c, rm, e1, srt);
  } else {
    floatExpr = Z3_mk_fpa_to_fp_unsigned(_c, rm, e1, srt);
  }
  _z3Expressions.push(z3::expr(_c, floatExpr));
}

} // namespace oden
