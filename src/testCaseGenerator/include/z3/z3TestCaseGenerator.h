
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#pragma once
#include "z3++.h"
#include "z3.h"

#include "visitors/OdenToZ3Visitor.hh"
#include "data_struct.hh"

#include <bitset>
#include <vector>
#include <memory>

namespace z3 {
std::ostream &operator<<(std::ostream &o, const TestCase &tc);

class Z3TestCaseGenerator {
public:
  Z3TestCaseGenerator() = default;

  std::vector<TestCase> generateTestCase(oden::Proposition &p, size_t n = 1);

  expr propToz3Expr(oden::Proposition &p);
  std::vector<expr> propToz3Expr(std::vector<oden::Proposition *> &ps);
  std::vector<expr> propToz3Expr(std::vector<std::shared_ptr<oden::Proposition>> &ps) ;
  z3::context *getContext();

private:
  oden::OdenToZ3Visitor _converter;
};
inline std::string valueToString(Assignment &ass) {
  if (ass.second.second.namedValue != "") {
    return ass.second.second.namedValue;
  }
  switch (ass.second.first) {
  case oden::VarType::Bool:
    return std::to_string(ass.second.second.bVal);
    break;
  case oden::VarType::ULogic:
    return std::to_string(ass.second.second.uVal);
    break;
  case oden::VarType::SLogic:
    return std::to_string(ass.second.second.sVal);
    break;
  case oden::VarType::Numeric:
    return std::to_string(ass.second.second.dVal);
    break;
  }
  return "Uknown";
}
} // namespace z3
