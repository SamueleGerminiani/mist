
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/


#pragma once
#include <iostream>
#include <vector>
#include "oden/odenEnums.hh"
#include "oden/odenUtils/applicationUtils.hh" 
namespace z3 {
struct VarValue {
    VarValue(){}
    VarValue(uint64_t value):uVal(value){}
    VarValue(int64_t value):sVal(value){}
    VarValue(int64_t value,std::string name):sVal(value), namedValue(name){}
    VarValue(bool value):bVal(value){}
    VarValue(double value):dVal(value){}
    VarValue(float value):dVal(value){}
  uint64_t uVal;
  int64_t sVal;
  bool bVal;
  double dVal;
  std::string namedValue = "";
};

union intToFloat {
  uint32_t i;
  float f;
};


using VarName = std::string;
using TypeValue = std::pair<oden::VarType, VarValue>;
using Assignment = std::pair<VarName, TypeValue>;
using TestCase = std::vector<Assignment>;

inline bool operator==(TypeValue e1, TypeValue e2){

    messageErrorIf(e1.first != e2.first,"== on variables with different types: "+ std::to_string((int)e1.first)+" vs "+ std::to_string((int)e2.first) );
    
  switch (e1.first) {
  case oden::VarType::Bool:
    return e1.second.bVal == e2.second.bVal;
    break;
  case oden::VarType::ULogic:
    return e1.second.uVal == e2.second.uVal;
    break;
  case oden::VarType::SLogic:
    return e1.second.sVal == e2.second.sVal;
    break;
  case oden::VarType::Numeric:
    return e1.second.dVal == e2.second.dVal;
    break;
  }

  return false;
}
inline std::ostream& operator<<(std::ostream& os,const TypeValue e1){
    
  switch (e1.first) {
  case oden::VarType::Bool:
      os<<e1.second.bVal;
    break;
  case oden::VarType::ULogic:
      os<<e1.second.uVal;
    break;
  case oden::VarType::SLogic:
      os<<e1.second.sVal;
    break;
  case oden::VarType::Numeric:
      os<<e1.second.dVal;
    break;
  }

  return os;
}


}
