
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#pragma once
#include "z3++.h"
#include "z3.h"

#include "oden/OdenVisitor.hh"

#include <algorithm>
#include <cmath>
#include <stack>
#include <unordered_map>
#include <unordered_set>

namespace oden {

class OdenToZ3Visitor : public OdenVisitor {
public:
  /// @brief Constructor.
  OdenToZ3Visitor();

  /// @brief Destructor.
  virtual ~OdenToZ3Visitor() override;

  z3::expr get();
  size_t getNumberVariables();
  z3::context *getContext();

  void clear();
  void clear_keep_context();

  // proposition
  void visit(BooleanConstant &o) override;
  void visit(BooleanVariable &o) override;
  void visit(PropositionAnd &o) override;
  void visit(PropositionOr &o) override;
  void visit(PropositionXor &o) override;
  void visit(PropositionEq &o) override;
  void visit(PropositionNeq &o) override;
  void visit(PropositionNot &o) override;

  // numeric
  void visit(NumericConstant &o) override;
  void visit(NumericVariable &o) override;
  void visit(NumericSum &o) override;
  void visit(NumericSub &o) override;
  void visit(NumericMul &o) override;
  void visit(NumericDiv &o) override;
  void visit(NumericEq &o) override;
  void visit(NumericNeq &o) override;
  void visit(NumericGreater &o) override;
  void visit(NumericGreaterEq &o) override;
  void visit(NumericLess &o) override;
  void visit(NumericLessEq &o) override;
  void visit(NumericToBool &o) override;

  // logic
  void visit(LogicConstant &o) override;
  void visit(LogicVariable &o) override;
  void visit(EnumVariable &o) override;
  void visit(NamedLogicConstant &o) override;
  void visit(LogicSum &o) override;
  void visit(LogicSub &o) override;
  void visit(LogicMul &o) override;
  void visit(LogicDiv &o) override;
  void visit(LogicBAnd &o) override;
  void visit(LogicBOr &o) override;
  void visit(LogicBXor &o) override;
  void visit(LogicNot &o) override;
  void visit(LogicEq &o) override;
  void visit(LogicNeq &o) override;
  void visit(LogicGreater &o) override;
  void visit(LogicGreaterEq &o) override;
  void visit(LogicLess &o) override;
  void visit(LogicLessEq &o) override;
  void visit(LogicToNumeric &o) override;
  void visit(LogicToBool &o) override;
  void visit(LogicLShift &o) override;
  void visit(LogicRShift &o) override;

protected:
  enum ope : int {
    PropositionNot = 0,
    PropositionAnd,
    PropositionOr,
    PropositionXor,
    PropositionEq,
    PropositionNeq,

    NumericSum,
    NumericSub,
    NumericMul,
    NumericDiv,
    NumericEq,
    NumericNeq,
    NumericGreater,
    NumericGreaterEq,
    NumericLess,
    NumericLessEq,

    LogicSum,
    LogicSub,
    LogicMul,
    LogicDiv,
    LogicBAnd,
    LogicBOr,
    LogicBXor,
    LogicNot,
    LogicEq,
    LogicNeq,
    LogicGreater,
    LogicGreaterEq,
    LogicLess,
    LogicLessEq,
  };

  std::stack<z3::expr> _z3Expressions;
  z3::context _c;
  z3::expr _overflowCond;

public:
  std::unordered_map<std::string, std::pair<VarType, uint8_t>> _varToTypeSize;
  std::unordered_map<std::string, EnumVariable> _enums;
};

} // namespace oden
