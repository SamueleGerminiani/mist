
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#include "codeGenerator.hh"
#include "commandLineParser.hh"
#include <cmath>
#include <experimental/filesystem>
#include <numeric>

static void parseCommandLineArguments(int argc, char *args[],
                                      std::vector<std::string> &files,
                                      std::string &outDirectory);

int main(int argc, char *args[]) {

  // set default output directory
  std::string outDirectory = ".";
  std::vector<std::string> files;

  // parse command line arguments and retrieve input xml files
  parseCommandLineArguments(argc, args, files, outDirectory);

  // parse the given xml files
  codeGenerator::XMLparser parser;
  for (auto f : files) {
    messageInfo("Parsing " + f);
    parser.parseXML(f);
  }
  parser.checkErrors();

  // convert psl implications to bùchi inplication automatas
  // ex. (antecedent in PSL) -> (consequent in PSL) becomes
  //     (antecedent buchi automaton) -> (consequent buchi automaton)
  std::unordered_map<std::string, codeGenerator::AutomataImplication>
      idToAutomataImplication;
  std::map<std::string, codeGenerator::IARchecker> IARcheckers;

  // execute conversion for each PSL implication found and init IARcheckers
  auto &parsedAssertions = parser.getAssertions();
  for (const auto &id_PSLimp : parser.getPSLimplications()) {
    const auto &tp = parser.getTestPlan();
    // add checker only if used in plan
    if (std::find(begin(tp), end(tp), id_PSLimp.first) != end(tp)) {
      // init checkers from assertion
      
      //gather the parameters
      auto &assertion = parsedAssertions.at(id_PSLimp.first);
      auto automataImplication =
          codeGenerator::converter::generateAutomata(id_PSLimp.second);
      std::string checkerID = id_PSLimp.first;

      //build the object
      IARcheckers.emplace(checkerID,codeGenerator::IARchecker(automataImplication, assertion, checkerID));
    }
  }

  // dump files
  // constructor <checker,interruptSetup,StartingCondition, NusedTimers>
  codeGenerator::IARfileManager fm(IARcheckers, parser.getIARinterruptsSetup(),
                                   parser.getStartingCondition(),
                                   std::vector<std::string>(), parser._maxTimers);

  if (parser.isGenerateTestplanSet()) {
    codeGenerator::TestPlanGenerator tpg(parser.getAssertions(),
                                         parser.getTestPlan(),
                                         parser.getFirstAssertion().first);
    // generate testplan
    // set testplan
    messageInfo("Generating testplan...");

    std::stringstream ss;
    ss << "Generated testplan: [";
    for (const auto &e : tpg._testplan) {
      ss << e << " ";
    }
    ss << "]";
    messageInfo(ss.str());

    fm.setTestplan(tpg._testplan);
    fm.setRestoreState(tpg._assIDtoRestoreStateID);
  } else {
    // set default testplan
    fm.setTestplan(parser.getTestPlan());
  }

  messageInfo("Number of tests in plan: " +
              std::to_string(parser.getTestPlan().size()));

  fm.setDebugVariables(parser.getDebugVars());

  // perform file dumping
  fm.generateFiles(outDirectory);

  return 0;
}
void parseCommandLineArguments(int argc, char *args[],
                               std::vector<std::string> &files,
                               std::string &outDirectory) {

  auto result = parse(argc, args);

  if (result.count("f")) {
    files = result["f"].as<std::vector<std::string>>();
  }
  if (result.count("o")) {
    outDirectory = result["o"].as<std::string>();
  }
  if (result.count("d")) {
    std::cout << "Adding files from directory: "
              << result["d"].as<std::string>() << "\n";
    for (const auto &entry : std::experimental::filesystem::directory_iterator(
             result["d"].as<std::string>())) {
      if (entry.path().extension() == ".xml") {
        files.push_back(entry.path().u8string());
      }
    }
  }
  messageErrorIf(files.empty(), "No xml files found!");
}
