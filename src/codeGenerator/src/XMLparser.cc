/**
****************************************************************************
* Copyright Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
 * @file
 * @author
 * Samuele Germiniani, samuele.germiniani@univr.it
 * Moreno Bragaglio, moreno.bragaglio@univr.it
 * @date 2020
 * @version v1.0
 */

#include "XMLparser.hh"
#include "parserUtils.hh"
#include <cstring>
#include <regex>
#include <string>

namespace memorySave {
std::pair<size_t, size_t> range;
std::pair<std::string, std::string> rangetoString() {
  std::pair<std::string, std::string> res;
  std::stringstream stream;
  stream << "0x" << std::hex << range.first;
  res.first = stream.str();
  stream.str("");
  stream << "0x" << std::hex << range.second;
  res.second = stream.str();
  return res;
}
} // namespace memorySave
namespace codeGenerator {

void XMLparser::parseSequence(
    rapidxml::xml_node<> *xmlTag, std::string assID, Assertion &impl,
    const std::string &locVarDec, const std::string &gloVarDec,
    std::unordered_map<size_t, rapidxml::xml_node<> *> &id2tc, bool is_ant) {

  // utility variables
  size_t fragID = 0;
  std::string tmpFormula = "";
  std::string prefix = std::string(is_ant ? "Ant" : "Con");

  // retrieve the sequence of fragments
  std::vector<rapidxml::xml_node<> *> xmlFragments;
  oden::getNodesFromName(xmlTag, "FRAGMENT", xmlFragments);
  messageErrorIf(xmlFragments.empty(),
                 "No fragments defined in assertion" + assID + ", " + prefix);

  // for every fragment
  for (const auto &xmlFragment : xmlFragments) {
    Fragment frag;

    // handle proposition
    tmpFormula = xmlFragment->value();
    /*
     * DEBUG
    messageInfoIf(xmlFragment->is_empty(),
                  "No proposition specified in " +
                      std::string(is_ant ? "antecedent" : "consequent") +
                      ", fragment " + std::to_string(fragID) + " assertion " +
                      std::to_string(assID) +
                      ": all parameters except delay will be ignored.");
                      */
    // assume the fragment to be the true constant if no proposition is defines
    if (xmlFragment->is_empty()) {
      tmpFormula = "1";
    }

    frag._p = std::shared_ptr<oden::Proposition>(oden::parseProposition(
        tmpFormula, locVarDec, gloVarDec,
        "proposition, fragment " + std::to_string(fragID) + ", " + prefix +
            ", assertion " + assID,
        "local declaration, assertion " + assID));

    // keep track of used variables
    for (const auto &var : oden::prop2Vars(*frag._p)) {
      impl._usedVars.insert({var});
    }

    /*the nameToInfo data struct is meant to keep a link between
    'targets'(propositions names) in PSL formulas and the related buchi
    automata*/

    frag._placeHolder = "f" + prefix + std::to_string(fragID);
    impl._nameToInfo.insert({{frag._placeHolder, Info(fragID, -1, is_ant)}});

    // parse the fragment parameters

    // until
    tmpFormula = oden::getAttributeValue(xmlFragment, "until", "");
    if (tmpFormula != "") {
      frag._until = std::shared_ptr<oden::Proposition>(oden::parseProposition(
          tmpFormula, locVarDec, gloVarDec,
          "until_proposition, fragment " + std::to_string(fragID) + ", " +
              prefix + ", assertion " + assID,
          "local declaration, assertion " + assID));

      // keep track of used variables
      for (const auto &var : oden::prop2Vars(*frag._until)) {
        impl._usedVars.insert({var});
      }
      frag._untilPlaceHolder = "uf" + prefix + std::to_string(fragID);
      impl._nameToInfo.insert(
          {{frag._untilPlaceHolder, Info(fragID, -2, is_ant)}});
    }

    // times
    frag._times =
        strToSizeCheckErrors(oden::getAttributeValue(xmlFragment, "times", "0"),
                             "times(ass" + assID + ", " + prefix +
                                 ", fragment" + std::to_string(fragID) + ")");

    // max
    frag._timers.emplace_back(
        "maxTimer" + prefix + std::to_string(fragID), TimerType::Max,
        strToSizeCheckErrors(oden::getAttributeValue(xmlFragment, "max", "0"),
                             "max(ass" + assID + ", " + prefix + ", fragment" +
                                 std::to_string(fragID) + ")"));
    impl._nameToInfo.insert(
        {{frag._timers.back()._name, Info(fragID, 0, is_ant)}});

    // min
    frag._timers.emplace_back(
        "minTimer" + prefix + std::to_string(fragID), TimerType::Min,
        strToSizeCheckErrors(oden::getAttributeValue(xmlFragment, "min", "0"),
                             "min(ass" + assID + ", " + prefix + ", fragment" +
                                 std::to_string(fragID) + ")"));
    impl._nameToInfo.insert(
        {{frag._timers.back()._name, Info(fragID, 1, is_ant)}});

    // delay
    frag._timers.emplace_back(
        "delayTimer" + prefix + std::to_string(fragID), TimerType::Min,
        strToSizeCheckErrors(oden::getAttributeValue(xmlFragment, "delay", "0"),
                             "delay(ass" + assID + ", " + prefix +
                                 ", fragment" + std::to_string(fragID) + ")"));
    impl._nameToInfo.insert(
        {{frag._timers.back()._name, Info(fragID, 2, is_ant)}});

    // manual forced
    frag._manualForced = strToIntCheckErrors(
        oden::getAttributeValue(xmlFragment, "manual_forced", "-1"),
        "manual_forced(ass" + assID + ", " + prefix + ", fragment" +
            std::to_string(fragID) + ")",
        -1);

    // forced
    frag._forced = strToSizeCheckErrors(
        oden::getAttributeValue(xmlFragment, "forced", "0"),
        "forced(ass" + assID + ", " + prefix + ", fragment" +
            std::to_string(fragID) + ")",
        0);

    messageErrorIf(
        frag._manualForced > -1 && frag._forced > 0,
        "Forced and manual forced are mutually exclusive! assertion " + assID +
            ", " + prefix + ", fragment " + std::to_string(fragID));

    if (frag._manualForced > -1) {
      messageErrorIf(id2tc.find(frag._manualForced) == id2tc.end(),
                     "Manual forced " + std::to_string(frag._manualForced) +
                         " in assertion " + assID + ", " + prefix +
                         ", fragment " + std::to_string(fragID) +
                         " does not exist");
      readManualTestcase(frag, id2tc.at(frag._manualForced), assID, fragID,
                         prefix);
    }

    if (is_ant) {
      impl._a.push_back(frag);
    } else {
      impl._c.push_back(frag);
    }

    fragID++;
  } // end for each xmlFragments
}

void XMLparser::readManualTestcase(Fragment &frag,
                                   rapidxml::xml_node<> *testcaseTag,
                                   std::string assID, size_t fragID,
                                   std::string prefix) {

  std::string tcContent;
  // get the content of TEST_CASE tag
  tcContent = testcaseTag->value();
  removeAllSpaces(tcContent);

  // check for some details
  size_t i = tcContent.find('=');
  messageErrorIf(std::count(tcContent.begin(), tcContent.end(), '=') != 1,
                 "Syntax error in manual testcase, missing '=' in assertion " +
                     (assID) + ", " + prefix + ", fragment " +
                     std::to_string(fragID) + ", manual testcase " +
                     std::to_string(strToIntCheckErrors(
                         oden::getAttributeValue(testcaseTag, "id", "-1"))));
  messageErrorIf(std::count(tcContent.begin(), tcContent.end(), '}') != 1,
                 "Syntax error in manual testcase, missing '}' in assertion " +
                     (assID) + ", " + prefix + ", fragment " +
                     std::to_string(fragID) + ", manual testcase " +
                     std::to_string(strToIntCheckErrors(
                         oden::getAttributeValue(testcaseTag, "id", "-1"))));
  messageErrorIf(std::count(tcContent.begin(), tcContent.end(), '{') != 1,
                 "Syntax error in manual testcase, missing '{' in assertion " +
                     (assID) + ", " + prefix + ", fragment " +
                     std::to_string(fragID) + ", manual testcase " +
                     std::to_string(strToIntCheckErrors(
                         oden::getAttributeValue(testcaseTag, "id", "-1"))));

  // error if there is some char after =
  size_t eqPos = tcContent.find('=');
  std::string afterEq = tcContent.substr(eqPos, tcContent.length() - 1);
  messageErrorIf(std::count(afterEq.begin(), afterEq.end(), '[') != 0 ||
                     std::count(afterEq.begin(), afterEq.end(), ']') != 0,
                 "Wrong char after = in assertion " + (assID) + ", " + prefix +
                     ", fragment " + std::to_string(fragID) +
                     ", manual testcase " +
                     std::to_string(strToIntCheckErrors(
                         oden::getAttributeValue(testcaseTag, "id", "-1"))));

  // check for [] possible errors
  std::string sqbr = tcContent.substr(0, eqPos); // sqbr = [P0,P4,P12]
  std::string::iterator end_pos = std::remove(sqbr.begin(), sqbr.end(), '\n');
  sqbr.erase(end_pos, sqbr.end());
  end_pos = std::remove(sqbr.begin(), sqbr.end(), ' ');
  sqbr.erase(end_pos, sqbr.end());

  messageErrorIf(std::count(sqbr.begin(), sqbr.end(), '[') !=
                         std::count(sqbr.begin(), sqbr.end(), ']') ||
                     sqbr.at(0) != '[' || sqbr.at(sqbr.length() - 1) != ']',
                 "Missing '[' or ']' in assertion " + (assID) + ", " + prefix +
                     ", fragment " + std::to_string(fragID) +
                     ", manual testcase " +
                     std::to_string(strToIntCheckErrors(
                         oden::getAttributeValue(testcaseTag, "id", "-1"))));

  while (!(tcContent.at(i) == ']')) {
    i--;
  }
  std::string tbVars = tcContent.substr(
      tcContent.find('[') + 1,
      i - tcContent.find('[') - 1); // tbVars from [P0,P4,P12] to P0,P4,P12

  size_t manVarsNumb = 1;

  // swipe P0,P4,P12 one by one
  while (tbVars.find(',') != std::string::npos) {
    frag._tbVars.push_back(tbVars.substr(0, tbVars.find(',')));
    messageErrorIf(tbVars.substr(0, tbVars.find(',')).empty(),
                   "Empty manual testcase variable in assertion " + (assID) +
                       ", " + prefix + ", fragment " + std::to_string(fragID) +
                       ", manual testcase " +
                       std::to_string(strToIntCheckErrors(
                           oden::getAttributeValue(testcaseTag, "id", "-1"))));

    messageErrorIf(!oden::exists(frag._tbVars.back()),
                   "Use of undecleared var " + frag._tbVars.back() +
                       " in assertion " + (assID) + ", " + prefix +
                       ", fragment " + std::to_string(fragID) +
                       ", manual testcase " +
                       std::to_string(strToIntCheckErrors(
                           oden::getAttributeValue(testcaseTag, "id", "-1"))));
    manVarsNumb++;
    tbVars.erase(0, tbVars.find(',') + 1);
  }
  // now tbVars contains the last variable (e.g. P0)
  frag._tbVars.push_back(tbVars);

  // 1. find all "fragments"
  tcContent = tcContent.erase(0, tcContent.find('{') + 1);
  tcContent = tcContent.erase(tcContent.find('}'), tcContent.length());
  // messageInfo("tcContent: "+tcContent); //(0,16,4,2200),(0,0,0,100);
  messageErrorIf(tcContent.find(';') == std::string::npos,
                 "Syntax error in manual testcase, no testcase found, missing "
                 "';' in assertion " +
                     (assID) + ", " + prefix + ", fragment " +
                     std::to_string(fragID) + ", manual testcase " +
                     std::to_string(strToIntCheckErrors(
                         oden::getAttributeValue(testcaseTag, "id", "-1"))));
  messageErrorIf(tcContent.find('(') == std::string::npos ||
                     tcContent.find('(') == std::string::npos ||
                     std::count(tcContent.begin(), tcContent.end(), '(') !=
                         std::count(tcContent.begin(), tcContent.end(), ')'),
                 "Syntax error in manual testcase, no testcase found, missing "
                 "'(' or ')' in assertion " +
                     (assID) + ", " + prefix + ", fragment " +
                     std::to_string(fragID) + ", manual testcase " +
                     std::to_string(strToIntCheckErrors(
                         oden::getAttributeValue(testcaseTag, "id", "-1"))));

  while (tcContent.find(';') != std::string::npos) {
    std::string singleTc;
    singleTc = tcContent.substr(
        0, tcContent.find(';')); // from (0,16,4,2200),(0,0,0,200); to
                                 // (0,16,4,2200),(0,0,0,200)

    // fill the _testBenches field and all the value that variables take
    std::vector<std::string> fragVec;
    frag._tcList.push_back(0); // initially every tc has no fragments

    while (singleTc.find(')') !=
           std::string::npos) { // inside this while will consider only one
                                // (...) e.g. (0,16,4,2200)
      // messageInfo("sg: "+singleTc); // (0,16,4,2200),(0,0,0,100)
      std::string tbFragm = singleTc.substr(
          singleTc.find('(') + 1, singleTc.find(')') - singleTc.find('(') -
                                      1); // tbFragm: 0,16,4,2200 OK
      size_t manVarsNumb_val = 0;
      messageErrorIf(
          singleTc.find(',') == std::string::npos,
          "Syntax error in manual testcase, no testcase found, missing ',' in "
          "assertion " +
              (assID) + ", " + prefix + ", fragment " + std::to_string(fragID) +
              ", manual testcase " +
              std::to_string(strToIntCheckErrors(
                  oden::getAttributeValue(testcaseTag, "id", "-1"))));

      // slide every element comma separated in tbFrag 0,16,4,2200
      while (tbFragm.find(',') != std::string::npos) {
        messageErrorIf(
            tbFragm.substr(0, tbFragm.find(',')).empty(),
            "Empty manual testcase value in assertion " + (assID) + ", " +
                prefix + ", fragment " + std::to_string(fragID) +
                ", manual testcase " +
                std::to_string(strToIntCheckErrors(
                    oden::getAttributeValue(testcaseTag, "id", "-1"))));
        fragVec.push_back(tbFragm.substr(0, tbFragm.find(','))); // put 0 OK
        tbFragm.erase(0, tbFragm.find(',') +
                             1); // tbFragm: 0,16,4,2200 ---> 16,4,2200
        manVarsNumb_val++;
      }

      messageErrorIf(
          manVarsNumb != manVarsNumb_val,
          "Number of injected values not equal to the number of declared "
          "variables. \nAssertion " +
              (assID) + ", " + prefix + ", fragment " + std::to_string(fragID) +
              ", manual testcase " +
              std::to_string(strToIntCheckErrors(
                  oden::getAttributeValue(testcaseTag, "id", "-1"))) +
              "\n Number of variables: " + std::to_string(manVarsNumb) +
              "\n Number of values: " + std::to_string(manVarsNumb_val));

      fragVec.push_back(tbFragm); // push back 2200

      singleTc = singleTc.erase(
          0,
          singleTc.find(')') +
              2); // singleTc:  (0,16,4,2200),(0,0,0,200) ---> tc:  (0,0,0,200)
      frag._testBenches.push_back(fragVec);
      fragVec.clear();
      frag._tcList.back()++; // sign that this testbench has a fragment
    }                        // end while

    tcContent.erase(0, tcContent.find(';') + 1);
  } // end while that looking for ;

} // end readManualTestcase

void XMLparser::parseInterruptNode(rapidxml::xml_node<> *xmlInterruptsSetup) {
  if (xmlInterruptsSetup != nullptr) {
    std::vector<rapidxml::xml_node<> *> xmlInterrupts;

    oden::getNodesFromName(xmlInterruptsSetup, "INTERRUPT", xmlInterrupts);
    messageErrorIf(xmlInterrupts.empty(), "No interrupts set in setup!");

    for (auto &xmlInterrupt : xmlInterrupts) {
      _interruptsSetup.emplace_back(
          oden::getAttributeValue(xmlInterrupt, "name"),
          strToSizeCheckErrors(
              oden::getAttributeValue(xmlInterrupt, "first_activation"),
              "first_activation"),
          strToSizeCheckErrors(
              oden::getAttributeValue(xmlInterrupt, "repeat_interval"),
              "repeat_interval"),
          strToSizeCheckErrors(
              oden::getAttributeValue(xmlInterrupt, "variance"), "variance", 0,
              100),
          strToSizeCheckErrors(
              oden::getAttributeValue(xmlInterrupt, "infinite_hold_time"),
              "infinite_hold_time", 0, 1),
          strToSizeCheckErrors(
              oden::getAttributeValue(xmlInterrupt, "hold_time"), "hold_time"),
          strToSizeCheckErrors(
              oden::getAttributeValue(xmlInterrupt, "probability"),
              "probability", 0, 100));
    }
  }
}
void XMLparser::parseAssertion(rapidxml::xml_node<> *xmlAssertion,
                               const std::string &globalDecls) {
  std::string tmpFormula = "";

  Assertion impl;

  // retrieve assertion id
  std::string assID = oden::getAttributeValue(xmlAssertion, "id");
  if (constainsIllegalChar(assID)) {
    messageWarning("Substituting illegal characters in : " + assID);
    substituteIllegalChar(assID);
  }

  messageErrorIf(_assertions.count(assID),
                 "Multiple definitions of assertions with id = " + assID);

  // retrieve number of testbenches for the current assertion
  impl._nTB = strToSizeCheckErrors(
      oden::getAttributeValue(xmlAssertion, "nTB", "1"), "nTB");

  // retrieve assertion comment to display in the IAR debug log
  std::vector<rapidxml::xml_node<> *> assCommentTag;
  oden::getNodesFromName(xmlAssertion, "COMMENT", assCommentTag);
  messageErrorIf(assCommentTag.size() > 1,
                 "Multiple comments defined for assertion " + assID);
  messageWarningIf(assCommentTag.empty(),
                   "COMMENT tag not defined for assertion " + assID);
  if (assCommentTag.empty()) {
    impl._comment = "COMMENT <empty>";
  } else {
    impl._comment = assCommentTag.front()->value();
  }
  removeSpacesBeforeAfter(impl._comment);
  removeCR(impl._comment);

  // retrieve local variables declarations
  std::vector<rapidxml::xml_node<> *> localDeclarationsTag;
  oden::getNodesFromName(xmlAssertion, "DECLARATIONS", localDeclarationsTag);
  std::string localDecls = "";
  if (!localDeclarationsTag.empty()) {
    localDecls = localDeclarationsTag.front()->value();
  }

  // precondition
  std::vector<rapidxml::xml_node<> *> fragmentPrecTag;
  oden::getNodesFromName(xmlAssertion, "PRECONDITION", fragmentPrecTag);
  messageErrorIf(fragmentPrecTag.size() > 1,
                 "Multiple definitions of precondition in assertion " + assID +
                     "!");
  messageErrorIf(fragmentPrecTag.empty(),
                 "Undefined precondition in assertion " + assID + "!");
  tmpFormula = fragmentPrecTag.front()->value();

  // parsing the C proposition with the given types
  impl._precondition = std::shared_ptr<oden::Proposition>(
      oden::parseProposition(tmpFormula, localDecls, globalDecls,
                             "precondition, assertion " + assID,
                             "local declaration, assertion " + assID));

  // postcondition
  std::vector<rapidxml::xml_node<> *> fragmentPostTag;
  oden::getNodesFromName(xmlAssertion, "POSTCONDITION", fragmentPostTag);
  messageErrorIf(fragmentPostTag.size() > 1,
                 "Multiple definitions of postcondition in assertion " +
                     (assID) + "!");

  // the posconditio is not mandatory if the testplan is not generated
  if (!fragmentPostTag.empty()) {
    tmpFormula = fragmentPostTag.front()->value();
    impl._postcondition = std::shared_ptr<oden::Proposition>(
        oden::parseProposition(tmpFormula, localDecls, globalDecls,
                               "postcondition, assertion " + (assID),
                               "local declaration, assertion " + (assID)));
  }

  // retrieve manual testcase
  std::vector<rapidxml::xml_node<> *> testcaseTag;
  oden::getNodesFromName(xmlAssertion, "TEST_VECTOR", testcaseTag);
  std::unordered_map<size_t, rapidxml::xml_node<> *> id2tc;

  for (const auto &tbNodeExt : testcaseTag) {
    size_t tcID;
    tcID = strToSizeCheckErrors(oden::getAttributeValue(tbNodeExt, "id", "0"),
                                "forced(ass" + (assID));
    assert(id2tc.count(tcID) == 0);
    id2tc[tcID] = tbNodeExt;
  }

  // antecedent
  std::vector<rapidxml::xml_node<> *> antecedentTag;
  oden::getNodesFromName(xmlAssertion, "ANTECEDENT", antecedentTag);
  messageErrorIf(antecedentTag.size() > 1,
                 "Multiple definitions of antecedent in assertion " + (assID) +
                     "!");
  messageErrorIf(antecedentTag.empty(),
                 "Undefined antecedent in assertion " + (assID) + "!\n");
  // parse the sequence of fragments
  parseSequence(antecedentTag.front(), assID, impl, localDecls, globalDecls,
                id2tc, true);

  // consequent
  std::vector<rapidxml::xml_node<> *> consequentTag;
  oden::getNodesFromName(xmlAssertion, "CONSEQUENT", consequentTag);
  messageErrorIf(consequentTag.size() > 1,
                 "Multiple definitions of consequent in assertion " + (assID) +
                     "!");
  messageErrorIf(consequentTag.empty(),
                 "Undefined consequent in assertion " + (assID) + "!\n");
  parseSequence(consequentTag.front(), assID, impl, localDecls, globalDecls,
                id2tc, false);

  // used to calculate how many timers will be used by IAR to implement the
  // checkers
  updateRequiredTimers(impl);

  // add idea implication
  _assertions.insert({{assID, impl}});

  // generate PSL implication
  _PSLimplications.insert(
      {{assID, PSLimplication(toPSL(impl._a), toPSL(impl._c))}});
}

void XMLparser::updateRequiredTimers(const Assertion &impl) {
  size_t nTimers = 0;
  for (const auto &f : impl._a) {
    if (f._timers[0]._duration != 0) {
      nTimers++;
    }
    if (f._timers[1]._duration > 1) {
      nTimers++;
    }
    if (f._timers[2]._duration != 0) {
      nTimers++;
    }
  }
  _maxTimers = nTimers > _maxTimers ? nTimers : _maxTimers;
  nTimers = 0;
  for (const auto &f : impl._c) {
    if (f._timers[0]._duration != 0) {
      nTimers++;
    }
    if (f._timers[1]._duration > 1) {
      nTimers++;
    }
    if (f._timers[2]._duration != 0) {
      nTimers++;
    }
  }

  _maxTimers = nTimers > _maxTimers ? nTimers : _maxTimers;
}
void XMLparser::parseXML(std::string filePath) {

  // mock variable to store parsed propositions
  std::string tmpFormula;

  rapidxml::file<> *_xmlFile;
  rapidxml::xml_document<> *_doc;

  // find root node
  try {
    _xmlFile = new rapidxml::file<>(filePath.c_str());
    _doc = new rapidxml::xml_document<>();
    _doc->parse<0>(_xmlFile->data());
  } catch (rapidxml::parse_error &e) {
    std::stringstream ss;
    ss << "Parsing error in " << filePath << " "
       << "\n"
       << e.m_what << " at line " << e.m_lineNumber << std::endl;
    if (!reinterpret_cast<char *>(e.m_where)[0]) {
      ss << "Hint: you probably forgot to close a tag\n";
    }
    messageError(ss.str());
  }

  // parse interrupts setup
  rapidxml::xml_node<> *xmlInterruptsSetup =
      _doc->first_node("INTERRUPTS_SETUP");
  parseInterruptNode(xmlInterruptsSetup);

  // specifications
  rapidxml::xml_node<> *xmlSpecifications = _doc->first_node("SPECIFICATIONS");
  if (xmlSpecifications == nullptr) {
    messageError("SPECIFICATIONS node not found!");
  }

  // global variables declarations
  std::vector<rapidxml::xml_node<> *> globalDeclarationsTag;
  oden::getNodesFromName(xmlSpecifications, "DECLARATIONS",
                         globalDeclarationsTag);

  std::string globalDecls = "";

  if (!globalDeclarationsTag.empty()) {
    globalDecls = globalDeclarationsTag.front()->value();
  }

  // parse assertions
  std::vector<rapidxml::xml_node<> *> xmlAssertions;
  oden::getNodesFromName(xmlSpecifications, "ASSERTION", xmlAssertions);

  for (auto &xmlAssertion : xmlAssertions) {
    parseAssertion(xmlAssertion, globalDecls);
  }

  // retrieve configuration
  std::vector<rapidxml::xml_node<> *> xmlConfig;
  oden::getNodesFromName(xmlSpecifications, "CONFIG", xmlConfig);
  messageErrorIf((!_defaultConfiguration && !xmlConfig.empty()) ||
                     (xmlConfig.size() > 1),
                 "Multiple configurations defined!");

  if (!xmlConfig.empty()) {
    _configuration._automatic_testplan_generation = strToSizeCheckErrors(
        oden::getAttributeValue(xmlConfig.front(),
                                "automatic_testplan_generation"),
        "automatic_testplan_generation");

    if (_configuration._automatic_testplan_generation == 1) {
      _defaultConfiguration = false;
      _configuration._first_assertion =
          oden::getAttributeValue(xmlConfig.front(), "first_assertion");
    }
  }

  // retrieve plan
  std::vector<rapidxml::xml_node<> *> xmlPlan;
  oden::getNodesFromName(xmlSpecifications, "PLAN", xmlPlan);
  messageErrorIf((!_plan.empty() && !xmlPlan.empty()) || (xmlPlan.size() > 1),
                 "Multiple test plan defined!");

  if (!xmlPlan.empty()) {
    parsePlan(xmlPlan.front()->value());
  }

  // retrieve starting condition
  std::vector<rapidxml::xml_node<> *> xmlStartingCondition;
  oden::getNodesFromName(xmlSpecifications, "STARTING_CONDITION",
                         xmlStartingCondition);
  messageErrorIf(
      (_starting_condition != nullptr && !xmlStartingCondition.empty()) ||
          (xmlStartingCondition.size() > 1),
      "Multiple starting conditions defined!");

  // retrieve wait condition
  if (!xmlStartingCondition.empty()) {
    std::vector<rapidxml::xml_node<> *> xmlStartTestingIf;
    oden::getNodesFromName(xmlStartingCondition.front(), "START_TESTING_IF",
                           xmlStartTestingIf);
    messageErrorIf((xmlStartTestingIf.empty()),
                   "Undefined start testing condition!");
    messageErrorIf((xmlStartTestingIf.size() > 1),
                   "Multiple start testing conditions defined!");
    tmpFormula = xmlStartTestingIf.front()->value();
    _starting_condition =
        std::shared_ptr<oden::Proposition>(oden::parseProposition(
            tmpFormula, "", globalDecls, "", "if_starting_condition"));

    std::string save_state =
        oden::getAttributeValue(xmlStartTestingIf.front(), "save_state", "");
    if (save_state == "") {
      messageWarning(
          "Save state range not defined! Using default [0x00000,0xfffff]");
      save_state = "[0x00000,0xfffff]";
    }
    removeAllSpaces(save_state);
    messageErrorIf(save_state.front() != '[',
                   "Parse error: expecting '[' in memory_save range");
    messageErrorIf(save_state.back() != ']',
                   "Parse error: expecting ']' in memory_save range");
    auto comma = save_state.find(',', 1);
    messageErrorIf(comma == std::string::npos,
                   "Parse error: expecting ',' in memory_save range");
    size_t lower_bound = hex_strToSizeCheckErrors(
        save_state.substr(1, comma), "lower_bound(save_state)", 0,
        std::numeric_limits<unsigned long long>::max());
    size_t upper_bound = hex_strToSizeCheckErrors(
        save_state.substr(comma + 1, save_state.size() - comma - 2),
        "upper_bound(save_state)", 0,
        std::numeric_limits<unsigned long long>::max());

    memorySave::range = std::make_pair(lower_bound, upper_bound);
  }
} // end parseXML method

const std::unordered_map<std::string, PSLimplication> &
XMLparser::getPSLimplications() {
  return _PSLimplications;
}

std::unordered_map<std::string, Assertion> &XMLparser::getAssertions() {
  return _assertions;
}

const std::vector<Interrupt> &XMLparser::getIARinterruptsSetup() {
  return _interruptsSetup;
}

const std::vector<std::string> &XMLparser::getTestPlan() { return _plan; }
std::string XMLparser::toPSL(const std::vector<Fragment> &fragments) {
  std::stringstream ss;
  ss << "{";
  for (auto &fr : fragments) {
    if (fr._until != nullptr) {
      messageErrorIf(fr._timers[0]._duration == 0,
                     "Max must be set when using the until operator!");
      // DEBUG
      // messageInfo("until paramer set! Ignoring all other parameters except
      // max " "and forced");
      ss << "(" << fr._untilPlaceHolder << ") || ((" << fr._placeHolder
         << " && " << fr._timers[0]._name << " && ~" << fr._untilPlaceHolder
         << ")[+];" << fr._untilPlaceHolder << ");";
    } else if (fr._times > 0) {
      messageErrorIf(fr._times > fr._timers[0]._duration, "Times > Max");
      if (fr._times == 1) {
        ss << "(" << fr._timers[0]._name << ")[*];" << fr._placeHolder << ";";
      } else {
        ss << "(" << fr._placeHolder << ")[->" << fr._times - 1 << "] && ("
           << fr._timers[0]._name << ")[*];";
        ss << "(" << fr._timers[0]._name << ")[*];" << fr._placeHolder << ";";
      }
    } else {
      if (fr._timers[0]._duration == 0 && fr._timers[1]._duration == 1) {
        // simplification of case Min==1 Max==0
        ss << fr._placeHolder << ";";
      } else if (fr._timers[0]._duration != 0 && fr._timers[1]._duration != 0) {
        messageErrorIf(fr._timers[0]._duration < fr._timers[1]._duration,
                       "Min > Max");
        ss << "((" << fr._placeHolder << " && " << fr._timers[1]._name
           << ") || (" << fr._placeHolder << " && " << fr._timers[0]._name
           << ") )[*];((~" << fr._placeHolder << " && ~" << fr._timers[1]._name
           << " && " << fr._timers[0]._name << "));";
      } else if (fr._timers[0]._duration == 0 && fr._timers[1]._duration > 0) {
        ss << "(" << fr._placeHolder << " && " << fr._timers[1]._name
           << ")[*];~" << fr._timers[1]._name << ";";
      } else if (fr._timers[1]._duration == 0 && fr._timers[0]._duration > 0) {
        ss << "(" << fr._placeHolder << " && " << fr._timers[0]._name
           << ")[*];~" << fr._placeHolder << ";";
      } else if (fr._timers[2]._duration == 0 &&
                 (fr._forced != 0 || fr._manualForced != -1)) {
        ss << fr._placeHolder << ";";
      } else if (fr._timers[2]._duration == 0) {
        std::stringstream es;
        es << "min = " << fr._timers[1]._duration << " ";
        es << "max = " << fr._timers[0]._duration << " ";
        es << "delay = " << fr._timers[2]._duration << " ";
        es << "forced = " << fr._forced << " ";
        es << "manual_forced = " << fr._manualForced << " ";
        es << "times = " << fr._times << "\n";
        messageError("Illegal parameters combination:\n" + es.str());
      }
    }

    if (fr._timers[2]._duration != 0) {
      ss << "("
         << "delay"
         << " && " << fr._timers[2]._name << ")[*];~" + fr._timers[2]._name
         << ";";
    }

  } // end for(auto &fr....
  // FIXME: better way to remove last char without copying the string?
  std::string res = ss.str();
  res.pop_back();
  res += "}";
  // DEBUG
  // std::cout << res << "\n";

  return res;
}
void XMLparser::checkErrors() {
  //======WARNINGS & INFOS=======
  std::stringstream info;
  if (_plan.empty()) {
    for (const auto &ID_Ass : _assertions) {
      _plan.push_back(ID_Ass.first);
    }
    std::sort(begin(_plan), end(_plan), std::less<std::string>());
    if (_defaultConfiguration) {
      _configuration._first_assertion = _plan.front();
    }
    if (_configuration._automatic_testplan_generation) {
      messageInfo("Generating a testplan with all assertions starting from " +
                  (_configuration._first_assertion));
    } else {
      messageInfo(
          "Test plan not defined, testing all assertions by increasing id");
    }

  } else {
    // check for duplicates in plan
    std::unordered_map<std::string, size_t> visited;
    for (auto e : _plan) {
      if (visited[e] == 1) {
        if (_configuration._automatic_testplan_generation) {
          messageError("Assertion " + (e) +
                       " appears multiple times in test plan, not allowed with "
                       "automatic generation of testplan!");

        } else {
          messageWarning("Assertion " + (e) +
                         " appears multiple times in test plan!");
        }
        messageWarning("Assertion " + (e) +
                       " appears multiple times in test plan!");
      }
      visited[e]++;
    }
  }

  if (_configuration._automatic_testplan_generation) {
    info << "Generating a test plan with the following set of assertions:";
  } else {
    info << "Test plan:";
  }
  info << "[ ";
  for (auto &e : _plan) {
    info << e << " ";
  }
  info << "]";
  messageInfo(info.str());
  //======WARNINGS & INFOS=======

  //======ERRORS=======
  std::stringstream error;
  bool report = false;
  // errors in fragments

  // postcondition is not mandatory if "_automatic_testplan_generation" is set
  // to 0
  bool postError = false;
  for (const auto &ann : _assertions) {
    if (ann.second._postcondition == nullptr &&
        _configuration._automatic_testplan_generation) {
      report = true;
      postError = true;
      error << "Undefined postcondition in assertion " << (ann.first) << "\n";
    }
  }
  if (postError) {
    error << "!\nDefine a postcondition for all assertions or turn "
             "'_automatic_testplan_generation' to 0\n";
  }
  // interrupt errors
  messageErrorIf(_interruptsSetup.empty(),
                 "Cannot find interrupts setup node!\n");

  std::unordered_map<std::string, size_t> reps;

  for (auto e : _interruptsSetup) {
    reps[e._name]++;
  }

  for (const auto &e : reps) {
    if (e.second > 1) {
      report = true;
      error << "Multiple definitions of interrupt " << e.first << "\n";
    }
  }

  for (auto e : _plan) {
    if (!_assertions.count(e)) {
      report = true;
      error << "Undefined assertion with id = " << e << " in test plan!\n";
    }
  }

  if (!_defaultConfiguration &&
      std::find(begin(_plan), end(_plan), _configuration._first_assertion) ==
          end(_plan)) {
    report = true;
    error << "Starting assertion " << _configuration._first_assertion
          << " is not in plan\n";
  }

  if (_starting_condition == nullptr) {
    messageWarning("Undefined starting condition! Testing will start as soon "
                   "as the simulation begins...");
    _starting_condition =
        std::shared_ptr<oden::Proposition>(new oden::BooleanConstant(true));
    /*
      report = true;
      error << "Undefined starting condition!\n";
    */
  }

  if (report) {
    messageError(error.str());
  }
  //======ERRORS=======
}
const std::pair<std::string, Assertion> XMLparser::getFirstAssertion() {
  return std::make_pair(_configuration._first_assertion,
                        _assertions.at(_configuration._first_assertion));
}
bool XMLparser::isGenerateTestplanSet() {
  return _configuration._automatic_testplan_generation;
}
std::shared_ptr<oden::Proposition> XMLparser::getStartingCondition() {
  return _starting_condition;
}
void XMLparser::parsePlan(const std::string &plan) {
  size_t pos1 = 0;
  size_t pos2 = 0;
  std::string token;
  char delimiter = ',';
  std::string fplan = plan;
  removeAllSpaces(fplan);

  while ((pos2 = fplan.find(delimiter, pos1)) != std::string::npos) {
    token = fplan.substr(pos1, pos2 - pos1);
    pos2++;
    pos1 = pos2;
    substituteIllegalChar(token);
    _plan.push_back(token);
  }
  // last assertion in plan
  auto tmp = fplan.substr(pos1, fplan.size());
  removeSpacesBeforeAfter(tmp);
  substituteIllegalChar(tmp);
  _plan.push_back(tmp);
}

std::unordered_map<std::string, std::string> &XMLparser::getDebugVars() {
  return oden::getDebugVars();
}

} // namespace codeGenerator
