
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#include "Converter.hh"

namespace codeGenerator {

namespace converter {
AutomataImplication generateAutomata(const PSLimplication &imp) {
  spot::parsed_formula ant = spot::parse_infix_psl(imp._ant);
  messageErrorIf(ant.format_errors(std::cerr),
                 "SpotLTL: Syntax errors in antecedent of assertion:\n" +
                     imp._ant);

  spot::parsed_formula con = spot::parse_infix_psl(imp._con);
  messageErrorIf(ant.format_errors(std::cerr),
                 "SpotLTL: Syntax errors in consequent of assertion:\n" +
                     imp._con);

  spot::translator trans;
  trans.set_pref(spot::postprocessor::Complete);

  AutomataImplication res(trans.run(ant.f), trans.run(con.f));

  //    DEBUG
  // print_hoa(std::cout, res._ant) << '\n';
  //  print_hoa(std::cout, res._con) << '\n';

  return res;
}
} // namespace Converter
} // namespace codeGenerator
