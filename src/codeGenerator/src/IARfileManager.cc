
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
 * @file
 * @author
 * Samuele Germiniani, samuele.germiniani@univr.it
 * Moreno Bragaglio, moreno.bragaglio@univr.it
 * @date 2020
 * @version v1.0
 */

#include "IARfileManager.hh"
#include <climits>
#include <experimental/filesystem>

namespace codeGenerator {
IARfileManager::IARfileManager(
    std::map<std::string, IARchecker> &checkers,
    const std::vector<Interrupt> &intsSetup,
    std::shared_ptr<oden::Proposition> beginCondition,
    const std::vector<std::string> &plan, size_t maxTimers)
    : _beginCondition(beginCondition), _checkers(checkers),
      _intsSetup(intsSetup), _plan(plan), _maxTimers(maxTimers) {
  // not to do
}

void IARfileManager::generateFiles(std::string pathToRoot) {
  messageInfo("Dumping files...");
  // create directory tree
  std::string sep = "";
#ifdef WIN
  sep = "\\\\";
#else
  sep = "/";
#endif
  std::string pathToStateDir = "mist.out\\\\savedStates";
  std::string root = pathToRoot + sep + "mist.out";
  if (std::experimental::filesystem::exists(root)) {
    std::experimental::filesystem::remove_all(root);
  }
  std::experimental::filesystem::create_directory(root);
  std::experimental::filesystem::create_directory(root + sep + "checkers");
  std::experimental::filesystem::create_directory(root + sep + "savedStates");

  // create checker implementation
  for (auto &pairID_Checker : _checkers) {
    std::ofstream ofs(root + sep + "checkers" + sep + "checker" + pairID_Checker.second.getID() + ".mac");
    pairID_Checker.second.generateImplementation();
    ofs << pairID_Checker.second.getCode()._data << std::flush;
    ofs << "\n\n\n";
    ofs << pairID_Checker.second.getCode()._code << std::flush;
    ofs.close();
  }

  // create time utilities
  {
    std::ofstream ofs(root + sep + "timerUtility.mac");
    Code tu = generateTimerUtility();
    ofs << tu._data << std::flush;
    ofs << "\n\n\n";
    ofs << tu._code << std::flush;
    ofs.close();
  }

  // create testPlan
  {
    std::ofstream ofs(root + sep + "testPlan.mac");
    Code tu = generateTestPlan(pathToStateDir);
    ofs << tu._data << std::flush;
    ofs << "\n\n\n";
    ofs << tu._code << std::flush;
    ofs.close();
  }

  // create setup simulation
  {
    std::ofstream ofs(root + sep + "SetupSimulation.mac");
    Code sus = generateExecUserSetup();
    Code use = generateUserExit();
    Code wbc = generateWaitBeginCondition(pathToStateDir);
    std::pair<Code, Code> is = generateInterruptsSetup();
    std::string incf = generateIncludeFiles("mist.out");
    Code debugVars = generateDebugVariables();
    Code initv = generateVariableInit();
    std::string sum = printSummary();

    ofs << wbc._data << std::flush;
    ofs << sus._data << std::flush;
    ofs << is.first._data << std::flush;
    ofs << is.second._data << std::flush;
    ofs << debugVars._data << std::flush;
    ofs << "\n\n\n";
    ofs << wbc._code << std::flush;
    ofs << sus._code << std::flush;
    ofs << use._code << std::flush;
    ofs << is.first._code << std::flush;
    ofs << is.second._code << std::flush;
    ofs << sum;
    ofs << incf << std::flush;
    ofs << initv._code << std::flush;

    ofs.close();
  }
}

Code IARfileManager::generateTimerUtility() {
  std::stringstream cs;
  std::stringstream ds;

  // num timers to max index
  _maxTimers = _maxTimers > 0 ? _maxTimers - 1 : 0;
  ds << "__var "
     << "bIdTimers;\n";

  /*
  cs << "//utility functions to handle timers\n";
  cs << "createTimer(timeSpan){\n";
  cs << ident1 << "__var basePtr;\n";
  cs << ident1 << "basePtr=cs_timers;\n";
  cs << ident1 << "__var index;\n";
  cs << ident1 << "index=0;\n";
  cs << ident1 << "//find a free timer\n";
  cs << ident1 << "while(index<" << _maxTimers << " && *basePtr!=-1){\n";
  cs << ident2 << "basePtr++;\n";
  cs << ident2 << "index++;\n";
  cs << ident1 << "}\n";

  cs << ident1 << "if(index==" << _maxTimers << " ){\n";
  cs << ident2 << "__message \"Error: no timers available!\";\n";
  cs << ident2 << "execUserExit();\n";
  cs << ident2 << "__abortLaunch(\"Testing Finished with errors\");\n";
  cs << ident1 << "}\n";

  cs << ident1 << "*basePtr=timeSpan;\n";

  cs << ident1 << "return index;\n";
  cs << "}\n";
  */

  ds << "__var "
     << "activatedTimers;\n";
  ds << "__var "
     << "timer0;\n";
  cs << "createTimer(timeSpan){\n";
  cs << ident1 << "if(activatedTimers>" << _maxTimers << " ){\n";
  cs << ident2 << "__message \"Error: no timers available!\";\n";
  cs << ident2 << "execUserExit();\n";
  cs << ident2 << "__abortLaunch(\"Testing Finished with errors\");\n";
  cs << ident1 << "}\n";
  cs << ident1 << "activatedTimers++;\n";
  cs << ident1 << "if(timer0==-1){\n";
  cs << ident2 << "timer0=timeSpan;\n";
  cs << ident2 << "return 0;\n";
  cs << ident1 << "}";
  for (size_t i = 1; i <= _maxTimers; i++) {
    ds << "__var "
       << "timer" << i << ";\n";
    cs << "else if(timer" << i << "==-1){\n";
    cs << ident2 << "timer" << i << "= timeSpan;\n";
    cs << ident2 << "return " << i << ";\n";
    cs << ident1 << "}";
  }
  cs << "}\n";

  /*
  cs << "updateTimers(){\n";
  cs << ident1 << "__var basePtr;\n";
  cs << ident1 << "basePtr=cs_timers;\n";
  cs << ident1 << "__var i;\n";
  cs << ident1 << "for(i=0;i<" << _maxTimers << ";i++){\n";
  cs << ident2 << "if(*basePtr>0){ (*basePtr)--; }   \n";
  cs << ident3 << "basePtr++;\n";
  cs << ident1 << "}\n";
  cs << "}\n";
  */
  cs << "updateTimers(){\n";
  cs << ident1 << "if(timer0>0){ timer0--;}\n";
  for (size_t i = 1; i <= _maxTimers; i++) {
    cs << ident1 << "if(timer" << i << ">0){ timer" << i << "--;}\n";
  }
  cs << ident1 << "}\n";

  /*
  cs << "eval(timerID){\n";
  cs << ident1 << "if(timerID>=" << _maxTimers << " || timerID<0){\n";
  cs << ident2 << "__message \" Error : index out of bound ! \";\n";
  cs << ident2 << "execUserExit();\n";
  cs << ident2 << "__abortLaunch(\" Testing Finished with errors \");\n";
  cs << ident1 << "}\n";

  cs << ident1 << "__var basePtr;\n";
  cs << ident1 << "basePtr=cs_timers;\n";
  cs << ident1 << "basePtr+=timerID;\n";

  cs << ident1 << "return ((*basePtr)>0?1:0);\n";
  cs << "}\n";
  */

  cs << "eval(timerID){\n";
  cs << ident1 << "if(timerID>" << _maxTimers << " || timerID<0){\n";
  cs << ident2 << "__message \" Error : index out of bound ! \";\n";
  cs << ident2 << "execUserExit();\n";
  cs << ident2 << "__abortLaunch(\" Testing Finished with errors \");\n";
  cs << ident1 << "}\n";
  cs << ident1 << "if(timerID==0){\n";
  cs << ident2 << "return timer0;\n";
  for (size_t i = 1; i <= _maxTimers; i++) {
    cs << ident1 << "}else if(timerID"
       << "==" << i << "){\n";
    cs << ident2 << "return timer" << i << ";\n";
  }
  cs << ident1 << "}\n";

  cs << "}\n";
  /*
  cs << "freeTimer(timerID){\n";
  cs << ident1 << "if(timerID>=" << _maxTimers << " || timerID<0){\n";
  cs << ident2 << "__message \" Error : index out of bound ! \";\n";
  cs << ident2 << "execUserExit();\n";
  cs << ident2 << "__abortLaunch(\" Testing Finished with errors \");\n";
  cs << ident1 << "}\n";

  cs << ident1 << "__var basePtr;\n";
  cs << ident1 << "basePtr=cs_timers;\n";
  cs << ident1 << "basePtr+=timerID;\n";

  cs << ident1 << "*basePtr=-1;\n";
  cs << "}\n";
  */

  cs << "freeTimer(timerID){\n";
  cs << ident1 << "if(timerID>" << _maxTimers << " || timerID<0){\n";
  cs << ident2 << "__message \" Error : index out of bound ! \";\n";
  cs << ident2 << "execUserExit();\n";
  cs << ident2 << "__abortLaunch(\" Testing Finished with errors \");\n";
  cs << ident1 << "}\n";

  cs << ident1 << "activatedTimers--;\n";
  cs << ident1 << "if(timerID==0){\n";
  cs << ident2 << "timer0 = -1;\n";
  cs << ident2 << "return;\n";
  for (size_t i = 1; i <= _maxTimers; i++) {
    cs << ident1 << "}else if(timerID"
       << "==" << i << "){\n";
    cs << ident2 << "timer" << i << " = -1;\n";
    cs << ident2 << "return;\n";
  }
  cs << ident1 << "}\n";
  cs << "}\n";

  cs << "initTimers(){\n";
  for (size_t i = 0; i <= _maxTimers; i++) {
    cs << ident1 << "timer" << i << "=-1;\n";
  }
  cs << "}\n";
  return Code(ds.str(), cs.str());
}

Code IARfileManager::generateExecUserSetup() {
  std::stringstream cs;
  std::stringstream ds;

  ds << "__var bIdupdateTimers;\n";
  ds << "__var repoFile;\n";
  ds << "__var vacCheckers;\n";
  ds << "__var failedCheckers;\n";
  ds << "__var completedCheckers;\n";
  ds << "__var testPlanList;\n";

  cs << "execUserSetup(){\n";
  cs << ident1 << "completedCheckers = 0;\n";
  cs << ident1 << "testPlanList = \"\";\n";
  cs << ident1 << "failedCheckers = 0;\n";
  cs << ident1 << "vacCheckers = 0;\n";
  cs << ident1 << "repoFile = __openFile(\"report.txt\", \"w\");\n";
  cs << ident1 << "includeFiles();\n";
  cs << ident1 << "initVariables();\n";
  cs << ident1 << "initTimers();\n";
  cs << ident1 << "__cancelAllInterrupts();\n";
  cs << ident1 << "setupInterrupts();\n";
  cs << ident1 << "bIdwaitBeginCond"
     << " = __setSimBreak( \"cs_time\", \"W\", \"waitBeginCondition()\");\n";
  cs << ident1 << "bIdupdateTimers"
     << " = __setSimBreak( \"cs_time\", \"W\", \"updateTimers()\");\n";
  cs << "}\n";
  return Code(ds.str(), cs.str());
}
Code IARfileManager::generateUserExit() {
  std::stringstream cs;
  std::stringstream ds;
  std::string st;

  cs << "execUserExit(){\n";
  cs << ident1 << "removeInterrupts();\n";
  cs << ident1 << "__clearBreak(bIdupdateTimers);\n";
  cs << "}\n";
  return Code(ds.str(), cs.str());
}

std::string IARfileManager::printSummary() {
  std::string s;
  s += "printSummary(){\n";
  s += "\t__var fracFail;\n";
  s += "\t__var fracCompl;\n";
  s += "\t__var fracVac;\n";
  s += "\t__var percFail;\n";
  s += "\t__var percCompl;\n";
  s += "\t__var percVac;\n";

  s += "\t__fmessage repoFile, \"\\n\\n\\n################################ "
       "\\n\";\n";
  s += "\t__fmessage repoFile, \"########### SUMMARY ############\\n\";\n";
  s += "\t__fmessage repoFile, \"################################\\n\";\n";
  s += "\t__fmessage repoFile, \" - Number of tests: \", "
       "(completedCheckers+failedCheckers+vacCheckers), \" \\n\";\n";
  s += "\t__fmessage repoFile, \" - Test plan [checker id, TB]: \", "
       "testPlanList, \" \\n\";\n";
  s += "\t__message \"\\n\\n\\n################################ "
       "\\n\";\n";
  s += "\t__message \"########### SUMMARY ############\\n\";\n";
  s += "\t__message \"################################\\n\";\n";
  s += "\t__message \" - Number of tests: \", "
       "(completedCheckers+failedCheckers+vacCheckers), \" \\n\";\n";
  s += "\t__message \" - Test plan [checker id, TB]: \", "
       "testPlanList, \" \\n\";\n";

  s += "\tfracCompl = "
       "((float)completedCheckers)/"
       "((float)completedCheckers+(float)failedCheckers+(float)vacCheckers);\n";
  s += "\tfracFail = "
       "((float)failedCheckers)/"
       "((float)completedCheckers+(float)failedCheckers+(float)vacCheckers);\n";
  s += "\tfracVac = "
       "((float)vacCheckers)/"
       "((float)completedCheckers+(float)failedCheckers+(float)vacCheckers);\n";

  s += "\tif((int)((fracCompl+fracFail+fracVac+0.005)*100.0) > "
       "((int)(fracCompl*100.0) + (int)(fracFail*100.0)+ "
       "(int)(fracVac*100.0))){\n";
  s += "\t\tpercCompl = (int)(fracCompl*100.0) + 1;\n";
  s += "\t}else{\n";
  s += "\t\tpercCompl = (int)(fracCompl*100.0);\n";
  s += "\t}\n";
  s += "\tpercFail = (int)(fracFail*100.0);\n";
  s += "\tpercVac = (int)(fracVac*100.0);\n";
  /*
  s += "\tif((int)((fracCompl+fracFail+fracVac+0.005)*100.0) >
  ((int)(fracCompl*100.0) "
       "+ (int)(fracFail*100.0))){;\n";
  s += "\t\t if((int)((fracCompl+0.005)*100.0) > (int)(fracCompl*100.0)){;\n";
  s += "\t\t\t percCompl = (int)(fracCompl*100.0) + 1;\n";
  s += "\t\t\t percFail = (int)(fracFail*100.0);\n";
  s += "\t\t }";
  s += "else{\n";
  s += "\t\t\t percCompl = (int)(fracCompl*100.0);\n";
  s += "\t\t\t percFail = (int)(fracFail*100.0) + 1;\n";
  s += "\t\t }\n";
  s += "\t}";
  s += "else{\n";
  s += "\t\t percCompl = (int)(fracCompl*100.0);\n";
  s += "\t\t percFail = (int)(fracFail*100.0);\n";
  s += "\t}\n";
  */

  s += "\t__fmessage repoFile, \" - Tests succeded: \", "
       "completedCheckers, \" [\", percCompl, \"%] \\n\";\n";
  s += "\t__fmessage repoFile, \" - Tests failed: \", "
       "failedCheckers, \" [\", percFail, \"%] \\n\";\n";
  s += "\t__fmessage repoFile, \" - Tests vacuously succeded: \", "
       "vacCheckers, \" [\", percVac, \"%] \\n\";\n";
  s += "\t__message \" - Tests succeded: \", "
       "completedCheckers, \" [\", percCompl, \"%] \\n\";\n";
  s += "\t__message \" - Tests failed: \", "
       "failedCheckers, \" [\", percFail, \"%] \\n\";\n";
  s += "\t__message \" - Tests vacuously succeded: \", "
       "vacCheckers, \" [\", percVac, \"%] \\n\";\n";

  s += "}\n";

  return s;
}

Code IARfileManager::generateTestPlan(std::string pathToStateDir) {
  auto range = memorySave::rangetoString();
  std::stringstream cs;
  std::stringstream ds;
  ds << "__var nextChecker;\n";
  ds << "__var nextInPlan;\n";
  ds << "__var enforcePrecondition;\n";
  ds << "__var generatedTestplan;\n";
  ds << "__var recoveryState;\n";

  // var initialization
  _varInit.push_back("nextInPlan = 0;");
  _varInit.push_back("enforcePrecondition = 0;");

  cs << "checkerActivator(){\n";
  //-------Next in plan----------------
  // check if the test plan was generated by MIST
  if (_assIDToRestoreStateID.empty()) {
    _varInit.push_back("generatedTestplan = 0;");
    _varInit.push_back("recoveryState = \"$PROJ_DIR$\\\\" + pathToStateDir +
                       "\\\\recoveryState.txt\";\n");
    cs << ident1 << "if(enforcePrecondition == "
       << "0"
       << "){\n";
    cs << ident2 << "enforcePrecondition = 1;\n";

    cs << ident2 << "__memoryRestore(\"Memory\",\"$PROJ_DIR$\\\\"
       << pathToStateDir << "\\\\memoryStateSafe.txt\");\n";
    cs << ident2 << "if(nextInPlan == "
       << "0"
       << "){\n";

    cs << ident3 << "nextChecker = \"" << _plan.front() << "\";\n";

    //    cs << ident3 << "if(nTestBench" <<
    //    _checkers.at(_plan.front())->getID() << "==0)\n";
    cs << ident4 << "__message \"Checker" << _plan.front() << ": "
       << _checkers.at(_plan.front()).getComment() << "\";\n";
    //    cs << ident3 << "else \n" << ident4 << "__message \"Testbench \",
    //    nTestBench" << _checkers.at(_plan.front())->getID() << ";\n";

    cs << ident3 << "bIdEnforcePrecondition" << _plan.front() << " = -1;\n";
    cs << ident3 << "enforcePrecondition" << _plan.front() << "();\n";

    //    cs << ident3 << "initChecker" << _plan.front() << "();\n";
    cs << ident3 << "return;\n";
    cs << ident2 << "}";
    for (size_t i = 1; i < _plan.size(); i++) {
      cs << "else if(nextInPlan == " << i << "){\n";
      cs << ident3 << "nextChecker = \"" << _plan[i] << "\";\n";

      //      cs << ident3 << "if(nTestBench" << _checkers.at(_plan[i])->getID()
      //      << "==0)\n";
      cs << ident4 << "__message \"Checker" << _plan[i] << ": "
         << _checkers.at(_plan[i]).getComment() << "\";\n";
      //      cs << ident3 << "else \n" << ident4 << "__message \"Testbench \",
      //      nTestBench" << _checkers.at(_plan[i])->getID() << ";\n";

      cs << ident3 << "bIdEnforcePrecondition" << _plan[i] << " = -1;\n";
      cs << ident3 << "enforcePrecondition" << _plan[i] << "();\n";
      //      cs << ident3 << "initChecker" << _plan[i] << "();\n";
      cs << ident3 << "return;\n";
      cs << ident2 << "}";
    }
    cs << "else {\n";
    cs << ident3 << "nextChecker = \"-1\";\n";
    cs << ident2 << "}\n";

    cs << ident1 << "}\n";
    cs << ident1 << "__memorySave(\""+range.first+"\" , \""+range.second+"\",  \"intel-extended\", "
       << "recoveryState);\n";
  } else {

    // automatic generated test-plan
    _varInit.push_back("generatedTestplan = 1;");
    cs << ident1 << "if(enforcePrecondition == "
       << "0"
       << "){\n";
    cs << ident2 << "enforcePrecondition = 1;\n";

    cs << ident2 << "if(nextInPlan == "
       << "0"
       << "){\n";
    /* cs << ident3
        << "__memorySave(\"0x00000\" , \"0xFFFFF\",  \"intel-extended\", "
           "\"$PROJ_DIR$\\\\SimulationSetup\\\\memoryState" +
               std::to_string(_assIDToRestoreStateID.at(_plan[0]).second) +
               "pr" + ".txt\");\n";
     */
    cs << ident3 << "nextChecker = \"" << _plan.front() << "\";\n";

    //    cs << ident3 << "if(nTestBench" <<
    //    _checkers.at(_plan.front())->getID() << "==0)\n";
    cs << ident3 << "__message \"Checker" << _plan.front() << ": "
       << _checkers.at(_plan.front()).getComment() << "\";\n";
    //   cs << ident3 << "else \n" << ident4 << "__message \"Testbench \",
    //   nTestBench" << _checkers.at(_plan.front())->getID() << ";\n";

    cs << ident3 << "recoveryState = "
       << "\"$PROJ_DIR$\\\\" << pathToStateDir << "\\\\memoryState"
       << _plan[0] << +"pr.txt\";\n";
    cs << ident3 << "bIdEnforcePrecondition" << _plan.front() << " = -1;\n";
    cs << ident3 << "enforcePrecondition" << _plan.front() << "();\n";
    cs << ident3
       << "__memorySave(\""+range.first+"\" , \""+range.second+"\",  \"intel-extended\", " "\"$PROJ_DIR$\\\\" + pathToStateDir + "\\\\memoryState" + (_assIDToRestoreStateID.at(_plan[0]).second) +
              "pr" + ".txt\");\n";
    // cs << ident3 << "initChecker" << _plan.front() << "();\n";
    // cs << ident3 << "FSMstate = " +
    // std::to_string(_assIDToNextState.at(_plan.front())) + ";\n";
    cs << ident3 << "return;\n";
    cs << ident2 << "}";
    for (size_t i = 1; i < _plan.size(); i++) {
      cs << "else if(nextInPlan == " << i << "){\n";
      //      cs << ident3 << "if(FSMstate != " <<
      //      std::to_string(_assIDToRestoreState.at(_plan[i])) << "){\n";
      std::string restoreState = "Safe";
      std::string recoveryState =
          "\"$PROJ_DIR$\\\\" + pathToStateDir + "memoryStateSafe.txt\"";
      if (_assIDToRestoreStateID.at(_plan[i]).second != (std::to_string((size_t)-1))) {
        restoreState =
            (_assIDToRestoreStateID.at(_plan[i]).second) +
            (_assIDToRestoreStateID.at(_plan[i]).first ? "pr" : "po");
      }
      recoveryState = "\"$PROJ_DIR$\\\\" + pathToStateDir + "memoryState" +
                      _plan[i] + "pr.txt\"";
      // save last checker postcondition
      /*cs << ident3
         << "__memorySave(\"0x00000\" , \"0xFFFFF\",  \"intel-extended\", "
            "\"$PROJ_DIR$\\\\SimulationSetup\\\\memoryState" +
                std::to_string(_plan[i - 1]) +
                "po"
                ".txt\");\n";
      */
      cs << ident3 << "__memoryRestore(\"Memory\",\"$PROJ_DIR$"
         << "\\\\" << pathToStateDir << "\\\\memoryState" << restoreState
         << ".txt\");\n";
      cs << ident3 << "nextChecker = \"" << _plan[i] << "\";\n";
      //    cs << ident3 << "if(nTestBench" << _checkers.at(_plan[i])->getID()
      //    << "==0)\n";
      cs << ident3 << "__message \"Checker" << _plan[i] << ": "
         << _checkers.at(_plan[i]).getComment() << "\";\n";
      //      cs << ident3 << "else \n" << ident4 << "__message \"Testbench \",
      //      nTestBench" << _checkers.at(_plan[i])->getID() << ";\n";

      cs << ident3 << "recoveryState = " << recoveryState << ";\n";
      cs << ident3 << "bIdEnforcePrecondition" << _plan[i] << " = -1;\n";
      cs << ident3 << "enforcePrecondition" << _plan[i] << "();\n";
      cs << ident3
         << "__memorySave(\""+range.first+"\" , \""+range.second+"\",  \"intel-extended\", "
         << ""
            "\"$PROJ_DIR$\\\\"
         << pathToStateDir
         << "\\\\memoryState" << _plan[i] <<
                "pr"
                ".txt\");\n";
      //      cs << ident3 << "initChecker" << _plan[i] << "();\n";
      /*
      cs << ident3
         << "FSMstate = " + std::to_string(_assIDToNextState.at(_plan[i])) +
                ";\n";
                */
      cs << ident3 << "return;\n";
      cs << ident2 << "}";
    }
    cs << "else {\n";
    cs << ident3 << "nextChecker = \"-1\";\n";
    cs << ident2 << "}\n";

    cs << ident1 << "}\n";
  }
  //-----------------------------------

  cs << ident1 << "enforcePrecondition = 0;\n";

  cs << ident1 << "if(nextChecker == \"" << _plan.front() << "\"){\n";
  cs << ident2 << "initChecker" << _plan.front() << "();\n";
  cs << ident2 << "bIdChecker" << _plan.front()
     << " = __setSimBreak( \"cs_time\", \"W\", \"checker" << _plan.front()
     << "()"
     << "\");\n";

  ds << "__var bIdChecker" << _plan.front() << ";\n";
  cs << ident2 << "checker" << _plan.front() << "();\n";
  cs << ident2 << "return;\n";
  cs << ident1 << "}";
  for (size_t i = 1; i < _plan.size(); i++) {
    cs << "else if(nextChecker == \"" << _plan[i] << "\"){\n";
    cs << ident2 << "initChecker" << _plan[i] << "();\n";
    cs << ident2 << "bIdChecker" << _plan[i]
       << " = __setSimBreak( \"cs_time\", \"W\", \"checker" << _plan[i] << "()"
       << "\");\n";
    ds << "__var bIdChecker" << _plan[i] << ";\n";
    cs << ident2 << "checker" << _plan[i] << "();\n";
    cs << ident2 << "return;\n";
    cs << ident1 << "}";
  }

  cs << "\n" << ident1 << "printSummary();\n";
  cs << "\n" << ident1 << "__message \"Testing Finished!\";\n";
  cs << "}\n";
  return Code(ds.str(), cs.str());
}
std::pair<Code, Code> IARfileManager::generateInterruptsSetup() {
  std::stringstream cs1;
  std::stringstream cs2;
  std::stringstream ds1;
  std::stringstream ds2;

  cs1 << "setupInterrupts(){\n";
  cs1 << ident1 << "__message \"Setting interrupts...\";\n\n";
  cs2 << "removeInterrupts(){\n";
  cs2 << ident1 << "__message \"Removing interrupts...\";\n\n";

  for (auto &i : _intsSetup) {
    ds1 << "__var bId" << i._name << ";\n";
    cs1 << ident1 << "bId" << i._name << " = "
        << "__orderInterrupt"
        << "(\"" << i._name << "\", " << i._first_activation << ", "
        << i._repeat_interval << ", " << i._variance << ", "
        << i._infinite_hold_time << ", " << i._hold_time << ", "
        << i._probability << ");\n";

    cs1 << ident1 << "if("
        << "bId" << i._name << " == -1)\n";
    cs1 << ident2 << "__message \"ERROR: failed to set up the interrupt"
        << "bId" << i._name << "\";\n\n";
    cs2 << ident1 << "if("
        << "bId" << i._name << " != -1)\n";
    cs2 << ident2 << "__cancelInterrupt(bId" << i._name << ");\n\n";
  }
  cs1 << "}\n";
  cs2 << "}\n";

  return std::make_pair(Code(ds1.str(), cs1.str()), Code(ds2.str(), cs2.str()));
}
Code IARfileManager::generateWaitBeginCondition(std::string pathToStateDir) {
  auto range = memorySave::rangetoString();
  std::stringstream cs;
  std::stringstream ds;
  ds << "__var bIdwaitBeginCond;\n";
  cs << "waitBeginCondition(){\n";
  cs << ident1 << "if(cs_time == 1){\n";
  cs << ident2 << "__message \"Waiting for safe state...\";\n";
  cs << ident2 << "__memoryRestore(\"Memory\",\"$PROJ_DIR$\\\\"
     << pathToStateDir << "\\\\memoryStateSafe.txt\");\n";
  cs << ident1 << "}\n";

  // cs << ident1 << "if(" << oden::prop2String(*_beginCondition) << "){\n";
  cs << ident1 << "if(" << oden::prop2String(*_beginCondition) << "){\n";
  cs << ident2 << "__clearBreak(bIdwaitBeginCond);\n";
  cs << ident2 << "__message \"Safe state reached! Begining testing...\";\n";
  cs << ident2
     << "__memorySave(\""+range.first+"\" , \""+range.second+"\",  \"intel-extended\", "
     << "\"$PROJ_DIR$\\\\" << pathToStateDir << "\\\\memoryStateSafe.txt\");\n";
  cs << ident2 << "checkerActivator();\n";
  cs << ident1 << "}\n";
  cs << "}\n";

  return Code(ds.str(), cs.str());
}
Code IARfileManager::generateVariableInit() {
  std::stringstream cs;
  std::stringstream ds;

  cs << "initVariables(){\n";
  for (const std::string &vari : _varInit) {
    cs << ident1 << vari << "\n";
  }
  cs << "}\n";
  return Code(ds.str(), cs.str());
}
std::string IARfileManager::generateIncludeFiles(const std::string &root) {
  std::stringstream ss;
  ss << "includeFiles(){\n";

  ss << ident1 << "__registerMacroFile(\"$PROJ_DIR$\\\\" << root
     << "\\\\testPlan"
     << ".mac\");\n";
  ss << ident1 << "__registerMacroFile(\"$PROJ_DIR$\\\\" << root
     << "\\\\timerUtility"
     << ".mac\");\n";
  for (const auto &c_ID : _checkers) {
    ss << ident1 << "__registerMacroFile(\"$PROJ_DIR$\\\\" << root
       << "\\\\checkers\\\\checker" << c_ID.first << ".mac\");\n";
  }
  ss << "}\n";
  return ss.str();
}
void IARfileManager::setDebugVariables(
    const std::unordered_map<std::string, std::string> &debugVars) {
  _debugVars = debugVars;
}

Code IARfileManager::generateDebugVariables() {
  std::stringstream ds;
  for (const auto &var : _debugVars) {
    ds << "__var " << var.first << ";\n";
    //      _varInit.push_back(var.first+" = "+var.second+";\n");
  }
  return Code(ds.str(), "");
}
void IARfileManager::setRestoreState(
    std::unordered_map<std::string, std::pair<bool, std::string>>
        &assIDtoRestoreStateID) {
  _assIDToRestoreStateID = assIDtoRestoreStateID;
}
void IARfileManager::setTestplan(const std::vector<std::string> &plan) {
  _plan = plan;
}
void IARfileManager::setIDtoNextState(
    std::map<std::string, size_t> assIDToNextState) {
  _assIDToNextState = assIDToNextState;
}
} // namespace codeGenerator
