
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
 * @file
 * @author
 * Samuele Germiniani, samuele.germiniani@univr.it
 * Moreno Bragaglio, moreno.bragaglio@univr.it
 * @date 2020
 * @version v1.0
 */

#include "IARchecker.hh"
#include "parserUtils.hh"
#include "testCaseGenerator.hh"
#include <spot/twa/bddprint.hh>

namespace codeGenerator {

IARchecker::IARchecker(const AutomataImplication &imp, Assertion &ass,
                       std::string id)
    : _imp(imp), _ass(ass), _id(id) {
  // not todo
}

void IARchecker::generateRecovery() {
  std::stringstream cs;
  std::stringstream ds;

  generateEnforcePostcondition();

  cs << "recover" << _id << "(){\n";

  cs << ident1 << "__memoryRestore(\"Memory\",recoveryState);\n";
  cs << ident1 << "enforcePostcondition" << _id << "();\n";
  cs << "}\n";

  _implementation._code += cs.str();
  _implementation._data += ds.str();
}
void IARchecker::generateImplementation() {
  // handler for the Ant and Con automatons
  // generate code for the automatons implementing the implication
  synthesizeAutomata(_imp._ant, "Ant");
  synthesizeAutomata(_imp._con, "Con");

  generateImplicationHandler();

  // generate code to enforce the precondition
  generateEnforcePrecondition();

  // generate code to enforce the postcondition when a checker fails
  if (_ass._postcondition != nullptr) {
    generateRecovery();
  }

  // method to init the checker's variables
  generateInitChecker();
}
void IARchecker::generateImplicationHandler() {
  std::stringstream cs;
  std::stringstream ds;

  // var initialization
  _varInit.push_back("phase" + _id + " = 0;");

  // automata handler : check the result of each instant of evaluation
  // the checker succeed if the antecedent is true and the consequent is false
  cs << "checker" << _id << "(){\n";
  cs << ident1 << "__var res;\n";
  cs << ident1 << "if(phase" << _id << " == 0){\n";
  cs << ident2 << "res = "
     << "checkerAnt" << _id << "();\n";
  cs << ident2 << "if(res == 1){\n";
  cs << ident3 << "phase" << _id << "++;\n";
  // cs << ident3 << "__message phase" << _id <<  ";\n";
  cs << ident3 << "__message \"Antecedent of checker " << _id << " true!\""
     << ";\n";
  cs << ident3 << "checker" << _id << "();\n";
  cs << ident3 << "return;\n";
  cs << ident2 << "}else if(res == 0){\n";
  cs << ident3 << "__message \"Antecedent of checker " << _id << " false!\""
     << ";\n";
  cs << ident3 << "vacCheckers = vacCheckers + 1"
     << ";\n";
  cs << ident3 << "testPlanList += __smessage \"[" << _id
     << ",\", nTestBench" << _id << ",\"] \";\n";
  cs << ident3 << "phase" << _id << " = -1;\n";
  cs << ident2 << "}";
  cs << "else {\n";
  cs << ident3 << "return;\n";
  cs << ident2 << "}\n";
  cs << ident1 << "}else if(phase" << _id << " == 1){\n";
  cs << ident2 << "res = "
     << "checkerCon" << _id << "();\n";
  cs << ident2 << "if(res == 1){\n";
  cs << ident3 << "phase" << _id << " = 2;\n";
  cs << ident3 << "__message \"Consequent of checker " << _id << " true!\""
     << ";\n";
  cs << ident3 << "completedCheckers = completedCheckers + 1"
     << ";\n";
  cs << ident3 << "testPlanList += __smessage \"[" << _id << ",\", nTestBench" << _id << ",\"] \";\n";
  cs << ident2 << "}else if(res == 0){\n";
  cs << ident3 << "phase" << _id << " = -1;\n";
  cs << ident3 << "failedCheckers = failedCheckers + 1"
     << ";\n";
  cs << ident3 << "testPlanList += __smessage \"[" << _id << ",\", nTestBench" << _id << ",\"] \";\n";
  cs << ident2 << "}";
  cs << "else{\n";
  cs << ident3 << "return;\n";
  cs << ident2 << "}\n";
  cs << ident1 << "}\n";

  cs << ident1 << "if(phase" << _id << " == -1 || phase" << _id << " == 2){\n";
  for (auto &f : _forcesToStop) {
    cs << ident2 << "if(bIdforce" << f << " != -1"
       << " ){\n";
    // cs << ident3 << "__clearBreak(" << "bIdforce" << f << ");\n";
    cs << ident3 << "force" << f << "(1);\n";
    cs << ident2 << "}\n";
  }
  _forcesToStop.clear();

  cs << ident2 << "if(nTestBench" << _id << " >= " << _ass._nTB - 1 << " ){\n";
  cs << ident3 << "nextInPlan++;\n";
  cs << ident2 << "}else{\n";
  cs << ident3 << "__memoryRestore(\"Memory\",recoveryState);\n";
  cs << ident3 << "nTestBench" << _id << "++;\n";
  cs << ident3 << "initChecker" << _id << "();\n";
  cs << ident3 << "return;\n";
  cs << ident2 << "}\n";

  cs << ident2 << "__clearBreak("
     << "bIdChecker" << _id << ");\n";
  cs << ident2 << "if(phase" << _id << " == 2 || generatedTestplan == 0){\n";
  cs << ident3 << "checkerActivator();\n";
  cs << ident2 << "}";
  cs << "else if(phase" << _id << " == -1){\n";
  cs << ident3 << "recover" << _id << "();\n";
  cs << ident2 << "}else{\n";
  cs << ident3 << "__abortLaunch(\"Error: unknown phase!\");\n";
  cs << ident2 << "}\n";
  cs << ident1 << "}\n";
  cs << "}\n";
  ds << "__var phase" << _id << ";\n";
  ds << "__var nTestBench" << _id << ";\n";

  _implementation._code += cs.str();
  _implementation._data += ds.str();
}
void IARchecker::generateInitChecker() {
  std::stringstream cs;
  std::stringstream ds;

  // utility to init all checkers variables
  cs << "initChecker" << _id << "(){\n";
  for (const std::string &vari : _varInit) {
    cs << ident1 << vari << "\n";
  }
  for (const std::string &var : _ass._usedVars) {
    if (oden::getDebugVars().count(var)) {
      cs << ident1 << var << " = " << oden::getDebugVars().at(var) << ";\n";
    }
  }
  cs << "}\n";

  _implementation._code += cs.str();
  _implementation._data += ds.str();
}
void IARchecker::addNextStateFunction(const spot::formula &f,
                                      const std::string &nextStateVar,
                                      size_t nextState,
                                      const std::string &prefix,
                                      const spot::twa_graph_ptr &aut) {
  std::stringstream cs;
  std::stringstream ds;

  //============next state function=========

  // first : data for the current edge
  // second : code for the current edge
  std::pair<std::stringstream, std::stringstream> decoration;

  // decorate the spot formulas as follows: substitue placeholders,
  // add start and stop timers, add forces.
  std::string decoratedEdge = visitFormula(f, decoration);

  ds << decoration.first.str();
  cs << decoration.second.str();

  // add next-state function only for propositions that can be true
  if (decoratedEdge != "!1" && decoratedEdge != "0" && decoratedEdge != "fo") {
    cs << ident2 << "if(" << decoratedEdge << "){\n";
    cs << ident3 << nextStateVar << " = " << nextState << ";\n";

    // if the function would change the state to a rejecting state,
    // add a log message to warn the user of the failure
    if (isFinalState(aut, nextState)) {
      if (!aut->state_is_accepting(nextState)) {
        // add log message if fragment fails
        printCheckerResult(prefix, cs);
      }
      // if the next state function would change the state to a
      // final state then we want to have the result of the
      // checker immediately
      cs << ident3 << "return "
         << "checker" << prefix << _id << "()"
         << ";"
         << "\n";
    } else {
      // Unknown, keep going...
      cs << ident3 << "return " << 2 << ";"
         << "\n";
    }
    cs << ident2 << "}"
       << "\n";
  }

  // eval directly the next state if the fragment needed only to force
  // values
  if (decoratedEdge == "fo") {
    cs << ident2 << nextStateVar << " = " << nextState << ";\n";
    cs << ident2 << "return "
       << "checker" << prefix << _id << "()"
       << ";"
       << "\n";
  }
  //=========================================

  _implementation._code += cs.str();
  _implementation._data += ds.str();
}

void IARchecker::printCheckerResult(const std::string &prefix,
                                    std::stringstream &cs) {
  std::string extendedPrefix;

  extendedPrefix = (prefix == "Ant") ? "Antecedent" : "Consequent";

  if (_lastDecoratedFragmentID != _lastDecoratedUntilFragmentID) {
    cs << ident3
       << "__fmessage repoFile, "
          "\"\\n\\n======================================================="
          "\\n\";"
          "\n";
    cs << ident3 << "__fmessage repoFile, \"==================== [CHECKER #"
       << _id << "] =====================\";\n";
    cs << ident3
       << "__fmessage repoFile, "
          "\"\\n=======================================================\";\n";

    cs << ident3 << "__fmessage repoFile, \""
       << "\\n"
       << " FRAGMENT FALSE in Checker " << _id << ", " << extendedPrefix
       << ", Fragment " << _lastDecoratedFragmentID
       << ", Testbench \", nTestBench" << _id
       << ",\"\\n\\t\\t\\t - Proposition "
       << oden::prop2String(*_lastDecoratedProposition) << " is false!"
       << " \\n\";\n";
    cs << ident3 << "__message \""
       << "\\n"
       << " FRAGMENT FALSE in Checker " << _id << ", " << extendedPrefix
       << ", Fragment " << _lastDecoratedFragmentID
       << ", Testbench \", nTestBench" << _id
       << ",\"\\n\\t\\t\\t - Proposition "
       << oden::prop2String(*_lastDecoratedProposition) << " is false!"
       << " \\n\";\n";

    auto vars = oden::prop2VarsWithType(*_lastDecoratedProposition);
    // remove duplicates
    std::sort(begin(vars), end(vars), [](const auto &e1, const auto &e2) {
      return e1.first < e2.first;
    });
    vars.erase(std::unique(begin(vars), end(vars)), end(vars));
    // assert(!vars.empty());
    cs << ident3
       << "__fmessage repoFile, \"\\t\\t\\t - Failed evaluation:\\n\";\n";
    cs << ident3 << "__message \"\\t\\t\\t - Failed evaluation: \";\n";
    for (auto &var : vars) {
      std::string outputFortmat = "";
      if (var.second == oden::VarType::SLogic ||
          var.second == oden::VarType::ULogic ||
          var.second == oden::VarType::Numeric) {
        outputFortmat = "%d";
      } else {
        messageError("Unknown formatting output!");
      }
      cs << ident3 << "__fmessage repoFile, \"\\t\\t\\t\\t\\t\\t" << var.first
         << " = \", " << var.first << ":" << outputFortmat << " , \"\\n\";\n";
      cs << ident3 << "__message \"\\t\\t\\t\\t\\t\\t" << var.first << " = \", "
         << var.first << ":" << outputFortmat << " , \"\\n\";\n";
    }
    cs << ident3 << "__fmessage repoFile, \"\\n\";\n";
    cs << ident3 << "__message \"\\n\";\n";

  } else {
    // until fragment
    cs << ident3
       << "__fmessage repoFile, "
          "\"\\n\\n======================================================="
          "\\n\";"
          "\n";
    cs << ident3 << "__fmessage repoFile, \"==================== [CHECKER #"
       << _id << "] =====================\";\n";
    cs << ident3
       << "__fmessage repoFile, "
          "\"\\n=======================================================\";\n";

    cs << ident3 << "__fmessage repoFile, \""
       << "\\n"
       << " FRAGMENT FALSE in Checker " << _id << ", " << extendedPrefix
       << ", Fragment " << _lastDecoratedFragmentID
       << ", Testbench \", nTestBench" << _id
       << ",\"\\n\\t\\t\\t - Proposition "
       << "\\n\\t\\t\\t - Proposition "
       << oden::prop2String(*_lastDecoratedProposition) << " until "
       << oden::prop2String(*_lastDecoratedUntilProposition) << " is false!"
       << " \\n\";\n";

    cs << ident3 << "__message \""
       << "\\n"
       << " FRAGMENT FALSE in Checker " << _id << ", " << extendedPrefix
       << ", Fragment " << _lastDecoratedFragmentID
       << ", Testbench \", nTestBench" << _id
       << ",\"\\n\\t\\t\\t - Proposition "
       << "\\n\\t\\t\\t - Proposition "
       << oden::prop2String(*_lastDecoratedProposition) << " until "
       << oden::prop2String(*_lastDecoratedUntilProposition) << " is false!"
       << " \\n\";\n";

    Timer maxTimer;
    if (prefix == "Ant") {
      maxTimer = _ass._a[_lastDecoratedFragmentID]._timers[0];
    } else {
      maxTimer = _ass._c[_lastDecoratedFragmentID]._timers[0];
    }

    cs << ident3 << "if(eval(" << maxTimer._name << "SocketAss" << _id
       << ") == 0"
       << "){\n";
    cs << ident4 << "__fmessage repoFile, \"\\t\\t\\t - Timer ran out!\\n\";\n";
    cs << ident4 << "__message \"\\t\\t\\t - Timer ran out! \";\n";

    cs << ident3 << "}else{\n";

    auto vars = oden::prop2VarsWithType(*_lastDecoratedProposition);
    auto vars2 = oden::prop2VarsWithType(*_lastDecoratedUntilProposition);
    vars.insert(end(vars), begin(vars2), end(vars2));
    std::sort(begin(vars), end(vars), [](const auto &e1, const auto &e2) {
      return e1.first < e2.first;
    });
    vars.erase(std::unique(begin(vars), end(vars)), end(vars));

    cs << ident4
       << "__fmessage repoFile, \"\\t\\t\\t - Failed evaluation:\\n\";\n";
    cs << ident4 << "__message \"\\t\\t\\t - Failed evaluation: \";\n";
    for (auto &var : vars) {
      std::string outputFortmat = "";
      if (var.second == oden::VarType::SLogic ||
          var.second == oden::VarType::ULogic ||
          var.second == oden::VarType::Numeric) {
        outputFortmat = "%d";
      } else {
        messageError("Unknown formatting output!");
      }
      cs << ident4 << "__fmessage repoFile, \"\\t\\t\\t\\t\\t\\t" << var.first
         << " = \", " << var.first << ":" << outputFortmat << " , \"\\n\";\n";
      cs << ident4 << "__message \"\\t\\t\\t\\t\\t\\t" << var.first << " = \", "
         << var.first << ":" << outputFortmat << " , \"\\n\";\n";
    }
    cs << ident4 << "__fmessage repoFile, \"\\n\";\n";
    cs << ident4 << "__message \"\\n\";\n";
    cs << ident3 << "}\n";
  }
}

void IARchecker::synthesizeAutomata(const spot::twa_graph_ptr &aut,
                                    const std::string &prefix) {
  std::stringstream cs;
  std::stringstream ds;

  // init to different values
  _lastDecoratedFragmentID = 0;
  _lastDecoratedUntilFragmentID = 1ULL << 32;

  auto initState = aut->get_init_state();
  unsigned initStateU = strToSizeCheckErrors(
      aut->format_state(aut->get_init_state()), "initStateU");

  // var decl for automata current state
  _implementation._data += "__var state" + prefix + _id + ";\n";
  // var initialization
  _varInit.push_back("state" + prefix + _id + " = " +
                     std::to_string(initStateU) + ";");

  // method definition
  _implementation._code += "checker" + prefix + _id + "(){\n";

  // dictionary to extract edge propositions
  auto bddDict = aut->get_dict();
  // keep track of visited states
  std::unordered_set<unsigned> visited;
  // next states to be visited
  std::deque<const spot::state *> fringe;
  std::vector<unsigned> finalStates;
  // used to implement the state function
  std::string nextStateVar = "state" + prefix + _id;

  // visit the automata starting from the initial state(BFS)
  fringe.push_front(initState);
  visited.insert({initStateU});
  while (!fringe.empty()) {
    unsigned currState =
        strToSizeCheckErrors(aut->format_state(fringe.back()), "currState");

    // add to fringe all states reachable from this state out edges
    // a state can be added to fringe only once
    addAllSuccessorsStates(fringe.back(), fringe, visited, aut, finalStates);

    // process the current state
    fringe.pop_back();

    // what to do in the current state
    _implementation._code +=
        (initStateU == currState ? ident1 + std::string("if")
                                 : std::string("else if")) +
        "(" + nextStateVar + " == " + std::to_string(currState) + "){\n";

    // visit out edges of the current state: we need to retrieve the
    // proposition on the edges
    for (auto &out_edge : aut->out(currState)) {
      // DEBUG
      // spot::bdd_print_formula(std::cout, bddDict,
      // out_edge.cond); std::cout << "\n";
      size_t nextState =
          strToSizeCheckErrors(aut->format_state(out_edge.dst), "nextState");

      // FIXME:this is a total hack
      // obtain a spot formula from the string representation of the edge
      spot::formula f =
          spot::parse_formula(spot::bdd_format_formula(bddDict, out_edge.cond));

      addNextStateFunction(f, nextStateVar, nextState, prefix, aut);
    }
    // end of this state code
    _implementation._code += ident1 + "}";
  } // while fringe

  handleFinalStates(nextStateVar, finalStates);
  handleReturnValues(aut, nextStateVar, finalStates);

  _implementation._code += cs.str();
  _implementation._data += ds.str();
  _implementation._code += _methods._code;
  _implementation._data += _methods._data;
  _methods = Code();
}
void IARchecker::addAllSuccessorsStates(const spot::state *,
                                        std::deque<const spot::state *> &fringe,
                                        std::unordered_set<unsigned> &visited,
                                        const spot::twa_graph_ptr &aut,
                                        std::vector<unsigned> &finalStates) {
  // iterable to all next states from currState
  auto itable = aut->succ(fringe.back());
  // add new states to fringe
  for (auto s : itable) {
    unsigned uState =
        strToSizeCheckErrors(aut->format_state(s->dst()), "uSate");
    if (visited.count(uState) == 0) {
      // mark as visited
      visited.insert({uState});
      // final state?handle later...
      if (isFinalState(aut, uState)) {
        finalStates.push_back(uState);
      } else {
        fringe.push_front(s->dst());
      }
    } // visited.count
  }   // itable
}
void IARchecker::handleReturnValues(const spot::twa_graph_ptr &aut,
                                    const std::string &nextStateVar,
                                    const std::vector<unsigned> &finalStates) {
  std::stringstream cs;
  std::stringstream ds;

  size_t accStateIndex = (aut->state_is_accepting(finalStates[0])) ? 0 : 1;
  // handle return values
  cs << ident2 << "if(" << nextStateVar << " == " << finalStates[accStateIndex]
     << " ){\n";
  cs << ident3 << "return " << 1 << ";"
     << "\n";
  cs << ident2 << "}\n";
  cs << ident2 << "if(" << nextStateVar
     << " == " << finalStates[(accStateIndex + 1) % 2] << " ){\n";
  cs << ident3 << "return " << 0 << ";\n";
  cs << ident2 << "}\n";

  cs << ident1 << "}\n";

  cs << ident1 << "return -1;\n";

  cs << "}\n";

  _implementation._code += cs.str();
  _implementation._data += ds.str();
}
void IARchecker::handleFinalStates(const std::string &nextStateVar,
                                   const std::vector<unsigned> &finalStates) {
  std::stringstream cs;
  std::stringstream ds;

  // handle final states
  assert(finalStates.size() == 2);
  cs << "else if(" << nextStateVar << " == " << finalStates[0] << " || "
     << nextStateVar << " == " << finalStates[1] << " ){\n";
  for (auto timer : _timersToFree) {
    std::string socket = timer + "SocketAss" + _id;
    // free timers
    cs << ident2 << "if(" << socket << " != -1){\n";
    cs << ident3 << "freeTimer(" << socket << ");\n";
    cs << ident3 << socket << "= -1;\n";
    cs << ident2 << "}\n";
  }

  // FIXME
  // stop forces
  if (false) {
    for (auto forceID : _forcesToStop) {
      // free timers
      cs << ident2 << "if(forceTime" << forceID << " > 0){\n";
      cs << ident3 << "force" << forceID << "(1);\n";
      cs << ident2 << "}\n";
    }
  }

  // FIXME
  // stop last force
  if (false) {
    if (_lastForceID != "") {
      cs << ident2 << "if(forceTime" << _lastForceID << " > 0){\n";
      cs << ident3 << "force" << _lastForceID << "(1);\n";
      cs << ident2 << "}\n";
    }
  }

  _implementation._code += cs.str();
  _implementation._data += ds.str();

  _timersToFree.clear();
  //  _forcesToStop.clear();
}
// returns the annotated formula
std::string IARchecker::visitFormula(
    spot::formula f,
    std::pair<std::stringstream, std::stringstream> &decoration) {

  // formula && formula
  if (f.is(spot::op::And)) {
    assert(f.size() >= 2);
    std::string tmp = "(";
    for (size_t i = 0; i < f.size(); i++) {
      std::string res = visitFormula(f[i], decoration);
      // semplify cases where a '0' would make the whole formula false
      if (res == "0") {
        return "0";
      }
      // semplify true constants in &&s
      if (res != "1") {
        tmp += res;
        if (i + 1 < f.size()) {
          tmp += " && ";
        }
      }
    }

    return tmp + ")";
    // return "(" + visitFormula(f[0], decoration) + " && " + visitFormula(f[1],
    // decoration) + ")";
  }

  // formula || formula
  if (f.is(spot::op::Or)) {
    assert(f.size() >= 2);
    std::string tmp = "(";
    for (size_t i = 0; i < f.size(); i++) {
      tmp += visitFormula(f[i], decoration);
      if (i + 1 < f.size()) {
        tmp += " || ";
      }
    }

    return tmp + ")";
    // return "(" + visitFormula(f[0], decoration) + " || " + visitFormula(f[1],
    // decoration) + ")";
  }

  //! placeholder
  if (f.is(spot::op::Not) && f[0].is(spot::op::ap)) {
    std::string name = f[0].ap_name();
    if (name == "delay") {
      return "0";
    }
    Fragment &e = nameToFragment(f[0].ap_name(), _ass);
    // keep track of last decorated proposition to implement the prints
    _lastDecoratedProposition = e._p;
    _lastDecoratedFragmentID = nameToFragmentID(f[0].ap_name(), _ass);
    int type = nameToType(f[0].ap_name(), _ass);

    if ((e._forced > 0 || e._manualForced != -1) &&
        e._timers[0]._duration == 0 && e._timers[1]._duration == 0 &&
        e._timers[2]._duration == 0) {
      return "0";
    }

    if (type == -1) {
      // name is a proposition: replace placeholder
      return "!" + oden::prop2String(*e._p);
    } else if (type == -2) {
      // name is an until proposition: replace placeholder
      // keep track of last decorated until proposition to implement the prints
      _lastDecoratedUntilProposition = e._until;
      _lastDecoratedUntilFragmentID = nameToFragmentID(f[0].ap_name(), _ass);
      return "!" + oden::prop2String(*e._until);
    } else {
      // name is a timer: evaluate timer
      return "!eval(" + name + "SocketAss" + _id + ") == 1";
    }
    return "!" + visitFormula(f[0], decoration);
  }

  // placeholder
  if (f.is(spot::op::ap)) {
    std::string name = f.ap_name();

    //'delay' is just a placeholder for the 'true' constant
    if (name == "delay") {
      return "1";
    }

    Fragment &e = nameToFragment(name, _ass);
    // keep track of last decorated proposition to implement the prints
    _lastDecoratedProposition = e._p;
    _lastDecoratedFragmentID = nameToFragmentID(name, _ass);

    int type = nameToType(name, _ass);

    // force the proposition
    if (e._forced > 0 || e._manualForced != -1) {
      decoration.second << generateForce(e, nameToFragmentID(name, _ass),
                                         inAnt(name, _ass))
                               ._code;

      // return if the fragment only requires to force the values (all timers
      if (e._timers[0]._duration == 0 && e._timers[1]._duration == 0 &&
          e._timers[2]._duration == 0) {
        return "fo";
      }
      // add force only once
      e._forced = 0;
      e._manualForced = -1;
    }

    if (type == -1) {
      // name is a proposition: replace placeholder
      return oden::prop2String(*e._p);
    } else if (type == -2) {
      // name is an until proposition: replace placeholder
      // keep track of last decorated until proposition to implement the prints
      _lastDecoratedUntilProposition = e._until;
      _lastDecoratedUntilFragmentID = nameToFragmentID(name, _ass);
      return oden::prop2String(*e._until);
    } else {
      // name is a timer: create & evaluate timer
      std::string socket = name + "SocketAss" + _id;
      if (_timersToFree.count(name) == 0) {
        // handle timer indentified by name
        _timersToFree.insert({name});
        decoration.first << "__var " << socket << ";\n";
        decoration.second << ident2 << "if(" << socket << " == -1"
                          << "){\n";
        decoration.second << ident3 << socket << " = "
                          << "createTimer(" << e._timers[type]._duration
                          << ");\n"
                          << ident2 << "}\n";
        // var init
        _varInit.push_back(socket + " = -1;");
      }
      return "eval(" + socket + ") == 1";
    }
  }

  return "1";
}
// returns code needed where the function is called --------
// all the other code is appended to the global code(force method) =========
Code IARchecker::generateForce(Fragment frag, size_t fragID, bool inAnt) {

  Code res;
  std::stringstream cs;
  std::stringstream ds;
  std::string prefix = inAnt ? "Ant" : "Con";
  std::string forceID = std::to_string(fragID) + _id + prefix;
  _forcesToStop.insert({forceID});

  //-----------------------------------------
  // var initialization
  _varInit.push_back("bIdforce" + forceID + " = -1;\n");
  _varInit.push_back("forceTime" + forceID + " = 0;\n");

  res._code += ident2 + "if(bIdforce" + forceID + " == -1" + "){\n";

  // deactivate last force if still running
  // FIXME
  if (false) {
    if (_lastForceID != "") {
      res._code += ident3 + "if(forceTime" + _lastForceID + "> 0){\n";
      res._code += ident4 + "forceTime" + _lastForceID + "(1);\n";
      res._code += ident3 + "}\n";
    }
    _lastForceID = forceID;
  }
  //-----------------------------------------

  //================================================
  // BEGIN OF force METHOD (COMMON FOR MANUAL AND AUTOMATIC TC)
  cs << "force" << forceID << "(end){\n";
  cs << ident2 << "if(forceTime" << forceID << " == 0 || end == 1){\n";

  // FILL STRUCTURE TCs TO HAVE ALL TCs THAT WILL BE USED TO WRITE (W.R.T. _nTB)
  // automatic tc ---> fill TCs
  // manual tc ------> fill TCs
  std::unordered_map<size_t, std::vector<std::vector<std::string>>> TCs;
  std::vector<std::string>
      automaticTcVariables; // variables used by automatic tc
  z3::Z3TestCaseGenerator tcGenerator;
  std::vector<z3::TestCase> tcs; // testcases generated by Z3

  if (frag._manualForced != -1) { // manual testcase
    size_t writtenTBnum =
        frag._tcList.size(); // TB that user has written in the XML (pratically
                             // number of ;)
    if (writtenTBnum > _ass._nTB || writtenTBnum <= 0)
      messageError("Number of testbenches written in assertion " +
                   _id + ", fragment " +
                   std::to_string(fragID) +
                   " is greater then the desidered number of testbenches (" +
                   std::to_string(_ass._nTB) + ")");

    else if (writtenTBnum <= _ass._nTB) {
      size_t missingTC = _ass._nTB - writtenTBnum;

      if (writtenTBnum < _ass._nTB) {
        std::string times =
            (_ass._nTB - writtenTBnum) == 1 ? " time" : " times";
        messageWarning("Number of testbenches written in assertion " +
                       _id + ", fragment " +
                       std::to_string(fragID) +
                       " is less then the desidered number of testbenches (" +
                       std::to_string(_ass._nTB) +
                       "). Last testbench will be replicated " +
                       std::to_string(_ass._nTB - writtenTBnum) + times);
      }

      // get last TC in _testBenches
      std::vector<std::vector<std::string>> lastTC;

      int lastTClen = frag._tcList.at(frag._tcList.size() - 1);
      for (int w = 0; w < lastTClen;
           w++) { // w = number of fragments of the last written TC
        lastTC.insert(lastTC.begin(),
                      frag._testBenches.at(frag._testBenches.size() - w - 1));
      }

      // copy tc from frag._testBranches to TCs
      size_t alreadyInserted = 0;
      std::vector<std::vector<std::string>> tmp;
      for (size_t w = 0; w < frag._tcList.size(); w++) {
        int numElem = frag._tcList.at(w);
        for (int z = 0; z < numElem; z++) {
          tmp.push_back(frag._testBenches[alreadyInserted++]);
        }
        TCs.insert({w, tmp});
        tmp.clear();
      }

      /*
      for(int i=0; i<TCs.size(); i++){
        std::cout << "TESTCASE " << i << "\n";
        std::vector<std::vector<std::string>> actual;
        actual = TCs.at(i);
        for(int j=0; j<actual.size(); j++){
          std::cout << "------ FRAGMENT " << j << "\n";
          for(int k=0; k<actual.at(j).size(); k++){
            std::cout << "------------ VAL " << actual.at(j).at(k) << "\n";
          }
        }
      }
      */

      // fill TCs with remaining tc not defined by the user (fill with
      // repetition of the last defined TC lastTC)
      for (size_t w = 0; w < missingTC; w++) {
        TCs.insert({TCs.size(), lastTC});
      }

      // at this moment TCs contains all testcases of the fragment frag
      // (including missingTC that are filled in TCs)
    } // end else if(writtenTBnum < _ass._nTB)
  }   // end if _manualForced > -1 (manual testcases)
  else {
    // generate testcase using Z3
    tcs = tcGenerator.generateTestCase(*frag._p, _ass._nTB);
    std::string antOrCons = prefix == "Ant" ? " antecedent" : " consequent";
    if (tcs.size() == 0)
      messageError("Z3 can not generate instances in " + antOrCons +
                   " assertion " + _id + ", fragment " +
                   std::to_string(fragID));

    std::vector<std::vector<std::string>>
        lastInsertedTC; // used if Z3 instances are not enoguh
    for (auto name_val : tcs.front()) {
      automaticTcVariables.push_back(name_val.first);
    }
    for (z3::TestCase tc : tcs) {
      std::vector<std::string> singleTc;
      for (z3::Assignment ass : tc) { // every ass is in the form (var, value) =
                                      // (ass.first, z3::valueToString(ass))
        singleTc.push_back(z3::valueToString(ass));
      } // enf internal for
      singleTc.push_back(std::to_string(
          frag._forced)); // last element of the test case is the forced time
      lastInsertedTC.clear();
      lastInsertedTC.push_back(
          singleTc); // in automatic testcase, lastInsertedTC has always only
                     // one element
      TCs.insert({TCs.size(), lastInsertedTC});
    } // end external for

    // fill TCs with remaining tc if Z3 has note enough instances (fill with
    // repetition of the last Z3 generated TC lastInsertedTC)
    if (tcs.size() < _ass._nTB) {
      std::string times = _ass._nTB - tcs.size() == 1 ? " time" : " times";
      std::string antOrCons = prefix == "Ant" ? "antecedent" : "consequent";
      messageWarning("Z3 can not generate enough instances in assertion " +
                     _id + ", fragment " +
                     std::to_string(fragID) + " of the " + antOrCons +
                     ". Last testbench instance has been replicated " +
                     std::to_string(_ass._nTB - tcs.size()) + times);

      size_t missingTC = _ass._nTB - tcs.size();

      for (size_t w = 0; w < missingTC; w++) {
        TCs.insert({TCs.size(), lastInsertedTC});
      }
    }

  } // end if _manualForced > -1 (automatic testcases)

  // at this moment, independently from automatic or manual tc definition in the
  // XML, TCs contains the list of testcases that need to be used by the IAR
  // checker

  size_t numVariables = TCs.at(0)[0].size() - 1;

  for (size_t index = 0; index < numVariables; index++) {
    // debug vars don't have breakpoint
    if ((frag._manualForced != -1 &&
         oden::getDebugVars().count(frag._tbVars[index]) == 0) ||
        (frag._forced != 0 &&
         oden::getDebugVars().count(automaticTcVariables.at(index)) == 0)) {
      cs << ident3 << "__clearBreak("
         << "bIdforce" << forceID << "_" << index << ");\n";
      ds << "__var "
         << "bIdforce" << forceID << "_" << index << ";\n";
    }
  }

  cs << ident3 << "__clearBreak("
     << "bIdforce" << forceID << ");\n";
  cs << ident3 << "return;\n";
  cs << ident2 << "}\n\n";

  //----------------------------------
  res._code += ident3 + "bIdforce" + forceID +
               " = __setSimBreak( \"cs_time\", \"W\", \"force" + forceID +
               "(0)" + "\" );\n";
  //----------------------------------

  // now scroll all testcase and write IAR checker
  for (size_t tcNumber = 0; tcNumber < TCs.size(); tcNumber++) {

    size_t actualTcLength = TCs.at(tcNumber).size();
    size_t numVariables = TCs.at(tcNumber).at(0).size() -
                          1; // number of variables used by the tc (use at(0)
                             // because at least there is 1 element in
                             // TCs.at(tcNumber)) -1 because the last element
                             // is the force time value

    std::string elseOrNotElse =
        tcNumber == 0 ? "if" : "}\n" + ident2 + "else if";
    cs << ident2 << elseOrNotElse << "(nTestBench" << _id
       << " == " << tcNumber << "){\n";
    std::vector<std::vector<std::string>> actualTC;

    size_t actualTcTotTime = 0; // sum of all times used by each assignment
    for (size_t i = 0; i < actualTcLength; i++)
      actualTcTotTime += std::stoi(TCs.at(tcNumber).at(i).at(numVariables));

    //--------------------------------------
    elseOrNotElse = tcNumber == 0 ? "if" : "else if";
    res._code += ident3 + elseOrNotElse + "(nTestBench" + _id +
                 "==" + std::to_string(tcNumber) + "){\n" + ident4 +
                 "forceTime" + forceID + "=" + std::to_string(actualTcTotTime) +
                 ";\n" + ident3 + "}\n";
    //--------------------------------------

    size_t passedTime = 0; // cumulatively, from 0 to actualTcTotTime
    std::vector<std::string>
        tbAssign; // contains strings P0=0 and P4=16 and so on

    for (size_t injection = 0; injection < actualTcLength;
         injection++) { // slide every (...) ---> 0 16 4 2200
      size_t injectionTime =
          std::stoi(TCs.at(tcNumber).at(injection).at(numVariables)); // 2200
      std::string elseOrNotElse = injection == 0 ? "if" : "else if";
      cs << ident3 << elseOrNotElse << "(forceTime" << forceID
         << " == " << actualTcTotTime - passedTime << "){\n";

      // for every variable of the tc (manual or automatic), write its value
      // (P0=0; P4=16;...)
      if (frag._manualForced != -1) {
        for (size_t injRow = 0; injRow < frag._tbVars.size();
             injRow++) { // for 3 times
          if (injection > 0)
            // debug vars can't use breakpoint
            if (oden::getDebugVars().count(frag._tbVars[injRow]) == 0) {
              cs << ident4 << "__clearBreak(bIdforce" << forceID << "_"
                 << injRow << ");\n";
            }

          tbAssign.push_back(frag._tbVars.at(injRow) + " = " +
                             TCs.at(tcNumber).at(injection).at(injRow));
          cs << ident4 << tbAssign[injRow] << ";\n"; // write P0=0; ...
          // can't create breakpoints on debug variables
          if (oden::getDebugVars().count(frag._tbVars[injRow]) == 0) {
            cs << ident4 << "bIdforce" << forceID << "_" << injRow
               << " = __setSimBreak(\"" << frag._tbVars.at(injRow)
               << "\", \"W\", \"" << tbAssign.at(injRow) << "\");\n";
          }
        }
      } else {
        for (size_t injRow = 0; injRow < automaticTcVariables.size();
             injRow++) { // for 3 times
          tbAssign.push_back(automaticTcVariables.at(injRow) + " = " +
                             TCs.at(tcNumber)[injection][injRow]);
          cs << ident4 << tbAssign.at(injRow) << ";\n"; // write P0=0; ...
          // can't create breakpoints on debug variables
          if (oden::getDebugVars().count(automaticTcVariables.at(injRow)) ==
              0) {
            cs << ident4 << "bIdforce" << forceID << "_" << injRow
               << " = __setSimBreak(\"" << automaticTcVariables.at(injRow)
               << "\", \"W\", \"" << tbAssign.at(injRow) << "\");\n";
          }
        }
      }

      cs << ident4 << " __message \"Forcing values of testbench \", nTestBench"
         << _id << ", \": ";
      // for every variable of the tc (manual or automatic), write its value
      // (P0=0; P4=16;...)
      if (frag._manualForced != -1) {
        for (size_t injRow = 0; injRow < frag._tbVars.size();
             injRow++) { // for 3 times
          cs << tbAssign.at(injRow) << "; ";
        }
      } else {
        for (size_t injRow = 0; injRow < automaticTcVariables.size();
             injRow++) { // for 3 times
          cs << tbAssign.at(injRow) << "; ";
        }
      }

      cs << "\";\n" << ident3 << "}\n"; // end if(forceTime...
      passedTime += injectionTime;
      tbAssign.clear();
    }
  } // end for tcNumber

  //-----------------------------------
  res._code += ident3 + "//force the values starting from this instant\n" +
               ident3 + "force" + forceID + "(0);\n";
  //-----------------------------------

  cs << ident2 << "}\n"; // end if(nTestBench
  cs << ident2 << "forceTime" << forceID << "--;\n";
  cs << "}\n";

  ds << "__var "
     << "bIdforce" << forceID << ";\n";
  ds << "__var "
     << "forceTime" << forceID << ";\n";
  //================================================

  //-----------------------------------------
  res._code += ident2 + "}\n";
  //-----------------------------------------

  _methods._code += cs.str();
  _methods._data += ds.str();

  return res;
}

void IARchecker::generateEnforcePrecondition() {
  std::stringstream cs;
  std::stringstream ds;

  cs << "enforcePrecondition" << _id << "(){\n";

  cs << ident1 << "if(bIdEnforcePrecondition" << _id << "==-1){\n";
  cs << ident2 << "bIdEnforcePrecondition" << _id
     << " = __setSimBreak( \"cs_time\", \"W\", \"enforcePrecondition" << _id
     << "()\");\n";
  cs << ident2 << "__message \"Forcing precondition!\";\n";
  cs << ident1 << "}\n";
  cs << ident1 << "if(" << oden::prop2String(*_ass._precondition) << "){\n";
  cs << ident2 << "__message \"Precondition" << _id << " is true!\";\n";
  cs << ident2 << "__clearBreak(bIdEnforcePrecondition" << _id << ");\n";
  cs << ident2 << "bIdEnforcePrecondition" << _id << "=-1;\n";
  cs << ident2 << "checkerActivator();\n";
  cs << ident2 << "return;\n";
  cs << ident1 << "}\n";

  z3::Z3TestCaseGenerator tcGenerator;
  if (!isConstant(*_ass._precondition.get())){
    std::vector<z3::TestCase> tcs =
        tcGenerator.generateTestCase(*_ass._precondition, 1);

    for (z3::TestCase tc : tcs) {
      for (z3::Assignment ass : tc) {
        cs << ident1 << ass.first << "=" << z3::valueToString(ass) << ";\n";
      }
    }
  } 

  cs << "}\n";

  ds << "__var bIdEnforcePrecondition" << _id << ";\n";
  // var initialization
  _varInit.push_back("bIdEnforcePrecondition" + _id + " = -1;");
  _implementation._code += cs.str();
  _implementation._data += ds.str();
}
void IARchecker::generateEnforcePostcondition() {
  std::stringstream cs;
  std::stringstream ds;

  cs << "enforcePostcondition" << _id << "(){\n";

  cs << ident1 << "if(bIdEnforcePostcondition" << _id << "==-1){\n";
  cs << ident2 << "bIdEnforcePostcondition" << _id
     << " = __setSimBreak( \"cs_time\", \"W\", \"enforcePostcondition" << _id
     << "()\");\n";
  cs << ident2 << "__message \"Forcing postcondition!\";\n";
  cs << ident1 << "}\n";
  cs << ident1 << "if(" << oden::prop2String(*_ass._postcondition) << "){\n";
  cs << ident2 << "__message \"Postcondition" << _id << " is true!\";\n";
  cs << ident2 << "__clearBreak(bIdEnforcePostcondition" << _id << ");\n";
  cs << ident2 << "bIdEnforcePostcondition" << _id << "=-1;\n";
  cs << ident2 << "checkerActivator();\n";
  cs << ident2 << "return;\n";
  cs << ident1 << "}\n";

  z3::Z3TestCaseGenerator tcGenerator;
  std::vector<z3::TestCase> tcs =
      tcGenerator.generateTestCase(*_ass._postcondition, 1);
  for (z3::TestCase tc : tcs) {
    for (z3::Assignment ass : tc) {
      cs << ident1 << ass.first << "=" << z3::valueToString(ass) << ";\n";
    }
  }

  cs << "}\n";

  ds << "__var bIdEnforcePostcondition" << _id << ";\n";
  // var initialization
  _varInit.push_back("bIdEnforcePostcondition" + _id + " = -1;");
  _implementation._code += cs.str();
  _implementation._data += ds.str();
}

const Code &IARchecker::getCode() { return _implementation; }

std::string IARchecker::getID() { return _id; }

std::string IARchecker::getComment() { return _ass._comment; }

} // namespace codeGenerator
