
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#include "TestPlanGenerator.hh"
#include "AssignExtractVisitor.hh"
#include "z3TestCaseGenerator.h"
#include <iomanip>

namespace codeGenerator {
#define HAS_VARIABLE(assID, curr_state, assIDToAss, assIDsContainer)           \
  for (std::string assID : curr_state->assIDsContainer) {                           \
    if (assIDToAss.at(assID).count(mostFreVar.first) == 0) {                   \
      goto skipRestart;                                                        \
    }                                                                          \
  }

#define FILL_FREQ_EVAL(assID, curr_state, assIDToAss, assIDsContainer)         \
  for (std::string assID : curr_state->assIDsContainer) {                           \
    for (const auto &var : assIDToAss.at(assID)) {                             \
      if (curr_state->_vars.count(var.first)) {                                \
        continue;                                                              \
      }                                                                        \
      varNameToFrequency[var.first]++;                                         \
                                                                               \
      auto &pe = varToPossibleEvaluations[var.first];                          \
      if (std::find_if(begin(pe), end(pe), [&var](const z3::TypeValue &e) {    \
            return e == var.second;                                            \
          }) == end(pe)) {                                                     \
                                                                               \
        pe.push_back(var.second);                                              \
      }                                                                        \
    }                                                                          \
  }

#define UPDATE_RESTORE_STATE(assertionID, clusterID)                           \
  _assIDtoRestoreStateID[assertionID] = _clusterIDtoSavedState.at(clusterID);

#define FILL_QUEUE(cluster, assertion, visited)                                \
  for (std::string assID : cluster->_preAssIDs) {                                   \
    if (assID != assertion && visited.count(assID) == 0) {                     \
      queue.push_back(assID);                                                  \
      visited.insert(assID);                                                   \
      UPDATE_RESTORE_STATE(assID, first_cluster->_id);                         \
    }                                                                          \
  }

static std::unordered_map<std::string, z3::TypeValue>
prop2Memory(oden::Proposition &prop) {
  oden::AssignExtractVisitor extractor;
  prop.acceptVisitor(extractor);
  return extractor.get();
}
static std::string getUnhandledAssertion(const std::shared_ptr<State> &curr_state,
                                    const std::unordered_set<std::string> &visited) {
  for (std::string assID : curr_state->_preAssIDs) {
    if (visited.count(assID) == 0) {
      return assID;
    }
  }
  return std::to_string((size_t)-1);
}
void TestPlanGenerator::updateSavedState(
    std::string assID, bool isPre, const std::shared_ptr<State> &curr_state) {

  auto tmp = curr_state;
  while (tmp->_upperLevel != nullptr) {
    if (_clusterIDtoSavedState.count(tmp->_id) == 0 ||
        tmp->_vars.size() >= curr_state->_vars.size()) {
      _clusterIDtoSavedState[tmp->_id] = std::make_pair(isPre, assID);
    }
    tmp = tmp->_upperLevel;
  }
}

void TestPlanGenerator::generateTestplan() {

  std::deque<std::string> queue;
  std::unordered_set<std::string> visited;

  size_t max_threshold = 100;
  std::vector<size_t> incompleteCount(max_threshold);
  size_t jumps = 0;

  // safe state(initial state)
  _clusterIDtoSavedState[0] = std::make_pair(true, ULONG_MAX);

  assert(_assIDtoPreCluster.count(_first_assertion));
  const std::shared_ptr<State> &first_cluster =
      _assIDtoPreCluster.at(_first_assertion);

  updateSavedState(_first_assertion, true, first_cluster);
  // prepare the queue to start the visit
  FILL_QUEUE(first_cluster, _first_assertion, visited);

  // add first assertion in front
  queue.push_front(_first_assertion);
  visited.insert({_first_assertion});

  UPDATE_RESTORE_STATE(_first_assertion, first_cluster->_id);

  size_t totAssertions = _rootState->_preAssIDs.size();
  size_t processed = 0;

  while (processed < totAssertions) {

    // process assertion
	  std::string curr_assertion = queue.front();
    assert(_assIDtoPreCluster.count(curr_assertion));
    updateSavedState(curr_assertion, true,
                     _assIDtoPreCluster.at(curr_assertion));

    _testplan.push_back(curr_assertion);
    //    std::cout << "adding:"<<curr_assertion << " from cluster:
    //    "<<_assIDtoPreCluster.at(curr_assertion)->_id<<"\n";

    queue.pop_front();
    processed++;

    // curr_assertion_pre -> curr_assertion_pos

    assert(_assIDtoPosCluster.count(curr_assertion));
    auto next_state = _assIDtoPosCluster.at(curr_assertion);
    updateSavedState(curr_assertion, false, next_state);

    // add assertions_pre found in curr_assertion_pos
    for (std::string assID : next_state->_preAssIDs) {
      if (visited.count(assID) == 0) {
        queue.push_front(assID);
        visited.insert(assID);
        UPDATE_RESTORE_STATE(assID, next_state->_id);
      }
    } // for assID

    // If the queue is empty: search the upper levels for unhandled assertions
    // If we only have the root cluster then we have already visited all
    // assertions(_clusters.size()==1)
    if (queue.empty() && _clusters.size() > 1) {
      // statistics...
      //------------------
      size_t numberOfBacktracks = 1;
      //------------------

      // recursively seach the upper levels

      assert(_assIDtoPosCluster.count(_testplan.back()));
      auto tmp = _assIDtoPosCluster.at(_testplan.back())->_upperLevel;

      // statistics...
      //------------------
      numberOfBacktracks++;
      //------------------
      while ((curr_assertion = getUnhandledAssertion(tmp, visited)) ==
             std::to_string((size_t)-1)) {
        if (tmp->_upperLevel == nullptr) {
          goto stopVisit;
        }
        tmp = tmp->_upperLevel;
        // statistics...
        //------------------
        numberOfBacktracks++;
        //------------------
      }

      // cluster of the newly found assertion
      assert(_assIDtoPreCluster.count(curr_assertion));
      const std::shared_ptr<State> &next_cluster =
          _assIDtoPreCluster.at(curr_assertion);

      // statistics...
      //------------------
      for (size_t i = 0; i < max_threshold; i++) {
        if (numberOfBacktracks > i) {
          incompleteCount[i]++;
        }
      }
      jumps++;
      //------------------

      // refill the queue with the assertions found in the new cluster
      FILL_QUEUE(next_cluster, curr_assertion, visited);

      // add the first found assertion in front
      queue.push_front(curr_assertion);
      visited.insert(curr_assertion);

      UPDATE_RESTORE_STATE(curr_assertion, tmp->_id);

    } // while !queue.empty
  }   // totAssertions

stopVisit:;
  /*
   * DEBUG
std::cout << "testplan:"
    << "\n";
for (auto i : _testplan) {
std::cout << i << " ";
}
std::cout << "\n";
*/

  std::cout << "--------------------------------------"
            << "\n";
  std::cout << "max jumps"
            << "  |  "
            << "completeness\n";
  std::cout << "--------------------------------------"
            << "\n";
  for (size_t i = 0; i < max_threshold; i++) {
    float completeness = (1.f - (float)incompleteCount[i] /
                                    ((float)_rootState->_preAssIDs.size())) *
                         100.f;
    std::cout << "    " << i << "      |  "
              << "     " << std::setprecision(4) << completeness << "%\n";
    if (completeness + 0.01f >= 100.f) {
      break;
    }
  }
  std::cout << "--------------------------------------"
            << "\n";
  std::cout << "Tot jumps: " << jumps << "\n";
  std::cout << "--------------------------------------"
            << "\n";

  /*
   * DEBUG
  std::cout << "AssIDtoRestoreState:"
            << "\n";
  for (auto i : _assIDtoRestoreStateID) {
    std::cout << i.first << "->" << i.second.second
              << (i.second.first ? "pr" : "ps") << "\n";
  }
  */
}
static std::unordered_map<std::string,
                          std::unordered_map<std::string, z3::TypeValue>>
    assIDToPrecAss;
static std::unordered_map<std::string,
                          std::unordered_map<std::string, z3::TypeValue>>
    assIDToPostAss;
static bool hasAssignment(std::string assID, bool isPre, const std::string &var_name,
                          const z3::TypeValue &eval) {
  if (isPre) {
    for (const auto &e : assIDToPrecAss.at(assID)) {
      if (e.second.first == eval.first && e.first == var_name &&
          e.second == eval) {
        return true;
      }
    }

  } else {
    for (const auto &e : assIDToPostAss.at(assID)) {
      if (e.second.first == eval.first && e.first == var_name &&
          e.second == eval) {
        return true;
      }
    }
  }

  return false;
}
static bool hasUnusedVars(std::shared_ptr<State> &state) {

  std::unordered_set<std::string> allVars;

  for (const auto &e : state->_posAssIDs) {
    for (const auto &k : assIDToPostAss.at(e)) {
      allVars.insert({k.first});
    }
  }

  for (const auto &e : state->_preAssIDs) {
    for (const auto &k : assIDToPrecAss.at(e)) {
      allVars.insert({k.first});
    }
  }

  for (const auto &i : allVars) {
    if (state->_vars.count(i) == 0) {
      return true;
    }
  }

  return false;
}
static void
fillUtilityVariables(const std::unordered_map<std::string, Assertion> &assertions) {
  for (const auto &a : assertions) {
    assIDToPrecAss[a.first] = prop2Memory(*a.second._precondition);
    assIDToPostAss[a.first] = prop2Memory(*a.second._postcondition);
  }
}

static std::string toString(const std::shared_ptr<State> &curr_state,
                            size_t level = 0, bool withVars = false) {

  std::string ident = "";
  for (size_t i = 0; i < level; i++) {
    ident += "\t";
  }

  std::stringstream ss;

  ss << ident << "(" << curr_state->_id << ") ";
  ss << ident << "Pre = {";
  for (const auto &i : curr_state->_preAssIDs) {
    ss << i << " ";
  }
  ss << "} ";
  ss << "Pos = {";
  for (const auto &i : curr_state->_posAssIDs) {
    ss << i << " ";
  }
  ss << "}\n";

  if (withVars) {
    ss << ident << "[";
    std::vector<std::string> tmp;
    for (const auto &e : curr_state->_vars) {
      tmp.push_back(e);
    }
    std::sort(begin(tmp), end(tmp));
    for (const auto &e : tmp) {
      ss << e << " ";
    }
    ss << "]\n";
  }

  for (const auto &cl : curr_state->_lowerLevels) {
    ss << toString(cl, level + 1, withVars);
  }

  return ss.str();
}
TestPlanGenerator::TestPlanGenerator(
    std::unordered_map<std::string, Assertion> &IDtoAssertion,
    std::vector<std::string> assertionsToSort, std::string first_assertion)
    : _IDtoAssertion(IDtoAssertion), _first_assertion(first_assertion) {

  // FIXME
  std::sort(begin(assertionsToSort), end(assertionsToSort));

  _rootState =
      std::make_shared<State>(0, nullptr, assertionsToSort, assertionsToSort);
  _clusters[0] = _rootState;
  fillUtilityVariables(IDtoAssertion);
  build(_rootState);
  // DEBUG print the clusters
  // std::cout << toString(_rootState, 0, false) << "\n";
  generateTestplan();

  // DEBUG
  std::unordered_map<std::string, size_t> assfreq;
  std::stringstream ss;
  bool error = false;
  for (auto e : _testplan) {
    assfreq[e]++;
    if (assfreq[e] > 1) {
      ss << e << " ";
      error = true;
    }
  }
  messageWarningIf(
      error, "Found duplicated assertions in test-plan: " + ss.str() + "\n");
  assert(assertionsToSort.size() == _testplan.size());
}
static std::pair<std::string, size_t>
getMostFrequentInState(std::shared_ptr<State> &curr_state,
                       std::vector<z3::TypeValue> &possibleEvals) {

  //======== calculate most frequent var in cluster======
  std::unordered_map<std::string, size_t> varNameToFrequency;
  std::unordered_map<std::string, std::vector<z3::TypeValue>>
      varToPossibleEvaluations;

  // calcuate frequency for all vars in this state
  // keep pre and post separated
  //
  // pre
  FILL_FREQ_EVAL(assID, curr_state, assIDToPrecAss, _preAssIDs);
  // pos
  FILL_FREQ_EVAL(assID, curr_state, assIDToPostAss, _posAssIDs);

  // find most frequent var
  std::pair<std::string, size_t> mostFreVar;
  while (1) {

    // all assertions belong to the same cluster
    if (varNameToFrequency.empty()) {
      return std::make_pair(std::string(), size_t());
    }

    mostFreVar =
        *std::max_element(begin(varNameToFrequency), end(varNameToFrequency),
                          [](const std::pair<std::string, size_t> &e1,
                             const std::pair<std::string, size_t> &e2) {
                            return e1.second < e2.second;
                          });

    // if we have only one possible evaluation then the assignment is common
    // to all assertions and will not be used to clusterize
    if (varToPossibleEvaluations.at(mostFreVar.first).size() == 1) {
      // check if all assertions have the assignment
      HAS_VARIABLE(assID, curr_state, assIDToPrecAss, _preAssIDs);
      HAS_VARIABLE(assID, curr_state, assIDToPostAss, _posAssIDs);
    } else {
      goto skipRestart;
    }

    curr_state->_vars.insert({mostFreVar.first});
    // restart without the current most frequent var
    varNameToFrequency.erase(mostFreVar.first);
  }

skipRestart:;
  possibleEvals = varToPossibleEvaluations.at(mostFreVar.first);

  return mostFreVar;
}
std::shared_ptr<State> &TestPlanGenerator::addCluster(
    const std::string &mostFreVarName, std::shared_ptr<State> &curr_state,
    std::vector<std::string> &preAssIDs, std::vector<std::string> &posAssIDs) {

  // FIXME
  // std::sort(begin(preAssIDs), end(preAssIDs));
  // std::sort(begin(posAssIDs), end(posAssIDs));

  _clusters[_clusterIndex] =
      std::make_shared<State>(_clusterIndex, curr_state, preAssIDs, posAssIDs);
  std::shared_ptr<State> &newState = _clusters.at(_clusterIndex);
  curr_state->_lowerLevels.push_back(newState);
  _clusterIndex++;

  newState->_vars.insert({mostFreVarName});
  for (const auto &e : curr_state->_vars) {
    newState->_vars.insert({e});
  }

  // add link from assertion pre/pos to the cluster that contains it
  for (std::string assID : preAssIDs) {
    _assIDtoPreCluster[assID] = newState;
  }
  for (std::string assID : posAssIDs) {
    _assIDtoPosCluster[assID] = newState;
  }

  return newState;
}
void TestPlanGenerator::build(std::shared_ptr<State> &curr_state) {

  std::vector<z3::TypeValue> possibleEvals;
  auto mostFreVar = getMostFrequentInState(curr_state, possibleEvals);

  // can't split in more clusters
  if (possibleEvals.empty()) {

    // all assertions are in the root cluster
    if (curr_state->_id == 0) {
      // add link from assertion pre/pos to the cluster that contains it
      for (std::string assID : curr_state->_preAssIDs) {
        _assIDtoPreCluster[assID] = curr_state;
      }
      for (std::string assID : curr_state->_posAssIDs) {
        _assIDtoPosCluster[assID] = curr_state;
      }
    }

    return;
  }

  // clusterize for mostFreVar
  std::vector<std::string> usedPreAssertions;
  std::vector<std::string> usedPosAssertions;

  // create a cluster for each possible eval
  for (size_t i = 0; i < possibleEvals.size(); i++) {
    const auto &curr_eval = possibleEvals[i];

    // fill with assertions that have the evaluation
    // ------------------------------
    std::vector<std::string> preAssIDs;
    std::vector<std::string> posAssIDs;

    for (std::string assID : curr_state->_preAssIDs) {
      if (hasAssignment(assID, true, mostFreVar.first, curr_eval)) {
        preAssIDs.push_back(assID);
        usedPreAssertions.push_back({assID});
      }
    }
    for (std::string assID : curr_state->_posAssIDs) {
      if (hasAssignment(assID, false, mostFreVar.first, curr_eval)) {
        posAssIDs.push_back(assID);
        usedPosAssertions.push_back({assID});
      }
    }
    // ------------------------------

    // add cluster
    std::shared_ptr<State> &newState =
        addCluster(mostFreVar.first, curr_state, preAssIDs, posAssIDs);

    // go on only if newState can be further clusterized
    if (hasUnusedVars(newState)) {
      build(newState);
    }

  } // for eval

  // add don't-care cluster (set of assertions that don't have the mostFreVar)
  //===================================================

  // find unused assertions to create the don't-care cluster
  // sort containers to perform set operations
  std::sort(begin(curr_state->_preAssIDs), end(curr_state->_preAssIDs));
  std::sort(begin(curr_state->_posAssIDs), end(curr_state->_posAssIDs));
  std::sort(begin(usedPreAssertions), end(usedPreAssertions));
  std::sort(begin(usedPosAssertions), end(usedPosAssertions));
  // remove duplicates
  usedPosAssertions.erase(
      std::unique(begin(usedPosAssertions), end(usedPosAssertions)),
      end(usedPosAssertions));
  usedPreAssertions.erase(
      std::unique(begin(usedPreAssertions), end(usedPreAssertions)),
      end(usedPreAssertions));

  // init result container with the correct size
  std::vector<std::string> preAssIDs(curr_state->_preAssIDs.size() -
                                usedPreAssertions.size());
  std::set_difference(begin(curr_state->_preAssIDs),
                      end(curr_state->_preAssIDs), begin(usedPreAssertions),
                      end(usedPreAssertions), begin(preAssIDs));

  std::vector<std::string> posAssIDs(curr_state->_posAssIDs.size() -
                                usedPosAssertions.size());
  std::set_difference(begin(curr_state->_posAssIDs),
                      end(curr_state->_posAssIDs), begin(usedPosAssertions),
                      end(usedPosAssertions), begin(posAssIDs));

  // only if there is something to cluster
  if (!preAssIDs.empty() || !posAssIDs.empty()) {
    // create a don't-care cluster
    std::shared_ptr<State> &dontCareState = addCluster(
        "(" + mostFreVar.first + ")", curr_state, preAssIDs, posAssIDs);
    // go on only if dontCareState can be further clusterized
    if (hasUnusedVars(dontCareState)) {
      build(dontCareState);
    }
  }

  //===================================================
}

} // namespace codeGenerator
