
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
 * @file
 * @author
 * Samuele Germiniani, samuele.germiniani@univr.it
 * Moreno Bragaglio, moreno.bragaglio@univr.it
 * @date 2020
 * @version v1.0
 */

#pragma once

#include "odenCore.hh"
#include "spot/tl/parse.hh"
#include "spot/twaalgos/hoa.hh"
#include "spot/twaalgos/translate.hh"
#include <limits>
#include <regex>
#include <string>
#include <vector>

namespace memorySave {
extern std::pair<size_t, size_t> range;
std::pair<std::string, std::string> rangetoString();
} // namespace memorySave
namespace codeGenerator {
static std::string tab = "   ";
static std::string ident1 = tab;
static std::string ident2 = ident1 + tab;
static std::string ident3 = ident2 + tab;
static std::string ident4 = ident3 + tab;

using SpotAutomata = spot::twa_graph_ptr;
using PSLformula = std::string;

struct AutomataImplication {
  AutomataImplication(const spot::twa_graph_ptr &ant,
                      const spot::twa_graph_ptr &con)
      : _ant(ant), _con(con) {}

  SpotAutomata _ant;
  SpotAutomata _con;
};

struct PSLimplication {
  PSLimplication(const std::string &ant, const std::string &con)
      : _ant(ant), _con(con) {}
  PSLformula _ant;
  PSLformula _con;
};

enum class TimerType { Min = 0, Max };

struct Code {
  Code(std::string data, std::string code) : _data(data), _code(code) {}
  Code() {}
  std::string _data;
  std::string _code;
};

struct Timer {
  Timer(std::string name, TimerType type, size_t duration)
      : _name(name), _type(type), _duration(duration) {}
  Timer() : _name(), _type(), _duration() {}
  std::string _name;
  TimerType _type;
  size_t _duration;
};

struct Fragment {
  size_t _forced;
  int _manualForced = -1; // TB id
  std::vector<std::vector<std::string>>
      _testBenches; //_testBenches = <<0,16,4,2200>, <0,0,0,200>>
  std::vector<std::string> _tbVars; // variables used by the manual testcases
  size_t _times;
  std::string _placeHolder;
  std::string _untilPlaceHolder;
  std::vector<Timer> _timers;
  std::shared_ptr<oden::Proposition> _until = nullptr;
  std::shared_ptr<oden::Proposition> _p = nullptr;
  std::vector<size_t>
      _tcList; // every element indicates how many vector<std::string> are used
               // for a single testbench (vector<std::string> kept by
               // fragment._testBenches) the size of _tcList is the number of
               // testcases. Index of _tcList corresponds to the tc number,
               // value at a certain index corresponds at the number of
               // fragments in _testBenches for that testcase
};
struct Stimulo {
  size_t _id;
};
// informations related to a 'name' in an implication
// used to provide a link between annotations and PSL implications
struct Info {
  Info(size_t fragID, int type, bool inAnt)
      : _fragID(fragID), _type(type), _inAnt(inAnt) {}
  Info() : _fragID(), _type(), _inAnt() {}
  size_t _fragID;
  int _type;   //-2 UntilProp, -1:Prop, 0:Max, 1:Min, 2:Delay
  bool _inAnt; // false is Con
};

struct Config {
  Config(bool check_completeness, std::string first_assertion)
      : _check_completeness(check_completeness),
        _first_assertion(first_assertion) {}
  Config() {}
  bool _check_completeness = false;
  bool _automatic_testplan_generation = false;
  std::string _first_assertion;
};
inline std::ostream &operator<<(std::ostream &os, Info i) {
  os << "(" << i._fragID << ", " << i._type << ", "
     << (i._inAnt ? "Ant" : "Con") << ")\n";
  return os;
}
// containts all information related to a single assertion in the form of an
// implication
struct Assertion {
  std::shared_ptr<oden::Proposition> _precondition = nullptr;
  std::shared_ptr<oden::Proposition> _postcondition = nullptr;
  std::vector<Fragment> _a;
  std::vector<Fragment> _c;
  std::vector<std::string> stimuli;
  // each name uniquely identifies an info in the implication
  std::unordered_map<std::string, Info> _nameToInfo;
  std::string _comment;
  size_t _nTB;
  std::unordered_set<std::string> _usedVars;
};
struct Interrupt {
  Interrupt(std::string name, size_t first_activation, size_t repeat_interval,
            size_t variance, size_t infinite_hold_time, size_t hold_time,
            size_t probability)
      : _name(name), _first_activation(first_activation),
        _repeat_interval(repeat_interval), _variance(variance),
        _infinite_hold_time(infinite_hold_time), _hold_time(hold_time),
        _probability(probability) {}
  Interrupt() {}

  std::string _name;
  size_t _first_activation;
  size_t _repeat_interval;
  size_t _variance;
  size_t _infinite_hold_time;
  size_t _hold_time;
  size_t _probability;
};
inline std::ostream &operator<<(std::ostream &os, Interrupt &i) {
  os << "(" << i._name << ", " << i._first_activation << ", "
     << i._repeat_interval << ", " << i._variance << ", "
     << i._infinite_hold_time << ", " << i._hold_time << ", " << i._probability
     << ")\n";
  return os;
}

// utility functions to correctly use the Info data
// structure=====================
inline Fragment &nameToFragment(std::string name, Assertion &imp) {
  Info &i = imp._nameToInfo.at(name);
  if (i._inAnt) {
    return imp._a[i._fragID];
  } else {
    return imp._c[i._fragID];
  }
}
inline size_t nameToFragmentID(std::string name, Assertion &imp) {
  return imp._nameToInfo.at(name)._fragID;
}
inline bool inAnt(std::string name, Assertion &imp) {
  return imp._nameToInfo.at(name)._inAnt;
}
inline int nameToType(std::string name, Assertion &imp) {
  return imp._nameToInfo.at(name)._type;
}
//=================================================================================

inline bool isFinalState(spot::twa_graph_ptr graph, unsigned state) {
  return std::distance(graph->out(state).begin(), graph->out(state).end()) == 1;
}
inline size_t strToSizeCheckErrors(
    std::string val, std::string name = "value", size_t lBound = 0,
    size_t uBound = std::numeric_limits<unsigned long>::max()) {
  try {
    long long signedConverted = std::stoll(val);
    messageErrorIf(signedConverted < 0, name + " is negative!");
    size_t converted = signedConverted;
    messageErrorIf(converted < lBound || converted > uBound,
                   name + " = " + std::to_string(converted) +
                       " is out of bounds! it must be in [" +
                       std::to_string(lBound) + "," + std::to_string(uBound) +
                       "]");
    return converted;
  } catch (std::exception &e) {
    messageError(name + " conversion error!");
    exit(1);
  }
}
inline size_t hex_strToSizeCheckErrors(
    std::string val, std::string name = "value", size_t lBound = 0,
    size_t uBound = std::numeric_limits<unsigned long>::max()) {
  try {
    size_t converted = std::stoull(val, nullptr, 0);
    messageErrorIf(converted < lBound || converted > uBound,
                   name + " = " + std::to_string(converted) +
                       " is out of bounds! it must be in [" +
                       std::to_string(lBound) + "," + std::to_string(uBound) +
                       "]");
    return converted;
  } catch (std::exception &e) {
    messageError(name + " conversion error!");
    exit(1);
  }
}
inline int strToIntCheckErrors(std::string val, std::string name = "value",
                               int lBound = std::numeric_limits<int>::min(),
                               int uBound = std::numeric_limits<int>::max()) {
  try {
    int signedConverted = std::stoi(val);
    messageErrorIf(signedConverted < lBound || signedConverted > uBound,
                   name + " = " + std::to_string(signedConverted) +
                       " is out of bounds! it must be in [" +
                       std::to_string(lBound) + "," + std::to_string(uBound) +
                       "]");
    return signedConverted;
  } catch (std::exception &e) {
    messageError(name + " conversion error!");
    exit(1);
  }
}
inline bool constainsIllegalChar(std::string &str) {
  std::regex idFormat("[A-Za-z0-9_]");
  return find_if(begin(str), end(str), [&idFormat](char e) {
           return !std::regex_match(std::string(1, e), idFormat);
         }) != end(str);
}
inline void substituteIllegalChar(std::string &str) {
  std::regex idFormat("[A-Za-z0-9_]");
  for (size_t i = 0; i < str.size(); i++) {
    if (!std::regex_match(std::string(1, str[i]), idFormat)) {
	    str[i] = '_';
    }
  }
}

inline void removeAllSpaces(std::string &str) {
  str.erase(remove_if(begin(str), end(str),
                      [](char e) {
                        return e == '\r' || e == '\n' || e == ' ' || e == '\t';
                      }),
            end(str));
}
inline void removeSpacesBeforeAfter(std::string &str) {
  auto it1 = std::find_if_not(begin(str), end(str), [](char e) {
    return e == '\r' || e == '\n' || e == ' ' || e == '\t';
  });
  str.erase(begin(str), it1);
  auto it2 = std::find_if_not(rbegin(str), rend(str), [](char e) {
    return e == '\r' || e == '\n' || e == ' ' || e == '\t';
  });
  str.erase(it2.base(), end(str));
}
inline void removeCR(std::string &str) {

  auto it1 = begin(str);
  while ((it1 = std::find_if(begin(str), end(str), [](char e) {
            return e == '\r' || e == '\n';
          })) != end(str)) {
    str.erase(it1);
  }
}
bool operator==(oden::Proposition &p1, oden::Proposition &p2);

} // namespace codeGenerator
