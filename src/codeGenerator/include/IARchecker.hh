
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#pragma once
#include "types.hh"
#include <deque>
namespace codeGenerator {

/*! \class IARchecker
    \brief Class representing a checker implementing an assertion
*/
class IARchecker {
public:
  /** \brief Constructor
   * \param imp an couple <_ant,_con> of spotLTL automatons
   * \param ass an assertion parsed with XMLparser
   * \param id checker identifier
   */
  IARchecker(const AutomataImplication &imp, Assertion &ass, std::string id);
  ~IARchecker() = default;
  /** \brief Generates C-Spy checker implementation of the given assertion
   */
  void generateImplementation();
  /** \brief Get the comment that will be displayed in the IAR debug log
   */
  std::string getComment();
  /** \brief Get the C-Spy implementation generated with generateImplementation
   */
  const Code &getCode();
  /** \brief 
   *  \return checker identifier
   */
  std::string getID();
private:
  /** \brief generates the C-spy code of the given automata, the code is added
   * to _implementation
   *  \param aut spotLTL automata
   *  \param prefix either 'ant' or 'con'
   */
  void synthesizeAutomata(const spot::twa_graph_ptr &aut,
                          const std::string &prefix);
  //Utility functions
  void generateInitChecker();
  void generateImplicationHandler();
  void generateEnforcePrecondition();
  void generateEnforcePostcondition();
  void generateRecovery();

  void addAllSuccessorsStates(const spot::state *,
                              std::deque<const spot::state *> &fringe,
                              std::unordered_set<unsigned> &visited,
                              const spot::twa_graph_ptr &aut,
                              std::vector<unsigned> &finalStates);
  void handleFinalStates(const std::string &nextStateVar,
                         const std::vector<unsigned> &finalStates);
  void handleReturnValues(const spot::twa_graph_ptr &aut,
                          const std::string &nextStateVar,
                          const std::vector<unsigned> &finalStates);
  void addNextStateFunction(const spot::formula &f,
                            const std::string &nextStateVar, size_t nextState,
                            const std::string &prefix,
                            const spot::twa_graph_ptr &aut);
  std::string visitFormula(spot::formula f,
               std::pair<std::stringstream, std::stringstream> &decoration);
  Code generateForce(Fragment frag, size_t fragID, bool inAnt);
  void printCheckerResult(const std::string &prefix, std::stringstream &cs);

  /** contains the whole implementation of the checker after calling
  "generateImplementation"*/
  Code _implementation;
  /// contains utility functions that eventually will be added to _implementation
  Code _methods;

  /// SpotLTL automatas used implement the checker
  const AutomataImplication _imp;

  /// keep track of variables to initialize in "initChecker"
  std::vector<std::string> _varInit;

  // Utility variables
  std::string _lastForceID = "";
  size_t _lastDecoratedFragmentID;
  size_t _lastDecoratedUntilFragmentID;
  std::shared_ptr<oden::Proposition> _lastDecoratedProposition;
  std::shared_ptr<oden::Proposition> _lastDecoratedUntilProposition;
  std::unordered_set<std::string> _timersToFree;
  std::unordered_set<std::string> _forcesToStop;

  /// assertion implemented by the checker
  Assertion &_ass;

  /// checker identifier
  std::string _id;
};
} // namespace codeGenerator
