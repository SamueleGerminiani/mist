
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
 * @file
 * @author
 * Samuele Germiniani, samuele.germiniani@univr.it
 * Moreno Bragaglio, moreno.bragaglio@univr.it
 * @date 2020
 * @version v1.0
 */

#pragma once

#include "types.hh"

#include <unordered_map>

namespace codeGenerator {
class XMLparser {
public:
  void parseXML(std::string filePath);
  void parseSequence(rapidxml::xml_node<> *xmlTag, std::string assID,
                     Assertion &impl, const std::string &locVarDec,
                     const std::string &gloVarDec,
                     std::unordered_map<size_t, rapidxml::xml_node<> *> &id2tc,
                     bool is_ant);
  void parseTestcaseSequence(std::string &s, std::string assID, Assertion &impl,
                             const std::string &varDec, bool is_ant);
  void checkErrors();
  void updateRequiredTimers(const Assertion &impl);
  void readManualTestcase(Fragment &frag, rapidxml::xml_node<> *testcaseTag,
                          std::string assID, size_t fragID, std::string prefix);
  const std::unordered_map<std::string, PSLimplication> &getPSLimplications();
  std::unordered_map<std::string, Assertion> &getAssertions();
  const std::vector<Interrupt> &getIARinterruptsSetup();
  std::string toPSL(const std::vector<Fragment> &fragments);
  const std::pair<std::string, Assertion> getFirstAssertion();
  const std::vector<std::string> &getTestPlan();
  bool isGenerateTestplanSet();
  std::shared_ptr<oden::Proposition> getStartingCondition();

  std::unordered_map<std::string, std::string> &getDebugVars();
  size_t _maxTimers = 0;

private:
  void parseInterruptNode(rapidxml::xml_node<> *xmlInterruptsSetup);
  void parseAssertion(rapidxml::xml_node<> *xmlAssertion,
                      const std::string &globalDecls);
  void parsePlan(const std::string &plan);
  std::vector<Interrupt> _interruptsSetup;
  std::unordered_map<std::string, PSLimplication> _PSLimplications;
  std::unordered_map<std::string, Assertion> _assertions;
  Config _configuration;
  std::shared_ptr<oden::Proposition> _starting_condition = nullptr;
  bool _defaultConfiguration = true;
  std::vector<std::string> _plan;
};
} // namespace codeGenerator
