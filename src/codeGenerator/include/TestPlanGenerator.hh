
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#pragma once
#include "types.hh"
#include <limits>
namespace codeGenerator {
struct State {
  State(size_t id, const std::shared_ptr<State> &upperLevel,
        const std::vector<std::string> &preAssIDs,const std::vector<std::string> &posAssIDs)
      : _id(id), _upperLevel(upperLevel), _preAssIDs(preAssIDs), _posAssIDs(posAssIDs) {}

  //unique id of the cluster
  size_t _id;
  //father of the cluster
  std::shared_ptr<State> _upperLevel;
  //children of the cluster
  std::vector<std::shared_ptr<State>> _lowerLevels;
  //assertions contained in the cluster
  std::vector<std::string> _preAssIDs;
  std::vector<std::string> _posAssIDs;
  //names of variables characterizing the cluster 
  std::unordered_set<std::string> _vars;
};

class TestPlanGenerator {
public:
  TestPlanGenerator(std::unordered_map<std::string, Assertion> &IDtoAssertion,
                    std::vector<std::string> assertionsToSort,
                    std::string _first_assertion);

  void generateTestplan();
public:
  //output testplan
  std::vector<std::string> _testplan;
  std::unordered_map<std::string,std::pair<bool,std::string>> _assIDtoRestoreStateID;
private:
  void build(std::shared_ptr<State> &curr_state);

  std::shared_ptr<State>& addCluster(const std::string &mostFreVarName,std::shared_ptr<State> &curr_state,std::vector<std::string> &preAssIDs,std::vector<std::string> &posAssIDs);

void updateSavedState( std::string assID, bool isPre, const std::shared_ptr<State> &curr_state) ;



  //input assertion set
  std::vector<std::string> _assertionsToSort;
  //cluster containig all the assertions
  std::shared_ptr<State> _rootState;
  //utility fields to visit the clusters
  std::unordered_map<size_t,std::shared_ptr<State>> _clusters;
  std::unordered_map<std::string,std::shared_ptr<State>> _assIDtoPreCluster;
  std::unordered_map<std::string,std::shared_ptr<State>> _assIDtoPosCluster;
  std::unordered_map<size_t,std::pair<bool,std::string>> _clusterIDtoSavedState;
  std::unordered_map<std::string, Assertion> &_IDtoAssertion;
  //keeps track of the number of clusters
  //0 is the 'safe state'
  size_t _clusterIndex=1;
  //first assertion in the test-plan
  std::string _first_assertion;
};
} // namespace codeGenerator
