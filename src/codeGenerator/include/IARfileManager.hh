
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#pragma once
#include "IARchecker.hh"
#include "types.hh"
#include <memory>

namespace codeGenerator {
/*! \class IARfileManager
    \brief Utility class to dump the .mac files (checkers, setup...)
*/
class IARfileManager {
public:
  /** \brief Constructor
   * \param checkers a set of <id,checkers> initilized with assertions
   * \param intsSetup list of interrupts to be set-up in IAR
   * \param beginCondition proposition that starts the verification if evaluated
   * to true
   * \param plan sorted list of checkers id
   * \param maxTimers number of timers used by the current verification
   */
  IARfileManager(std::map<std::string, IARchecker> &checkers,
                 const std::vector<Interrupt> &intsSetup,
                 std::shared_ptr<oden::Proposition> beginCondition,
                 const std::vector<std::string> &plan = std::vector<std::string>(),
                 size_t maxTimers = 10);

  /** \brief dumps all files to the given path
   */
  void generateFiles(std::string pathToRoot);

  void setTestplan(const std::vector<std::string> &plan);
  /** \brief Utility function to add debug variables that must be created by
   * c-spy and are not present in the DUV
   */
  void setDebugVariables(
      const std::unordered_map<std::string, std::string> &debugVars);

  /** \brief Needed when the test-plan is automatically generated
   */
  void setRestoreState(std::unordered_map<std::string, std::pair<bool, std::string>>
                           &assIDtoRestoreStateID);
  /** \brief Needed when the test-plan is automatically generated
   */
  void setIDtoNextState(std::map<std::string, size_t> assIDToNextState);


private:
  //utility functions to generate all parts of verification environment
  std::pair<Code, Code> generateInterruptsSetup();
  Code generateTimerUtility();
  Code generateExecUserSetup();
  Code generateTestPlan(std::string pathToStateDir) ;
  Code generateVariableInit();
Code generateWaitBeginCondition(std::string pathToStateDir) ;
  Code generateUserExit();
  Code generateDebugVariables();
  std::string generateIncludeFiles(const std::string &root);
  std::string printSummary();

  
  ///list of initializations <varname> = <value>;
  std::vector<std::string> _varInit;
  ///vars (not in the DUV) to be generated and init by c-spy
  std::unordered_map<std::string, std::string> _debugVars;
  ///utility fields to generate the automatic test-plan
  std::unordered_map<std::string, std::pair<bool, std::string>> _assIDToRestoreStateID;
  std::map<std::string, size_t> _assIDToNextState;
  ///condition to start the verification
  std::shared_ptr<oden::Proposition> _beginCondition;
  ///checkers to dump
  std::map<std::string, IARchecker> &_checkers;
  const std::vector<Interrupt> &_intsSetup;
  ///test-plan
  std::vector<std::string> _plan;
  ///timers used by the verification
  size_t _maxTimers = 0;
};
} // namespace codeGenerator
