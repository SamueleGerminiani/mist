
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#include "oden/visitors/AssignExtractVisitor.hh"
#include "oden/odenUtils/propositionUtils.hh"
#include <cassert>

//------------------------------------------------------------------------------
#define VARIABLE(LEAF)                                                         \
  void AssignExtractVisitor::visit(__attribute__((unused)) LEAF &o) {}

#define CONSTANT(LEAF)                                                         \
  void AssignExtractVisitor::visit(__attribute__((unused)) LEAF &o) {}

#define EXPRESSION(NODE)                                                       \
  void AssignExtractVisitor::visit(__attribute__((unused)) oden::NODE &o) {    \
    messageError(                                                              \
        "Illegal operand in proposition, only && and == are allowed!");        \
  }

#define NUMERIC_TO_BOOL(NODE)                                                  \
  void AssignExtractVisitor::visit(NODE &o) {                                  \
    o.getItem().acceptVisitor(*this);                                          \
  }

#define LOGIC_TO_BOOL(NODE)                                                    \
  void AssignExtractVisitor::visit(NODE &o) {                                  \
    o.getItem().acceptVisitor(*this);                                          \
  }

#define LOGIC_TO_NUMERIC(NODE)                                                 \
  void AssignExtractVisitor::visit(NODE &o) {                                  \
    o.getItem().acceptVisitor(*this);                                          \
  }

//------------------------------------------------------------------------------

namespace oden {

AssignExtractVisitor::AssignExtractVisitor() : OdenVisitor() {
  // proposition
  operators[ope::PropositionNot] = std::string("!");
  operators[ope::PropositionAnd] = std::string("&&");
  operators[ope::PropositionOr] = std::string("||");
  operators[ope::PropositionXor] = std::string("^");
  operators[ope::PropositionEq] = std::string("==");
  operators[ope::PropositionNeq] = std::string("!=");

  // numeric
  operators[ope::NumericSum] = std::string("+");
  operators[ope::NumericSub] = std::string("-");
  operators[ope::NumericMul] = std::string("*");
  operators[ope::NumericDiv] = std::string("/");
  operators[ope::NumericEq] = std::string("==");
  operators[ope::NumericNeq] = std::string("!=");
  operators[ope::NumericGreater] = std::string(">");
  operators[ope::NumericGreaterEq] = std::string(">=");
  operators[ope::NumericLess] = std::string("<");
  operators[ope::NumericLessEq] = std::string("<=");

  // logic
  operators[ope::LogicSum] = std::string("+");
  operators[ope::LogicSub] = std::string("-");
  operators[ope::LogicMul] = std::string("*");
  operators[ope::LogicDiv] = std::string("/");
  operators[ope::LogicBAnd] = std::string("&");
  operators[ope::LogicBOr] = std::string("|");
  operators[ope::LogicBXor] = std::string("^");
  operators[ope::LogicNot] = std::string("~");
  operators[ope::LogicEq] = std::string("==");
  operators[ope::LogicNeq] = std::string("!=");
  operators[ope::LogicGreater] = std::string(">");
  operators[ope::LogicGreaterEq] = std::string(">=");
  operators[ope::LogicLess] = std::string("<");
  operators[ope::LogicLessEq] = std::string("<=");
  operators[ope::LogicLShift] = std::string("<<");
  operators[ope::LogicRShift] = std::string(">>");
}

void AssignExtractVisitor::clear() {}

const std::unordered_map<std::string, z3::TypeValue> &
AssignExtractVisitor::get() {
  return _assignments;
}

// proposition
VARIABLE(BooleanVariable)
CONSTANT(BooleanConstant)
EXPRESSION(PropositionOr)
EXPRESSION(PropositionXor)
EXPRESSION(PropositionNeq)

void AssignExtractVisitor::visit(oden::PropositionAnd &o) {

  auto &items = o.getItems();
  assert(items.size() == 2);

  if (!items.empty()) {
    auto iterStop = items.end();
    auto iter = items.begin();

    if (items.size() > 1) {
      --iterStop;
      for (; iter != iterStop; ++iter) {
        (*iter)->acceptVisitor(*this);
      }
    }
    (*iter)->acceptVisitor(*this);
  }
}
void AssignExtractVisitor::visit(oden::PropositionEq &o) {
  auto items = o.getItems();
  assert(items.size() == 2);
  if (dynamic_cast<BooleanVariable *>(items[0]) != nullptr &&
      dynamic_cast<BooleanConstant *>(items[1]) != nullptr) {
  _assignments.insert( {{dynamic_cast<BooleanVariable *>(items[0])->getName(), std::make_pair(dynamic_cast<BooleanVariable *>(items[0])->getType(), z3::VarValue(dynamic_cast<BooleanConstant *>(items[1])->_getValue()))}});

  } else if (dynamic_cast<BooleanVariable *>(items[1]) != nullptr &&
             dynamic_cast<BooleanConstant *>(items[0]) != nullptr) {
  } else {
    messageError(
        "All propositions must be of the form : <Variable> == <Constant>");
  }
}
void AssignExtractVisitor::visit(oden::LogicEq &o) {
  auto items = o.getItems();
  assert(items.size() == 2);
  if (dynamic_cast<LogicVariable *>(items[0]) != nullptr &&
      dynamic_cast<LogicConstant *>(items[1]) != nullptr) {
      if(items[0]->getType()==VarType::ULogic){
  _assignments.insert( {{dynamic_cast<LogicVariable *>(items[0])->getName(), std::make_pair(dynamic_cast<LogicVariable *>(items[0])->getType(), z3::VarValue(dynamic_cast<LogicConstant *>(items[1])->_getValue()))}});
      }else if(items[0]->getType()==VarType::SLogic){
  _assignments.insert( {{dynamic_cast<LogicVariable *>(items[0])->getName(), std::make_pair(dynamic_cast<LogicVariable *>(items[0])->getType(), z3::VarValue((int64_t)dynamic_cast<LogicConstant *>(items[1])->_getValue()))}});
      }

  } else if (dynamic_cast<LogicVariable *>(items[1]) != nullptr &&
             dynamic_cast<LogicConstant *>(items[0]) != nullptr) {
  } else {
    messageError(
        "All propositions must be of the form : <Variable> == <Constant>");
  }
}
void AssignExtractVisitor::visit(oden::NumericEq &o) {
  auto items = o.getItems();
  assert(items.size() == 2);
  if (dynamic_cast<NumericVariable *>(items[0]) != nullptr &&
      dynamic_cast<NumericConstant *>(items[1]) != nullptr) {
      if(items[0]->getType()==VarType::Numeric){
  _assignments.insert( {{dynamic_cast<NumericVariable *>(items[0])->getName(), std::make_pair(dynamic_cast<NumericVariable *>(items[0])->getType(), z3::VarValue(dynamic_cast<NumericConstant *>(items[1])->_getValue()))}});
      }

  } else if (dynamic_cast<NumericVariable *>(items[1]) != nullptr &&
             dynamic_cast<NumericConstant *>(items[0]) != nullptr) {
  } else {
    messageError(
        "All propositions must be of the form : <Variable> == <Constant>");
  }
}
void AssignExtractVisitor::visit(__attribute__((unused))oden::PropositionNot &o) {

  messageError("Illegal operand in proposition, only && and == are allowed!");
}

void AssignExtractVisitor::visit(__attribute__((unused))oden::LogicNot &o) {
  messageError("Illegal operand in proposition, only && and == are allowed!");
}
void AssignExtractVisitor::visit(__attribute__((unused))
                                 oden::NamedLogicConstant &o) {}

// numeric
VARIABLE(NumericVariable)
CONSTANT(NumericConstant)
EXPRESSION(NumericSum)
EXPRESSION(NumericSub)
EXPRESSION(NumericMul)
EXPRESSION(NumericDiv)
EXPRESSION(NumericNeq)
EXPRESSION(NumericGreater)
EXPRESSION(NumericGreaterEq)
EXPRESSION(NumericLess)
EXPRESSION(NumericLessEq)
NUMERIC_TO_BOOL(NumericToBool)

// logic
VARIABLE(LogicVariable)
VARIABLE(EnumVariable)
CONSTANT(LogicConstant)
EXPRESSION(LogicSum)
EXPRESSION(LogicSub)
EXPRESSION(LogicMul)
EXPRESSION(LogicDiv)
EXPRESSION(LogicBAnd)
EXPRESSION(LogicBOr)
EXPRESSION(LogicBXor)
EXPRESSION(LogicNeq)
EXPRESSION(LogicGreater)
EXPRESSION(LogicGreaterEq)
EXPRESSION(LogicLess)
EXPRESSION(LogicLessEq)
LOGIC_TO_NUMERIC(LogicToNumeric)
LOGIC_TO_BOOL(LogicToBool)
EXPRESSION(LogicLShift)
EXPRESSION(LogicRShift)

} // namespace oden
