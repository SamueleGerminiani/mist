
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#include "oden/OdenVisitor.hh"

//------------------------------------------------------------------------------
#define LEAF_NODE(LEAF)                                                        \
  void OdenVisitor::visit(LEAF &) {}

#define EXPRESSION_NODE_1(NODE)                                                \
  void OdenVisitor::visit(NODE &o) {                                           \
    auto &propositions = o.getItems();                                         \
    for (auto *prop : propositions)                                            \
      prop->acceptVisitor(*this);                                              \
  }

#define EXPRESSION_NODE_2(NODE)                                                \
  void OdenVisitor::visit(NODE &o) { o.getItem().acceptVisitor(*this); }

#define EXPRESSION_NODE_3(NODE)                                                \
  void OdenVisitor::visit(NODE &o) {                                           \
    o.getItem1().acceptVisitor(*this);                                         \
    o.getItem2().acceptVisitor(*this);                                         \
  }

#define NUMERIC_TO_BOOL(NODE)                                                  \
  void OdenVisitor::visit(NODE &o) { o.getItem().acceptVisitor(*this); }

#define LOGIC_TO_BOOL(NODE)                                                    \
  void OdenVisitor::visit(NODE &o) { o.getItem().acceptVisitor(*this); }

#define LOGIC_TO_NUMERIC(NODE)                                                 \
  void OdenVisitor::visit(NODE &o) { o.getItem().acceptVisitor(*this); }
//------------------------------------------------------------------------------

namespace oden {

// proposition
LEAF_NODE(BooleanConstant)
LEAF_NODE(BooleanVariable)
EXPRESSION_NODE_1(PropositionAnd)
EXPRESSION_NODE_1(PropositionOr)
EXPRESSION_NODE_1(PropositionXor)
EXPRESSION_NODE_1(PropositionEq)
EXPRESSION_NODE_1(PropositionNeq)
EXPRESSION_NODE_1(PropositionNot)

// numeric
LEAF_NODE(NumericConstant)
LEAF_NODE(NumericVariable)
EXPRESSION_NODE_1(NumericSum)
EXPRESSION_NODE_1(NumericSub)
EXPRESSION_NODE_1(NumericMul)
EXPRESSION_NODE_1(NumericDiv)
EXPRESSION_NODE_1(NumericEq)
EXPRESSION_NODE_1(NumericNeq)
EXPRESSION_NODE_1(NumericGreater)
EXPRESSION_NODE_1(NumericGreaterEq)
EXPRESSION_NODE_1(NumericLess)
EXPRESSION_NODE_1(NumericLessEq)
NUMERIC_TO_BOOL(NumericToBool)

// logic
LEAF_NODE(LogicConstant)
LEAF_NODE(NamedLogicConstant)
LEAF_NODE(LogicVariable)
LEAF_NODE(EnumVariable)
EXPRESSION_NODE_1(LogicSum)
EXPRESSION_NODE_1(LogicSub)
EXPRESSION_NODE_1(LogicMul)
EXPRESSION_NODE_1(LogicDiv)
EXPRESSION_NODE_1(LogicBAnd)
EXPRESSION_NODE_1(LogicBOr)
EXPRESSION_NODE_1(LogicBXor)
EXPRESSION_NODE_1(LogicEq)
EXPRESSION_NODE_1(LogicNeq)
EXPRESSION_NODE_1(LogicGreater)
EXPRESSION_NODE_1(LogicGreaterEq)
EXPRESSION_NODE_1(LogicLess)
EXPRESSION_NODE_1(LogicLessEq)
EXPRESSION_NODE_1(LogicNot)
LOGIC_TO_NUMERIC(LogicToNumeric)
LOGIC_TO_BOOL(LogicToBool)
EXPRESSION_NODE_1(LogicLShift)
EXPRESSION_NODE_1(LogicRShift)

} // namespace oden
