
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#include "oden/classes/expression/Expression.hh"
#include "oden/OdenVisitor.hh"

namespace oden {

//==== Call to Visitor's methods ===============================================
#define VISITOR_CALL(Operator, ET, OT)                                         \
  template <>                                                                  \
  void Expression<BinaryOperator::Operator, ET, OT>::acceptVisitor(            \
      OdenVisitor &vis) {                                                      \
    vis.visit(*this);                                                          \
  }

// proposition
VISITOR_CALL(AND, Proposition, Proposition)
VISITOR_CALL(OR, Proposition, Proposition)
VISITOR_CALL(XOR, Proposition, Proposition)
VISITOR_CALL(EQ, Proposition, Proposition)
VISITOR_CALL(NEQ, Proposition, Proposition)
VISITOR_CALL(NOT, Proposition, Proposition)

// numeric
VISITOR_CALL(SUM, NumericExpression, NumericExpression)
VISITOR_CALL(SUB, NumericExpression, NumericExpression)
VISITOR_CALL(MUL, NumericExpression, NumericExpression)
VISITOR_CALL(DIV, NumericExpression, NumericExpression)
VISITOR_CALL(EQ, NumericExpression, Proposition)
VISITOR_CALL(NEQ, NumericExpression, Proposition)
VISITOR_CALL(GT, NumericExpression, Proposition)
VISITOR_CALL(GE, NumericExpression, Proposition)
VISITOR_CALL(LT, NumericExpression, Proposition)
VISITOR_CALL(LE, NumericExpression, Proposition)

// logic
VISITOR_CALL(SUM, LogicExpression, LogicExpression)
VISITOR_CALL(SUB, LogicExpression, LogicExpression)
VISITOR_CALL(MUL, LogicExpression, LogicExpression)
VISITOR_CALL(DIV, LogicExpression, LogicExpression)
VISITOR_CALL(BAND, LogicExpression, LogicExpression)
VISITOR_CALL(BOR, LogicExpression, LogicExpression)
VISITOR_CALL(BXOR, LogicExpression, LogicExpression)
VISITOR_CALL(EQ, LogicExpression, Proposition)
VISITOR_CALL(NEQ, LogicExpression, Proposition)
VISITOR_CALL(GT, LogicExpression, Proposition)
VISITOR_CALL(GE, LogicExpression, Proposition)
VISITOR_CALL(LT, LogicExpression, Proposition)
VISITOR_CALL(LE, LogicExpression, Proposition)
VISITOR_CALL(NOT, LogicExpression, LogicExpression)
VISITOR_CALL(LS, LogicExpression, LogicExpression)
VISITOR_CALL(RS, LogicExpression, LogicExpression)
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

} // namespace oden
