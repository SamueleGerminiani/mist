
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#include "oden/classes/atom/Variable.hh"
#include "oden/OdenVisitor.hh"
#include "oden/odenUtils/applicationUtils.hh"

namespace oden {

template <> void BooleanVariable::acceptVisitor(OdenVisitor &vis) {
  vis.visit(*this);
}

template <> void NumericVariable::acceptVisitor(OdenVisitor &vis) {
  vis.visit(*this);
}

template <> void LogicVariable::acceptVisitor(OdenVisitor &vis) {
  vis.visit(*this);
}

EnumVariable::EnumVariable(const std::string &name,
                           const std::vector<enumItem> &items)
    : LogicVariable(name, VarType::SLogic, 32), _items(items) {}
const std::vector<enumItem> &EnumVariable::getItems() { return _items; }
void EnumVariable::acceptVisitor(OdenVisitor &vis) { vis.visit(*this); }
} // namespace oden
