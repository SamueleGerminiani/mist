
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#include "oden/odenUtils/applicationUtils.hh"

#include <assert.h>
#include <iostream>
#define _POSIX_THREAD_SAFE_FUNCTIONS
#include <sys/time.h>

namespace oden {
namespace applicationUtils {

namespace /* anon */ {

std::string NowTime() {
  struct timeval tv;
  gettimeofday(&tv, 0);
  char buffer[100];
  tm r;
  strftime(buffer, sizeof(buffer), "%X", localtime_r(&tv.tv_sec, &r));
  char result[100];
  snprintf(result, 100, "%s" /*.%06ld"*/, buffer /*, (long)tv.tv_usec*/);
  return result;
}

} // namespace

void _oden_internal_messageInfo(const std::string &message) {
  std::cout << "[INFO] " << NowTime() << " - "
            << "Message: " << message << std::endl;
  std::cout.flush();
}

void _oden_internal_messageWarning(const std::string &file, unsigned int line,
                                   const std::string &message) {
  std::cout << "\033[1;33m[WARNING] " << NowTime() << " - "
            << "File: " << file << " -- "
            << "Line: " << line << std::endl
            << "\tMessage: " << message << std::endl<<"\033[0m";

  std::cout.flush();
}
void _oden_internal_messageWarning(const std::string &message) {
  std::cout << "\033[1;33m[WARNING] " << NowTime() << " - "
            << "Message: " << message << std::endl<<"\033[0m";
  std::cout.flush();
}

void _oden_internal_messageError(const std::string &file, unsigned int line,
                                 const std::string &message) {
  std::cerr << "\033[1;31m[ERROR] " << NowTime() << " - "
            << "File: " << file<<" "
            << "at line " << line << std::endl
            << "Message: " << message << std::endl<<"\033[0m";

  std::cerr.flush();
  assert(false);
  exit(1);
}

} // namespace applicationUtils
} // namespace oden
