
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#include "oden/odenUtils/xmlUtils.hh"

namespace oden {

void getNodesFromName(XmlNode *root, const std::string &name,
                      XmlNodeList &list) {
    XmlNode *node;
    node = root->first_node(name.c_str(), name.size());

  while (node != nullptr) {
    list.push_back(node);
    node = node->next_sibling(name.c_str());
  }
}

std::string getAttributeValue(XmlNode *node, const std::string &attributeName,
                              const std::string &defaultValue) {
  auto ret = node->first_attribute(attributeName.c_str());
  return ret==nullptr ? defaultValue : ret->value();
}

} // namespace oden
