
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#pragma once

#include <iostream>
#include <string>

namespace oden {

enum class VarType { Bool = 0, ULogic, SLogic, Numeric };

inline std::ostream &operator<<(std::ostream &o, const oden::VarType &vt) {
  switch (vt) {
  case oden::VarType::Bool:
    o << "Bool";
    break;
  case oden::VarType::ULogic:
    o << "ULogic";
    break;
  case oden::VarType::SLogic:
    o << "SLogic";
    break;
  case oden::VarType::Numeric:
    o << "Numeric";
    break;
  }
  return o;
}

enum class BinaryOperator {
  // Boolean operators
  AND = 1,
  OR = 2,
  XOR = 3,
  // Arithmetic operators
  SUM = 4,
  SUB = 5,
  MUL = 6,
  DIV = 7,
  // Bitwise operators
  BAND = 8,
  BOR = 9,
  BXOR = 10,
  NOT = 11,
  // Relational operators
  EQ = 12,
  NEQ = 13,
  LT = 14,
  LE = 15,
  GT = 16,
  GE = 17,
  LS = 18,
  RS = 19
};

} // namespace oden
