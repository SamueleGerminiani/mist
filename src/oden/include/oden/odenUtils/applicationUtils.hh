
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#pragma once

#include <string>

namespace oden {
namespace applicationUtils {

/// @brief Prints information message.
/// @param message is the actual message to print.
void _oden_internal_messageInfo(const std::string &message);

/// @brief Prints warning message.
/// @param file is expanded by macro to the raising point of the message.
/// @param line is expanded by macro to the raising point of the message.
/// @param message is the actual message to print.
void _oden_internal_messageWarning(const std::string &file, unsigned int line,
                                   const std::string &message);

/// @brief Prints an error message, which causes exit.
/// @param file is expanded by macro to the raising point of the message.
/// @param line is expanded by macro to the raising point of the message.
/// @param message is the actual message to print.
void _oden_internal_messageError(const std::string &file, unsigned int line,
                                 const std::string &message);

void _oden_internal_messageWarning(const std::string &message);

#define messageInfo(message)                                                   \
  oden::applicationUtils::_oden_internal_messageInfo((message))

#define messageInfoIf(condition, message)                                      \
  if (condition)                                                               \
  oden::applicationUtils::_oden_internal_messageInfo((message))

#define messageWarning(message)                                                \
  oden::applicationUtils::_oden_internal_messageWarning((message))

#define messageWarningIf(condition, message)                                      \
  if (condition)                                                               \
  oden::applicationUtils::_oden_internal_messageWarning((message))

#define messageError(message)                                                  \
  oden::applicationUtils::_oden_internal_messageError(__FILE__, __LINE__,      \
                                                      (message))

#define messageErrorIf(condition, message)                                     \
  if (condition)                                                               \
  oden::applicationUtils::_oden_internal_messageError(__FILE__, __LINE__,      \
                                                      (message))
} // namespace applicationUtils
} // namespace oden
