
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#include "propositionUtils.hh"

namespace oden {

template <typename T1> void addItems(__attribute__((unused)) T1 &base) {
  // ntd. base case for variadic function.
}

template <typename T1, typename T2, typename... T3>
void addItems(T1 &base, T2 arg1, T3... args) {
  base.addItem(arg1);
  addItems(base, args...);
}

template <typename T1, typename... T2> T1 *makeExpression(T2... args) {
  auto *ret = new T1();
  addItems(*ret, args...);
  return ret;
}

} // namespace oden
