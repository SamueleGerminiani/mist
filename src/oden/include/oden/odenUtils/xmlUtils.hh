
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#pragma once

#include "oden/types.hh"
#include <string>

namespace oden {

/// @brief Fill a xml list with all xml nodels having a specific name.
/// @param node The starting xml node of the function search.
/// @param name The name of wanted xml nodes.
/// @param node A list of wanted xml nodes.
void getNodesFromName(XmlNode *node, const std::string &nanme,
                      XmlNodeList &list);

/// @brief Return as string the attribute's value of a xml node.
/// @param node A xml node.
/// @param node The attribute's name of the given node.
/// @param node The default string if the attribute has not been set.
/// @return A string containing the attribute's value or the defaultValue
std::string getAttributeValue(XmlNode *node, const std::string &attributeName,
                              const std::string &defaultValue = "");
} // namespace oden
