
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#pragma once

#include <cstddef>
#include <cstdint>

#include "oden/odenEnums.hh"

namespace oden {

// Forwards declaration of base class Visitor
class OdenVisitor;

/// @brief An Atom can represent either a Variable or a Constant.
/// It only provides the base methods evaluate and visit.
template <typename T> class Atom {
public:
  /// @brief Destructor.
  virtual ~Atom() = default;

  Atom(VarType type = VarType::Bool, uint8_t size = 64);

  /// @brief Accepts a visitor to visit the current object.
  /// @param vis The visitor.
  virtual void acceptVisitor(OdenVisitor &vis) = 0;

  void setParams(VarType type, uint8_t size) {
    _type = type;
    _size = size;
  }

  uint8_t getSize() { return _size; }

  VarType getType() { return _type; }

protected:
  VarType _type;
  uint8_t _size;
};

using Numeric = double;
using Logic = uint64_t;

using NumericExpression = Atom<Numeric>;
using LogicExpression = Atom<Logic>;
using Proposition = Atom<bool>;

} // namespace oden

#include "Atom.i.hh"
