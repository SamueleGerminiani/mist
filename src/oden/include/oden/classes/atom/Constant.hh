
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#pragma once

#include "Atom.hh"

namespace oden {

/// @brief Constant declaration.
/// This class represents a constant value.
template <typename T> class Constant : public Atom<T> {

public:
  /// @brief Constructor.
  /// @param value a constant value.
  /// @param max_time the largest simulation time that can be provided to
  /// the method evaluate.
  explicit Constant(T value, VarType type = VarType::Bool, uint8_t size = 64);

  /// @brief copy Constructor.
  /// @param other The other constant to be copied.
  Constant(const Constant &other);

  /// @brief Destructor.
  ~Constant() = default;

  /// @brief Assignment operator
  /// @param other The other variable to be copied.
  Constant &operator=(const Constant &other);

  /// @brief Accepts a visitor to visit the current object.
  /// @param vis The visitor.
  void acceptVisitor(OdenVisitor &vis) override;

  T _getValue() { return _value; }

protected:
  ///  @brief Stores the constant value
  T _value;
};

using BooleanConstant = Constant<bool>;
using NumericConstant = Constant<Numeric>;
using LogicConstant = Constant<Logic>;

template <typename T> class NamedConstant : public Constant<T> {
public:
  explicit NamedConstant(std::string name, T value,
                         VarType type = VarType::Bool, uint8_t size = 64);
  void acceptVisitor(OdenVisitor &vis) override;
  std::string _getNamedValue() { return _name; }

private:
  std::string _name;
};

using NamedLogicConstant = NamedConstant<Logic>;

} // namespace oden

#include "Constant.i.hh"
