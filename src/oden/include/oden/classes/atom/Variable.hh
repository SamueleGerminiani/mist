
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#pragma once

#include "Atom.hh"
#include <string>
#include <vector>

namespace oden {

/// @brief BooleanVariable declaration.
/// This class represents a boolean variable.
template <typename T> class Variable_Base : public Atom<T> {
public:
  explicit Variable_Base(const std::string &name, VarType type = VarType::Bool,
                         uint8_t size = 64);

  void acceptVisitor(OdenVisitor &vis) override;

  const std::string &getName();

private:
  // the name of the variable
  std::string _name;
};

using BooleanVariable = Variable_Base<bool>;
using NumericVariable = Variable_Base<double>;
using LogicVariable = Variable_Base<Logic>;

struct enumItem {
  enumItem(const std::string &namedValue, int value)
      : _namedValue(namedValue), _value(value) {}
  std::string _namedValue;
  int _value;
};
class EnumVariable : public LogicVariable {
public:
  EnumVariable(const std::string &name, const std::vector<enumItem> &items);

  void acceptVisitor(OdenVisitor &vis) override;
  const std::vector<enumItem> &getItems();

private:
  const std::vector<enumItem> _items;
};
} // namespace oden

#include "Variable.i.hh"
