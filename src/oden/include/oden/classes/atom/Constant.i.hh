
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/


#include "oden/odenEnums.hh"

namespace oden {

template <typename T>
Constant<T>::Constant(T value, VarType type, uint8_t size)
    : Atom<T>(type, size), _value(value) {}

template <typename T>
Constant<T>::Constant(const Constant &other) : Atom<T>(), _value(other._value) {
  // ntd
}

template <typename T>
NamedConstant<T>::NamedConstant(std::string name, T value, VarType type,
                                uint8_t size)
    : Constant<T>(value, type, size), _name(name) {
  // ntd
}
} // namespace oden
