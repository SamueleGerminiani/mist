
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/


namespace oden {

template <BinaryOperator O, typename OT, typename ET>
Expression<O, OT, ET>::Expression() : ET(), _items() {
  // ntd
}

template <BinaryOperator O, typename OT, typename ET>
void Expression<O, OT, ET>::addItem(OT *item) {
  _items.push_back(item);
}

template <BinaryOperator O, typename OT, typename ET>
std::vector<OT *> &Expression<O, OT, ET>::getItems() {
  return _items;
}

template <BinaryOperator O, typename OT, typename ET>
size_t Expression<O, OT, ET>::size() {
  return _items.size();
}

template <BinaryOperator O, typename OT, typename ET>
bool Expression<O, OT, ET>::empty() {
  return _items.empty();
}

} // namespace oden
