
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#pragma once

#include "oden/classes/atom/Atom.hh"
#include "oden/classes/atom/Constant.hh"
#include "oden/odenUtils/applicationUtils.hh"

namespace oden {

/// @brief TypeCast declaration.
/// This class represents the bit selector operator
template <typename ET, typename RT> class TypeCast : public RT {

public:
  using ReturnType =
      typename std::conditional<std::is_same<RT, LogicExpression>::value, Logic,
                                bool>::type;

  TypeCast(ET *e, VarType type = VarType::Bool, uint8_t size = 8);

  /// @brief Copy constructor.
  TypeCast(const TypeCast &other) = delete;

  /// @brief Destructor.
  virtual ~TypeCast();

  /// @brief Accepts a visitor to visit the current object.
  /// @param vis The visitor.
  void acceptVisitor(OdenVisitor &vis) override;

  /// @brief Return the expression of the next operator
  /// @return A reference to the expression.
  ET &getItem();

  /// @brief Assing operator
  TypeCast &operator=(const TypeCast &other) = delete;

private:
  ET *_e;
};
using LogicToNumeric = TypeCast<LogicExpression, NumericExpression>;
using LogicToBool = TypeCast<LogicExpression, Proposition>;
using NumericToBool = TypeCast<NumericExpression, Proposition>;

} // namespace oden

#include "TypeCast.i.hh"
