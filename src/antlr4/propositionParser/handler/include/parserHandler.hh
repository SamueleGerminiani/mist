
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#pragma once

#include "oden/classes/classes.hh"
#include "oden/odenUtils/propositionUtils.hh"
#include "propositionBaseListener.h"

#include <cmath>
#include <stack>
#include <string>

using namespace antlr4;

using Name = std::string;
using Type = std::string;
using VarDeclaration = std::pair<Name, Type>;
using EnumValues = std::vector<std::pair<Name, int>>;
using EnumMap = std::unordered_map<Name, EnumValues>;
namespace oden {

class ParserHandler : public propositionBaseListener {

public:
  explicit ParserHandler(const EnumMap &enums,std::string location="unknown location");

  ~ParserHandler() override = default;

  Proposition *getProposition();

private:
  bool _abort;
  bool createExpression = false;
  std::unordered_map<std::string, std::vector<enumItem>> _enums;

  std::stack<Proposition *> _proposition;
  std::stack<LogicExpression *> _logicExpressions;
  std::stack<NumericExpression *> _numericExpressions;
  std::string _location;
  size_t boolStack = 0;
  size_t logicStack = 0;
  size_t numeriStack = 0;

  // already implemented
  void enterFile(propositionParser::FileContext *ctx) override;

  // already implemented
  void exitBoolean(propositionParser::BooleanContext *ctx) override;
  // already implemented
  virtual void exitLogic(propositionParser::LogicContext *ctx) override;
  // already implemented
  virtual void exitNumeric(propositionParser::NumericContext *ctx) override;
  // to implement
  // virtual void enterString(propositionParser::StringContext *ctx) override;

  // already implemented
  void
  enterBooleanVariable(propositionParser::BooleanVariableContext *ctx) override;
  // already implemented
  virtual void
  enterLogicVariable(propositionParser::LogicVariableContext *ctx) override;
  // already implemented
  virtual void
  enterNumericVariable(propositionParser::NumericVariableContext *ctx) override;
  // to implement
  // virtual void enterStringVariable(propositionParser::StringVariableContext
  // *ctx) override;

  // already implemented
  virtual void
  enterBooleanConstant(propositionParser::BooleanConstantContext *ctx) override;
  // already implemented
  virtual void
  enterLogicConstant(propositionParser::LogicConstantContext *ctx) override;
  // already implemented
  virtual void
  enterNumericConstant(propositionParser::NumericConstantContext *ctx) override;
  // to implement
  //  virtual void enterStringConstant(propositionParser::StringConstantContext
  //  *ctx) override;

  virtual void
  enterNamedLogicConst(propositionParser::NamedLogicConstContext *ctx) override;

  virtual void
  enterEnumVariable(propositionParser::EnumVariableContext *ctx) override;
  virtual void visitErrorNode(antlr4::tree::ErrorNode *node) override;
};

} // namespace oden
