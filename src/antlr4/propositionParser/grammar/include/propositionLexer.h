
// Generated from proposition.g4 by ANTLR 4.7.2

#pragma once


#include "antlr4-runtime.h"




class  propositionLexer : public antlr4::Lexer {
public:
  enum {
    T__0 = 1, T__1 = 2, T__2 = 3, T__3 = 4, T__4 = 5, T__5 = 6, T__6 = 7, 
    SIGN = 8, LGPAREN = 9, RGPAREN = 10, LCPAREN = 11, RCPAREN = 12, LPAREN = 13, 
    RPAREN = 14, CAST_BOOL = 15, CAST_NUMERIC = 16, VARIABLE = 17, NUMERIC = 18, 
    VERILOG_BINARY = 19, GCC_BINARY = 20, HEX = 21, BOOLEAN = 22, PLUS = 23, 
    MINUS = 24, TIMES = 25, DIV = 26, GT = 27, GE = 28, LT = 29, LE = 30, 
    EQ = 31, NEQ = 32, BAND = 33, BOR = 34, BXOR = 35, NEG = 36, LSHIFT = 37, 
    RSHIFT = 38, AND = 39, OR = 40, NOT = 41, WS = 42
  };

  propositionLexer(antlr4::CharStream *input);
  ~propositionLexer();

  virtual std::string getGrammarFileName() const override;
  virtual const std::vector<std::string>& getRuleNames() const override;

  virtual const std::vector<std::string>& getChannelNames() const override;
  virtual const std::vector<std::string>& getModeNames() const override;
  virtual const std::vector<std::string>& getTokenNames() const override; // deprecated, use vocabulary instead
  virtual antlr4::dfa::Vocabulary& getVocabulary() const override;

  virtual const std::vector<uint16_t> getSerializedATN() const override;
  virtual const antlr4::atn::ATN& getATN() const override;

private:
  static std::vector<antlr4::dfa::DFA> _decisionToDFA;
  static antlr4::atn::PredictionContextCache _sharedContextCache;
  static std::vector<std::string> _ruleNames;
  static std::vector<std::string> _tokenNames;
  static std::vector<std::string> _channelNames;
  static std::vector<std::string> _modeNames;

  static std::vector<std::string> _literalNames;
  static std::vector<std::string> _symbolicNames;
  static antlr4::dfa::Vocabulary _vocabulary;
  static antlr4::atn::ATN _atn;
  static std::vector<uint16_t> _serializedATN;


  // Individual action functions triggered by action() above.

  // Individual semantic predicate functions triggered by sempred() above.

  struct Initializer {
    Initializer();
  };
  static Initializer _init;
};

