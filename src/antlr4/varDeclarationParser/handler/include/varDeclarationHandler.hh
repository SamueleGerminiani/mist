
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#pragma once

#include "odenCore.hh"
#include "varDeclarationBaseListener.h"

#include <cmath>
#include <stack>
#include <string>

using namespace antlr4;

namespace oden {

using Name = std::string;
using Type = std::string;
using VarDeclaration = std::pair<Name, Type>;
using EnumValues = std::vector<std::pair<Name, int>>;
using EnumMap = std::unordered_map<Name, EnumValues>;
class varDeclarationParserHandler : public varDeclarationBaseListener {
public:
explicit varDeclarationParserHandler(std::string location);

  ~varDeclarationParserHandler() override = default;

  virtual void enterVarDec(varDeclarationParser::VarDecContext *ctx) override;
  virtual void enterEnumDec(varDeclarationParser::EnumDecContext *ctx) override;
  virtual void exitEnumDec(varDeclarationParser::EnumDecContext *) override;

  virtual void
  enterEnum_list(varDeclarationParser::Enum_listContext *ctx) override;
  virtual void
  exitAddLogic(varDeclarationParser::AddLogicContext * /*ctx*/) override;
  virtual void exitLogic(varDeclarationParser::LogicContext *ctx) override;
  virtual void
  enterLogicName(varDeclarationParser::LogicNameContext *ctx) override;
  virtual void
  enterLogicConstant(varDeclarationParser::LogicConstantContext *ctx) override;
  void print();
  virtual void
  enterEnumType(varDeclarationParser::EnumTypeContext *ctx) override;
  virtual void visitErrorNode(antlr4::tree::ErrorNode * /*node*/) override;

  std::vector<VarDeclaration> &getVarDeclarations();
std::vector<std::pair<std::string,std::string>> &getDebugVars() ;
  const EnumMap &getEnums();

private:
  bool _abort;

  std::vector<VarDeclaration> _varDecls;
  std::vector<std::pair<std::string,std::string>> _debugVariables;
  EnumMap _enums;
  std::unordered_map<std::string, int> _enumToInt;
  std::string _currEnum = "";
  int _lastLogic = -1;
  std::stack<int> _logicExpressions;
  std::string _location;
};

} // namespace oden
