
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
* @file             
* @author
* Samuele Germiniani, samuele.germiniani@univr.it
* Moreno Bragaglio, moreno.bragaglio@univr.it
* @date 2020
* @version v1.0
*/

#pragma once


#include "oden/odenUtils/odenUtils.hh"
#include <unordered_set>

namespace oden {
extern std::unordered_map<std::string, std::string> foundVariablesDefault;
extern std::unordered_map<std::string, std::string> debugVars;

    using Name = std::string;
    using Type = std::string;
    using VarDeclaration = std::pair<Name, Type>;
    using EnumValues = std::vector<std::pair<Name, int>>;
    using EnumMap = std::unordered_map<Name, EnumValues>;

enum class expectedToken { TypeP1 = 0, TypeP2, TypeP3, Name };
void clearVariables();

std::pair<VarType,uint8_t> string2VarTypes(std::string type);
void addTypeToProposition(std::string &formula,
                          std::vector<VarDeclaration> &varDeclaration,
                          const EnumMap &enums);

Proposition *parseProposition(std::string &formula,
                              const std::string &localDecls,const std::string &globalDecls,
                              std::string propLocation="unknown location",std::string locDecLocation="unknown location");

std::unordered_map<std::string,std::string>& getDebugVars();
bool exists(const std::string &varName) ;
}  // namespace oden    
