
/**
****************************************************************************
* Copyright (C) 2020 Mist
*
*  This file is part of Mist.
*
*  Mist is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Mist is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Mist.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/
/**
 * @file
 * @author
 * Samuele Germiniani, samuele.germiniani@univr.it
 * Moreno Bragaglio, moreno.bragaglio@univr.it
 * @date 2020
 * @version v1.0
 */

#include "parserUtils.hh"
#include "parsers.hh"
#include <regex>
namespace oden {
std::unordered_map<std::string, std::string> foundVariablesDefault;
std::unordered_map<std::string, std::string> debugVars;
void clearVariables() {
  foundVariablesDefault.clear();
  debugVars.clear();
}
bool exists(const std::string &varName) {
  return foundVariablesDefault.count(varName);
}
std::unordered_map<std::string, std::string> &getDebugVars() {
  return debugVars;
}

Proposition *parseProposition(std::string &formula,
                              const std::string &localDecls,
                              const std::string &globalDecls,
                              std::string propLocation,
                              std::string locDecLocation) {
  // DEBUG
  // formula="ePage == PAGE_MAIN && c8val==M_TEST+M_NULL";

  // parse local var declarations
  //  messageErrorIf(localDecls == "" && globalDecls == "", "No declarations
  //  found for " + propLocation);
  oden::varDeclarationParserHandler listenerLocDec(locDecLocation);
  if (localDecls != "") {
    antlr4::ANTLRInputStream inputLocDec(localDecls);
    varDeclarationLexer lexerLocDec(&inputLocDec);
    CommonTokenStream tokensLocDec(&lexerLocDec);
    varDeclarationParser parserPrecLocDec(&tokensLocDec);
    tree::ParseTree *treeLocDec = parserPrecLocDec.file();
    // std::cout << treeLocDec->toStringTree(&parserPrecLocDec) << "\n\n\n";
    tree::ParseTreeWalker::DEFAULT.walk(&listenerLocDec, treeLocDec);

    // keep track of debug variables
    for (const auto &debugVar : listenerLocDec.getDebugVars()) {
      debugVars.insert({{debugVar.first, debugVar.second}});
    }
    // check var type
    for (const auto &name_type : listenerLocDec.getVarDeclarations()) {
      if (foundVariablesDefault.count(name_type.first) &&
          name_type.second != foundVariablesDefault[name_type.first]) {
        messageError("Variable " + name_type.first +
                     " redaclered with different type in " + locDecLocation +
                     "\n\tPrevious declaration: " +
                     foundVariablesDefault[name_type.first] +
                     "\n\tRedeclaration: " + name_type.second);
      } else {
        foundVariablesDefault[name_type.first] = name_type.second;
      }
    }
  }

  // parse global var declarations
  oden::varDeclarationParserHandler listenerGloDec("global declaration");
  if (globalDecls != "") {
    antlr4::ANTLRInputStream inputGloDec(globalDecls);
    varDeclarationLexer lexerGloDec(&inputGloDec);
    CommonTokenStream tokensGloDec(&lexerGloDec);
    varDeclarationParser parserPrecGloDec(&tokensGloDec);
    tree::ParseTree *treeGlo = parserPrecGloDec.file();
    tree::ParseTreeWalker::DEFAULT.walk(&listenerGloDec, treeGlo);

    // keep track of debug variables
    for (const auto &debugVar : listenerGloDec.getDebugVars()) {
      debugVars.insert({{debugVar.first, debugVar.second}});
    }
    // check var type
    for (const auto &name_type : listenerGloDec.getVarDeclarations()) {
      if (foundVariablesDefault.count(name_type.first) &&
          name_type.second != foundVariablesDefault[name_type.first]) {
        messageError("Variable " + name_type.first +
                     " redaclered with different type in global declaration" +
                     "\n\tPrevious declaration: " +
                     foundVariablesDefault[name_type.first] +
                     "\n\tRedeclaration: " + name_type.second);
      } else {
        foundVariablesDefault[name_type.first] = name_type.second;
      }
    }
  }
  // DEBUG
  //    listener1.print();
  std::vector<VarDeclaration> varDecls = listenerLocDec.getVarDeclarations();
  varDecls.insert(varDecls.end(), listenerGloDec.getVarDeclarations().begin(),
                  listenerGloDec.getVarDeclarations().end());
  EnumMap enums = listenerLocDec.getEnums();

  // Add global enums
  for (auto key_enum : listenerGloDec.getEnums()) {
    // handle special case (unamed enums) with overlapping key ("")
    if (key_enum.first == "" && enums.count(key_enum.first)) {
      for (auto e : key_enum.second) {
        enums[key_enum.first].push_back(e);
      }
    } else {
      enums[key_enum.first] = key_enum.second;
    }
  }

  addTypeToProposition(formula, varDecls, enums);

  // parse typed propositions
  oden::ParserHandler listener2(enums, propLocation);
  antlr4::ANTLRInputStream input2(formula);
  propositionLexer lexer2(&input2);
  CommonTokenStream tokens2(&lexer2);
  propositionParser parser2(&tokens2);
  tree::ParseTree *treeFragAnt = parser2.file();
  tree::ParseTreeWalker::DEFAULT.walk(&listener2, treeFragAnt);
  /*
  DEBUG
  std::cout << tree->toStringTree(&parser2) << "\n\n\n";
  exit(0);
  */
  return listener2.getProposition();
}
void findAllOccurances(std::vector<size_t> &vec, std::string data,
                       std::string toSearch) {
  // Get the first occurrence
  size_t pos = data.find(toSearch);
  // Repeat till end is reached
  while (pos != std::string::npos) {
    // Add position to the vector
    vec.push_back(pos);
    // Get the next occurrence from the current position
    pos = data.find(toSearch, pos + toSearch.size());
  }
}
void addTypeToProposition(std::string &formula,
                          std::vector<VarDeclaration> &varDeclaration,
                          const EnumMap &enums) {
  /*all this code is to solve the problem of
  adding types to the variables in the formula:
  The complexity of the code is to handle the following problems:*/
  //(1) A variable can be used mutiple times non sequentially(need to parse
  // the string n times) (2) A variable can contain the words bool, logic,
  // numeric (3) A variable name can be contained in an other variable, ex.
  // state, next_state

  std::vector<std::pair<std::string, VarType>> names;
  std::unordered_map<std::string, size_t> nameToSize;
  std::unordered_map<std::string, int> enumNamedValToValue;
  std::unordered_map<std::string, std::string> varNameToEnumName;

  // add variables as names
  for (auto varDecl : varDeclaration) {
    // add enum name
    if (enums.count(varDecl.second)) {
      names.push_back(std::make_pair(varDecl.first, VarType::SLogic));
      varNameToEnumName.insert({{varDecl.first, varDecl.second}});
      continue;
    }
    std::pair<VarType, uint8_t> t = string2VarTypes(varDecl.second);
    names.push_back(std::make_pair(varDecl.first, t.first));
    nameToSize[varDecl.first] = t.second;
  }

  // add enum constants as names
  for (const auto &name_values : enums) {
    for (const auto &name_val : name_values.second) {
      std::pair<VarType, uint8_t> t = std::make_pair(VarType::SLogic, 32);
      names.push_back(std::make_pair(name_val.first, t.first));
      enumNamedValToValue.insert({{name_val.first, name_val.second}});
    }
  }

  /*now that with have all the variables, we can insert the types in the
  formula*/

  // match the longest variables first to solve (3)
  std::sort(begin(names), end(names),
            [](std::pair<std::string, VarType> &e1,
               std::pair<std::string, VarType> &e2) {
              return e1.first.size() > e2.first.size();
            });

  /* keep track of substitutions to solve (2) and (3)
   The false values correspond to characters of formula that can be
   substituted, the true values correspond to characters of formula that
   can not be substituted*/

  std::deque<bool> changes(formula.size());

  // exclude hex values from substitutions
  std::vector<size_t> indexes;
  findAllOccurances(indexes, formula, "0x");
  std::regex hex("[A-Fa-f0-9]");

  for (size_t index : indexes) {
    size_t current_index = index + 2;
    if (current_index >= formula.size()) {
      break;
    }
    while (std::regex_match(std::string(1, formula[current_index]), hex)) {
      changes[current_index++] = true;
    }
  }

  // Substitute each variable in the formula (if present)
  for (auto name : names) {
    std::string nameType = "";
    switch (name.second) {
    case VarType::Bool:
      nameType = "<" + name.first + ",bool>";
      break;
    case VarType::ULogic:
      nameType = "<" + name.first + ",logic(u," +
                 std::to_string(nameToSize.at(name.first)) + ")>";
      break;
    case VarType::SLogic:
      if (varNameToEnumName.count(name.first)) {
        nameType = "<" + name.first + ",enum(" +
                   varNameToEnumName.at(name.first) + ")>";
        break;
      }
      if (enumNamedValToValue.count(name.first)) {
        nameType = "<" + name.first + ",constlogic(" +
                   std::to_string(enumNamedValToValue.at(name.first)) + ")>";
        break;
      }

      nameType = "<" + name.first + ",logic(s," +
                 std::to_string(nameToSize.at(name.first)) + ")>";
      break;
    case VarType::Numeric:
      nameType = "<" + name.first + ",numeric(" +
                 std::to_string(nameToSize.at(name.first)) + ")>";
      break;
    default:
      messageError("Variable is of \'Uknown type\'");
      break;
    }

    // restart from the beginning of the current formula
    auto it = begin(formula);
    std::string toFind = name.first;

    //    std::cout<<"matching:"<<toFind<<"\n";
    // add type to the variable (if present)
    while (1) {
      // search for the current var in the formula
      it = std::search(it, end(formula), begin(toFind), end(toFind));
      if (it == end(formula)) {
        // not found... terminating the substitution for this
        // variable
        //            std::cout<<"No\n";
        break;
      }

      /*check if you are trying to substitute a substring of an
      already substituted variable*/
      if (*(begin(changes) + (it - begin(formula)))) {
        /*increment the index iterator until the end of the
        forbidden area or until the end of the string*/
        while (*(begin(changes) + (it - begin(formula)))) {
          it++;
          if (it == end(formula)) {
            break;
          }
        }
        continue;
      }

      // substitute the typeless variable with <varName,type>

      // remove the plain variable
      formula.erase(it, it + toFind.size());

      /* keep track of substitutions: tranform the changes vector to
       mirror the state of formula*/
      std::transform(begin(changes) + (it - begin(formula)),
                     begin(changes) + (it - begin(formula)) + toFind.size(),
                     begin(changes) + (it - begin(formula)),
                     [](__attribute__((unused)) bool e) { return true; });

      /*calculate how much to extend the changes vector (must be of
      the same size of formula*/

      if (nameType.size() > toFind.size()) {
        size_t toAdd = nameType.size() - toFind.size();
        // extend changes to match the new size of formula
        changes.insert(begin(changes) + (it - begin(formula)), toAdd, true);
      } else {
        // restrict changes to match the new size of formula
        size_t toRemove = toFind.size() - nameType.size();
        changes.erase(begin(changes) + (it - begin(formula)),
                      begin(changes) + (it - begin(formula) + toRemove));
      }

      // insert the variable with its type
      size_t dist = std::distance(begin(formula), it);
      formula.insert(it, begin(nameType), end(nameType));
      it = begin(formula) + dist + nameType.size();
      /*'it' is an iterator to the character of formula after the last
      substitution*/
    } // end while(1) --> end of formula

    assert(changes.size() == formula.size());
  } // end var
    //    std::cout << "After: " << formula << "\n";
}

std::pair<VarType, uint8_t> string2VarTypes(std::string type) {
  if (type == "bool") {
    return std::make_pair(VarType::Bool, 8);
  } else if (type == "unsigned char") {
    return std::make_pair(VarType::ULogic, 8);
  } else if (type == "unsigned short") {
    return std::make_pair(VarType::ULogic, 16);
  } else if (type == "unsigned int") {
    return std::make_pair(VarType::ULogic, 32);
  } else if (type == "unsigned long long") {
    return std::make_pair(VarType::ULogic, 64);
  } else if (type == "char") {
    return std::make_pair(VarType::SLogic, 8);
  } else if (type == "short") {
    return std::make_pair(VarType::SLogic, 16);
  } else if (type == "int") {
    return std::make_pair(VarType::SLogic, 32);
  } else if (type == "long long") {
    return std::make_pair(VarType::SLogic, 64);
  } else if (type == "float") {
    return std::make_pair(VarType::Numeric, 32);
  } else if (type == "double") {
    return std::make_pair(VarType::Numeric, 64);
  } else {
    messageError("Uknown variable type '" + type + "'");
  }

  return std::make_pair(VarType::Bool, 0);
}
} // namespace oden
